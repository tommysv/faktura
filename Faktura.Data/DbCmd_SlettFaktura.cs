﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_SlettFaktura : DbCmd
    {
        private int fakturaId;

        public DbCmd_SlettFaktura(int fakturaId)
            : base()
        {
            this.fakturaId = fakturaId;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@ID", FbDbType.Integer)
            };
            param[0].Value = fakturaId;
            DbHelper.ExecuteScalar("DP_FAKTURA", param);
        }
    }
}
