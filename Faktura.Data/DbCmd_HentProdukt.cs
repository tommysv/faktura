﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentProdukt : DbCmd
    {
        private int produktId;

        public DbCmd_HentProdukt(int produktId)
            : base()
        {
            this.produktId = produktId;
        }

        public override void Execute()
        {
            Common.Produkt produkt = null;

            FbDataReader reader = null;
            try
            {
                FbParameter[] param = new FbParameter[] { new FbParameter("@ID", FbDbType.Integer) };
                param[0].Value = produktId;
                reader = DbHelper.ExecuteDataReader("SP_PRODUKT", param);
                if (reader.HasRows)
                {
                    reader.Read();
                    produkt = new Common.Produkt();
                    produkt.Id = Convert.ToInt32(reader["PR_ID"]);
                    produkt.ProduktKode = Convert.ToString(reader["PR_PRODUKTKODE"]);
                    produkt.Beskrivelse = Convert.ToString(reader["PR_BESKRIVELSE"]);
                    produkt.Enhet = Convert.ToString(reader["PR_ENHET"]);
                    produkt.EnhetsPris = Convert.ToDouble(reader["PR_ENHETSPRIS"]);
                    produkt.MvaSats = Convert.ToDouble(reader["PR_MVASATS"]);
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = produkt;
        }
    }
}
