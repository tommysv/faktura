﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public abstract class DbCmd
    {
        private object result;
        private FbConnection connection;
        private FbTransaction transaction;
        private bool useTransaction;

        public DbCmd()
        {
            result = null;
            useTransaction = false;
            connection = null;
            transaction = null;
        }

        public object Result
        {
            get
            {
                return result;
            }
            set
            {
                result = value;
            }
        }

        public FbConnection Connection
        {
            get
            {
                return connection;
            }
            set
            {
                connection = value;
            }
        }

        public FbTransaction Transaction
        {
            get
            {
                return transaction;
            }
            set
            {
                transaction = value;
            }
        }

        public bool UseTransaction
        {
            get
            { 
                return useTransaction; 
            }
            set 
            { 
                useTransaction = value; 
            }
        }

        public abstract void Execute();
    }
}
