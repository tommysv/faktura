﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_LagreFakturaGrunnlag : DbCmd
    {
        private Common.FakturaGrunnlag fakturaGrunnlag;
        private bool bLagreFakturaLinjer;

        public DbCmd_LagreFakturaGrunnlag(Common.FakturaGrunnlag fakturaGrunnlag, bool bLagreFakturaLinjer = true)
            : base()
        {
            this.fakturaGrunnlag = fakturaGrunnlag;
            this.bLagreFakturaLinjer = bLagreFakturaLinjer;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@FG_ID", FbDbType.Integer),
                new FbParameter("@FG_OPPRETTET", FbDbType.TimeStamp),
                new FbParameter("@FG_REFERANSE", FbDbType.VarChar, 50),
                new FbParameter("@FG_BESKRIVELSE", FbDbType.VarChar, 100),
                new FbParameter("@FG_BELOP", FbDbType.Double),
                new FbParameter("@FG_MVA", FbDbType.Double),
                new FbParameter("@FG_SUM", FbDbType.Double),
                new FbParameter("@FG_STATUS", FbDbType.Integer),
                new FbParameter("@FG_KU_ID", FbDbType.Integer)
            };
            param[0].Value = fakturaGrunnlag.Id;
            param[1].Value = fakturaGrunnlag.Opprettet;
            param[2].Value = fakturaGrunnlag.Referanse;
            param[3].Value = fakturaGrunnlag.Beskrivelse;
            param[4].Value = fakturaGrunnlag.Beløp;
            param[5].Value = fakturaGrunnlag.Mva;
            param[6].Value = fakturaGrunnlag.Sum;
            param[7].Value = fakturaGrunnlag.Status;
            param[8].Value = fakturaGrunnlag.Kunde.Id;

            object id = DbHelper.ExecuteScalar("UP_FAKTURAGRUNNLAG", param, Connection, Transaction);
            fakturaGrunnlag.Id = Convert.ToInt32(id);

            if (bLagreFakturaLinjer)
            {
                for (int nLinje = 0; nLinje < fakturaGrunnlag.Linjer.Count; nLinje++)
                {
                    if ((fakturaGrunnlag.Linjer[nLinje].Id != -1) && (fakturaGrunnlag.Linjer[nLinje].SlettFlagg))
                    {
                        DbCmd_SlettFakturaLinje cmd_SlettFakturaLinje = new DbCmd_SlettFakturaLinje(fakturaGrunnlag.Linjer[nLinje].Id);
                        cmd_SlettFakturaLinje.Connection = Connection;
                        cmd_SlettFakturaLinje.Transaction = Transaction;
                        cmd_SlettFakturaLinje.Execute();
                    }
                    else
                    {
                        DbCmd_LagreFakturaLinje cmd_LagreFakturaLinje = new DbCmd_LagreFakturaLinje(fakturaGrunnlag.Linjer[nLinje], fakturaGrunnlag.Id);
                        cmd_LagreFakturaLinje.Connection = Connection;
                        cmd_LagreFakturaLinje.Transaction = Transaction;
                        cmd_LagreFakturaLinje.Execute();
                    }
                }
            }
        }
    }
}
