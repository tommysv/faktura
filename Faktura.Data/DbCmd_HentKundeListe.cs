﻿using System;
using System.Collections.Generic;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentKundeListe : DbCmd
    {
        public DbCmd_HentKundeListe()
            : base()
        {
        }

        public override void Execute()
        {
            List<Common.Kunde> kundeListe = new List<Common.Kunde>();

            FbDataReader reader = null;
            try
            {
                reader = DbHelper.ExecuteDataReader("SP_KUNDELISTE", null);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Common.Kunde kunde = new Common.Kunde();
                        kunde.Id = Convert.ToInt32(reader["KU_ID"]);
                        kunde.Navn = Convert.ToString(reader["KU_NAVN"]);
                        kunde.Postadresse = Convert.ToString(reader["KU_POSTADRESSE"]);
                        kunde.PostNr = Convert.ToString(reader["KU_POSTNR"]);
                        kunde.PostSted = Convert.ToString(reader["KU_POSTSTED"]);
                        kunde.FakturaEpost = Convert.ToString(reader["KU_FAKTURAEPOST"]);
                        kunde.GrunnlagEpost = Convert.ToString(reader["KU_GRUNNLAGEPOST"]);
                        kunde.KundeType = (Common.KundeType)Convert.ToInt32(reader["KU_TYPE"]);
                        kunde.FakturaMetode = (Common.FakturaMetode)Convert.ToInt32(reader["KU_FA_METODE"]);
                        kundeListe.Add(kunde);
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = kundeListe;
        }
    }
}
