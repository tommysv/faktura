﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_LagreFakturaLinje : DbCmd
    {
        private Common.FakturaLinje fakturaLinje;
        private int fakturaId;

        public DbCmd_LagreFakturaLinje(Common.FakturaLinje fakturaLinje, int fakturaId)
            : base()
        {
            this.fakturaLinje = fakturaLinje;
            this.fakturaId = fakturaId;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@FL_ID", FbDbType.Integer),
                new FbParameter("@FL_PRODUKTKODE", FbDbType.VarChar, 6),
                new FbParameter("@FL_BESKRIVELSE", FbDbType.VarChar, 100),
                new FbParameter("@FL_ANTALL", FbDbType.Double),
                new FbParameter("@FL_ENHET", FbDbType.VarChar, 10),
                new FbParameter("@FL_ENHETSPRIS", FbDbType.Double),
                new FbParameter("@FL_KALKULERT_BELOP", FbDbType.Decimal),
                new FbParameter("@FL_KALKULERT_MVA", FbDbType.Decimal),
                new FbParameter("@FL_KALKULERT_SUM", FbDbType.Decimal),
                new FbParameter("@FL_FA_ID", FbDbType.Integer)
            };
            param[0].Value = fakturaLinje.Id;
            param[1].Value = fakturaLinje.ProduktKode;
            param[2].Value = fakturaLinje.Beskrivelse;
            param[3].Value = fakturaLinje.Ant;
            param[4].Value = fakturaLinje.Enhet;
            param[5].Value = fakturaLinje.EnhetsPris;
            param[6].Value = fakturaLinje.KalkulertBeløp;
            param[7].Value = fakturaLinje.KalkulertMva;
            param[8].Value = fakturaLinje.KalkulertSum;
            param[9].Value = fakturaId;
            object id = DbHelper.ExecuteScalar("UP_FAKTURALINJE", param, Connection, Transaction);
            fakturaLinje.Id = Convert.ToInt32(id);
        }
    }
}
