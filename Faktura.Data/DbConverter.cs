﻿using System;

namespace Faktura.Data
{
    public static class DbConverter
    {
        public static DateTime? DBNullToDateTime(object obj)
        {
            DateTime? dateTime = null;
            if (obj != DBNull.Value)
                dateTime = Convert.ToDateTime(obj);
            return dateTime;
        }
    }
}
