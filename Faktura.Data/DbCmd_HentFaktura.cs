﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentFaktura : DbCmd
    {
        private int fakturaId;
        private bool bHentFakturaLinjer;

        public DbCmd_HentFaktura(int fakturaId, bool bHentFakturaLinjer = true)
            : base()
        {
            this.fakturaId = fakturaId;
            this.bHentFakturaLinjer = bHentFakturaLinjer; 
        }

        public override void Execute()
        {
            Common.Faktura faktura = null;

            FbDataReader reader = null;
            try
            {
                FbParameter[] param = new FbParameter[] { new FbParameter("@ID", FbDbType.Integer) };
                param[0].Value = fakturaId;
                reader = DbHelper.ExecuteDataReader("SP_FAKTURA", param);
                if (reader.HasRows)
                {
                    reader.Read();
                    faktura = new Common.Faktura();
                    faktura.Id = Convert.ToInt32(reader["FA_ID"]);
                    faktura.Nr = Convert.ToInt32(reader["FA_NR"]);
                    faktura.Dato = DbConverter.DBNullToDateTime(reader["FA_DATO"]);
                    faktura.ModifisertDato = Convert.ToDateTime(reader["FA_DATO_MODIFISERT"]);
                    faktura.ForfallsDato = DbConverter.DBNullToDateTime(reader["FA_DATO_FORFALL"]);
                    faktura.InnbetaltDato = DbConverter.DBNullToDateTime(reader["FA_DATO_INNBETALT"]);
                    faktura.Beskrivelse = Convert.ToString(reader["FA_BESKRIVELSE"]);
                    faktura.Referanse = Convert.ToString(reader["FA_REFERANSE"]);
                    faktura.KalkulertBeløp = Convert.ToDouble(reader["FA_KALKULERT_BELOP"]);
                    faktura.KalkulertMva = Convert.ToDouble(reader["FA_KALKULERT_MVA"]);
                    faktura.KalkulertSum = Convert.ToDouble(reader["FA_KALKULERT_SUM"]);
                    faktura.Status = (Common.FakturaStatus)Convert.ToInt32(reader["FA_STATUS"]);
                    faktura.Kredittnota = Convert.ToBoolean(reader["FA_KREDITTNOTA"]);
                    faktura.Kreditert = Convert.ToBoolean(reader["FA_KREDITERT"]);
                    Common.Kunde kunde = new Common.Kunde();
                    kunde.Id = Convert.ToInt32(reader["FA_KU_ID"]);
                    kunde.Navn = Convert.ToString(reader["KU_NAVN"]);
                    kunde.Postadresse = Convert.ToString(reader["KU_POSTADRESSE"]);
                    kunde.PostNr = Convert.ToString(reader["KU_POSTNR"]);
                    kunde.PostSted = Convert.ToString(reader["KU_POSTSTED"]);
                  //  kunde.Epost =  Convert.ToString(reader["KU_EPOST"]);
                    kunde.KundeType = (Common.KundeType)Convert.ToInt32(reader["KU_TYPE"]);
                    faktura.Kunde = kunde;

                    if (bHentFakturaLinjer)
                    {
                        DbCmd_HentFakturaLinjer cmd_HentFakturaLinjer = new DbCmd_HentFakturaLinjer(fakturaId);
                        cmd_HentFakturaLinjer.Execute();
                        faktura.Linjer = (List<Common.FakturaLinje>)cmd_HentFakturaLinjer.Result;
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = faktura;
        }
    }
}
