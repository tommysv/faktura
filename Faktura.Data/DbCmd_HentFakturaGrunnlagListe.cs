﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentFakturaGrunnlagListe : DbCmd
    {
        private Common.FakturaGrunnlagFilter fakturaGrunnlagFilter;

        public DbCmd_HentFakturaGrunnlagListe(Common.FakturaGrunnlagFilter fakturaGrunnlagFilter)
            :base()
        {
            this.fakturaGrunnlagFilter = fakturaGrunnlagFilter;
        }

        public override void Execute()
        {
            List<Common.FakturaGrunnlag> fakturaGrunnlagListe = new List<Common.FakturaGrunnlag>();

            FbDataReader reader = null;
            try
            {
                FbParameter[] param = new FbParameter[] 
                { 
                    new FbParameter("@AAR", FbDbType.Integer),
                    new FbParameter("@STATUS", FbDbType.Integer),
                    new FbParameter("@KU_ID", FbDbType.Integer)
                };
                param[0].Value = fakturaGrunnlagFilter.Aar;
                param[1].Value = Convert.ToInt32(fakturaGrunnlagFilter.Status);
                param[2].Value = Convert.ToInt32(fakturaGrunnlagFilter.KundeId);
                reader = DbHelper.ExecuteDataReader("SP_FAKTURAGRUNNLAGLISTE", param);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Common.FakturaGrunnlag fakturaGrunnlag = new Common.FakturaGrunnlag();
                        fakturaGrunnlag.Id = Convert.ToInt32(reader["FG_ID"]);
                        fakturaGrunnlag.Opprettet = Convert.ToDateTime(reader["FG_OPPRETTET"]);
                        fakturaGrunnlag.Referanse = Convert.ToString(reader["FG_REFERANSE"]);
                        fakturaGrunnlag.Beskrivelse = Convert.ToString(reader["FG_BESKRIVELSE"]);
                        fakturaGrunnlag.Beløp = Convert.ToDouble(reader["FG_BELOP"]);
                        fakturaGrunnlag.Mva = Convert.ToDouble(reader["FG_MVA"]);
                        fakturaGrunnlag.Sum = Convert.ToDouble(reader["FG_SUM"]);
                        fakturaGrunnlag.Status = (Common.FakturaGrunnlagStatus)Convert.ToInt32(reader["FG_STATUS"]);

                        int kundeId = Convert.ToInt32(reader["FG_KU_ID"]);
                        DbCmd_HentKunde cmd_HentKunde = new DbCmd_HentKunde(kundeId);
                        cmd_HentKunde.Execute();
                        fakturaGrunnlag.Kunde = (Common.Kunde)cmd_HentKunde.Result;

                        fakturaGrunnlagListe.Add(fakturaGrunnlag);
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = fakturaGrunnlagListe;
        }
    }
}
