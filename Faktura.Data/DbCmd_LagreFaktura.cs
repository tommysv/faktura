﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_LagreFaktura : DbCmd
    {
        private Common.Faktura faktura;
        private bool bLagreFakturaLinjer;

        public DbCmd_LagreFaktura(Common.Faktura faktura, bool bLagreFakturaLinjer = true) : base()
        {
            this.faktura = faktura;
            this.bLagreFakturaLinjer = bLagreFakturaLinjer;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@FA_ID", FbDbType.Integer),
                new FbParameter("@FA_NR", FbDbType.Integer),
                new FbParameter("@FA_DATO", FbDbType.TimeStamp),
                new FbParameter("@FA_DATO_MODIFISERT", FbDbType.TimeStamp),
                new FbParameter("@FA_DATO_FORFALL", FbDbType.TimeStamp),
                new FbParameter("@FA_DATO_INNBETALT", FbDbType.TimeStamp),
                new FbParameter("@FA_BESKRIVELSE", FbDbType.VarChar, 100),
                new FbParameter("@FA_REFERANSE", FbDbType.VarChar, 50),
                new FbParameter("@FA_KALKULERT_BELOP", FbDbType.Decimal),
                new FbParameter("@FA_KALKULERT_MVA", FbDbType.Decimal),
                new FbParameter("@FA_KALKULERT_SUM", FbDbType.Decimal),
                new FbParameter("@FA_STATUS", FbDbType.Integer),
                new FbParameter("@FA_KREDITTNOTA", FbDbType.Integer),
                new FbParameter("@FA_KREDITERT", FbDbType.Integer),
                new FbParameter("@FA_KU_ID", FbDbType.Integer)
            };
            param[0].Value = faktura.Id;
            param[1].Value = faktura.Nr;
            param[2].Value = faktura.Dato;
            param[3].Value = faktura.ModifisertDato;
            param[4].Value = faktura.ForfallsDato;
            param[5].Value = faktura.InnbetaltDato;
            param[6].Value = faktura.Beskrivelse;
            param[7].Value = faktura.Referanse;
            param[8].Value = faktura.KalkulertBeløp;
            param[9].Value = faktura.KalkulertMva;
            param[10].Value = faktura.KalkulertSum;
            param[11].Value = Convert.ToInt32(faktura.Status);
            param[12].Value = Convert.ToInt32(faktura.Kredittnota);
            param[13].Value = Convert.ToInt32(faktura.Kreditert);
            param[14].Value = faktura.Kunde.Id;
            object id = DbHelper.ExecuteScalar("UP_FAKTURA", param, Connection, Transaction);
            faktura.Id = Convert.ToInt32(id);

            if (bLagreFakturaLinjer)
            {
                for (int nLinje = 0; nLinje < faktura.Linjer.Count; nLinje++)
                {
                    if ((faktura.Linjer[nLinje].Id != -1) && (faktura.Linjer[nLinje].SlettFlagg))
                    {
                        DbCmd_SlettFakturaLinje cmd_SlettFakturaLinje = new DbCmd_SlettFakturaLinje(faktura.Linjer[nLinje].Id);
                        cmd_SlettFakturaLinje.Connection = Connection;
                        cmd_SlettFakturaLinje.Transaction = Transaction;
                        cmd_SlettFakturaLinje.Execute();
                    }
                    else
                    {
                        DbCmd_LagreFakturaLinje cmd_LagreFakturaLinje = new DbCmd_LagreFakturaLinje(faktura.Linjer[nLinje], faktura.Id);
                        cmd_LagreFakturaLinje.Connection = Connection;
                        cmd_LagreFakturaLinje.Transaction = Transaction;
                        cmd_LagreFakturaLinje.Execute();
                    }
                }
            }
        }
    }
}
