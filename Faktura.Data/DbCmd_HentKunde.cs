﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentKunde : DbCmd
    {
        private int kundeId;

        public DbCmd_HentKunde(int kundeId)
            : base()
        {
            this.kundeId = kundeId;
        }

        public override void Execute()
        {
            Common.Kunde kunde = null;

            FbDataReader reader = null;
            try
            {
                FbParameter[] param = new FbParameter[] { new FbParameter("@ID", FbDbType.Integer) };
                param[0].Value = kundeId;
                reader = DbHelper.ExecuteDataReader("SP_KUNDE", param);
                if (reader.Read())
                {
                    kunde = new Common.Kunde();
                    kunde.Id = Convert.ToInt32(reader["KU_ID"]);
                    kunde.Navn = Convert.ToString(reader["KU_NAVN"]);
                    kunde.Postadresse = Convert.ToString(reader["KU_POSTADRESSE"]);
                    kunde.PostNr = Convert.ToString(reader["KU_POSTNR"]);
                    kunde.PostSted = Convert.ToString(reader["KU_POSTSTED"]);
                    kunde.FakturaEpost = Convert.ToString(reader["KU_FAKTURAEPOST"]);
                    kunde.GrunnlagEpost = Convert.ToString(reader["KU_GRUNNLAGEPOST"]);
                    kunde.KundeType = (Common.KundeType)Convert.ToInt32(reader["KU_TYPE"]);
                    kunde.FakturaMetode = (Common.FakturaMetode)Convert.ToInt32(reader["KU_FA_METODE"]);
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = kunde;
        }
    }
}
