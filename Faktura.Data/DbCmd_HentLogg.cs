﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentLogg : DbCmd
    {
        public override void Execute()
        {
            List<Common.LoggElement> logg = new List<Common.LoggElement>();

            FbDataReader reader = null;
            try
            {
                reader = DbHelper.ExecuteDataReader("SP_LOGG", null);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Common.LoggElement loggElement = new Common.LoggElement();
                        loggElement.Dato = Convert.ToDateTime(reader["LG_DATO"]);
                        loggElement.Beskrivelse = Convert.ToString(reader["LG_BESKRIVELSE"]);
                        logg.Add(loggElement);
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = logg;
        }
    }
}
