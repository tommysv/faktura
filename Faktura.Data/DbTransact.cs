﻿using System;

using FirebirdSql.Data.FirebirdClient;
using Faktura.Config;

namespace Faktura.Data
{
    public class DbTransact : IDisposable
    {
        private string connectionString;
        private FbConnection connection;
        private FbTransaction transaction;
        private bool bCommited = false;

        public DbTransact()
        {
            connectionString = I_Konfig.Database.ConnectionString;
            connection = new FbConnection(connectionString);
            connection.Open();
            transaction = connection.BeginTransaction();
        }

        public void ExecuteCommand(DbCmd command)
        {
            command.Connection = connection;
            command.Transaction = transaction;
            command.UseTransaction = true;
            command.Execute();
        }

        public void Commit()
        {
            transaction.Commit();
            bCommited = true;
        }

        public void Dispose()
        {
            if (transaction != null)
            {
                if (!bCommited)
                {
                    transaction.Rollback();
                }
                transaction.Dispose();
                transaction = null;
            }
            if (connection != null)
            { 
                if (connection.State != System.Data.ConnectionState.Closed)
                connection.Close();
                connection.Dispose();
                connection = null;
            }
        }
    }
}
