﻿using System;
using System.Collections.Generic;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentFakturaListe : DbCmd
    {
        private Common.FakturaFilter fakturaFilter;

        public DbCmd_HentFakturaListe(Common.FakturaFilter fakturaFilter) : base()
        {
            this.fakturaFilter = fakturaFilter;
        }

        public override void Execute()
        {
            List<Common.FakturaPreview> fakturaListe = new List<Common.FakturaPreview>();

            FbDataReader reader = null;
            try
            {
                FbParameter[] param = new FbParameter[] 
                { 
                    new FbParameter("@AAR", FbDbType.Integer),
                    new FbParameter("@STATUS", FbDbType.Integer),
                    new FbParameter("@KU_ID", FbDbType.Integer)
                };
                param[0].Value = fakturaFilter.Aar;
                param[1].Value = Convert.ToInt32(fakturaFilter.Status);
                param[2].Value = Convert.ToInt32(fakturaFilter.KundeId);
                reader = DbHelper.ExecuteDataReader("SP_FAKTURALISTE", param);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Common.FakturaPreview faktura = new Common.FakturaPreview();
                        faktura.Id = Convert.ToInt32(reader["FA_ID"]);
                        faktura.Nr = Convert.ToInt32(reader["FA_NR"]);
                        faktura.Dato = DbConverter.DBNullToDateTime(reader["FA_DATO"]);
                        faktura.ModifisertDato = Convert.ToDateTime(reader["FA_DATO_MODIFISERT"]);
                        faktura.ForfallsDato = DbConverter.DBNullToDateTime(reader["FA_DATO_FORFALL"]);
                        faktura.InnbetaltDato = DbConverter.DBNullToDateTime(reader["FA_DATO_INNBETALT"]);
                        faktura.Beskrivelse = Convert.ToString(reader["FA_BESKRIVELSE"]);
                        faktura.Referanse = Convert.ToString(reader["FA_REFERANSE"]);
                        faktura.KalkulertBeløp = Convert.ToDecimal(reader["FA_KALKULERT_BELOP"]);
                        faktura.KalkulertMva = Convert.ToDecimal(reader["FA_KALKULERT_MVA"]);
                        faktura.KalkulertSum = Convert.ToDecimal(reader["FA_KALKULERT_SUM"]);
                        faktura.Status = (Common.FakturaStatus)Convert.ToInt32(reader["FA_STATUS"]);
                        faktura.Kredittnota = Convert.ToBoolean(reader["FA_KREDITTNOTA"]);
                        faktura.Kreditert = Convert.ToBoolean(reader["FA_KREDITERT"]);
                        faktura.KundeId = Convert.ToInt32(reader["FA_KU_ID"]);
                        faktura.KundeNavn = Convert.ToString(reader["KU_NAVN"]);
                        fakturaListe.Add(faktura);
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = fakturaListe;
        }
    }
}
