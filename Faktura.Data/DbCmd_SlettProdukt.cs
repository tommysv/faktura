﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_SlettProdukt : DbCmd
    {
        private int produktId;

        public DbCmd_SlettProdukt(int produktId)
            : base()
        {
            this.produktId = produktId;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@ID", FbDbType.Integer)
            };
            param[0].Value = produktId;
            DbHelper.ExecuteScalar("DP_PRODUKT", param);
        }
    }
}
