﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentFakturaGrunnlagAar : DbCmd
    {
        public DbCmd_HentFakturaGrunnlagAar()
            : base()
        {
        }

        public override void Execute()
        {
            List<int> fakturaGrunnlagAar = new List<int>();
            FbDataReader reader = null;
            try
            {
                reader = DbHelper.ExecuteDataReader("SP_FAKTURAGRUNNLAG_AAR", null);
                if (reader.HasRows)
                {
                    while (reader.Read())
                        fakturaGrunnlagAar.Add(Convert.ToInt32(reader["AAR"]));
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = fakturaGrunnlagAar.ToArray();
        }
    }
}
