﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentFakturaLinjer : DbCmd
    {
        private int fakturaGrunnlagId;

        public DbCmd_HentFakturaLinjer(int fakturaGrunnlagId)
            : base()
        {
            this.fakturaGrunnlagId = fakturaGrunnlagId;
        }


        public override void Execute()
        {
            List<Common.FakturaLinje> fakturaLinjer = new List<Common.FakturaLinje>();

            FbDataReader reader = null;
            try
            {
                FbParameter[] param = new FbParameter[] { new FbParameter("@FG_ID", FbDbType.Integer) };
                param[0].Value = fakturaGrunnlagId;
                reader = DbHelper.ExecuteDataReader("SP_FAKTURALINJELISTE", param);
                while (reader.Read())
                {
                    Common.FakturaLinje fakturaLinje = new Common.FakturaLinje();
                    fakturaLinje.Id = Convert.ToInt32(reader["FL_ID"]);
                    fakturaLinje.ProduktKode = Convert.ToString(reader["FL_PRODUKTKODE"]);
                    fakturaLinje.Beskrivelse = Convert.ToString(reader["FL_BESKRIVELSE"]);
                    fakturaLinje.Ant = Convert.ToDouble(reader["FL_ANTALL"]);
                    fakturaLinje.Enhet = Convert.ToString(reader["FL_ENHET"]);
                    fakturaLinje.EnhetsPris = Convert.ToDouble(reader["FL_ENHETSPRIS"]);
                    fakturaLinje.KalkulertBeløp = Convert.ToDouble(reader["FL_KALKULERT_BELOP"]);
                    fakturaLinje.KalkulertMva = Convert.ToDouble(reader["FL_KALKULERT_MVA"]);
                    fakturaLinje.KalkulertSum = Convert.ToDouble(reader["FL_KALKULERT_SUM"]);
                    fakturaLinjer.Add(fakturaLinje);
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            this.Result = fakturaLinjer;
        }
    }
}
