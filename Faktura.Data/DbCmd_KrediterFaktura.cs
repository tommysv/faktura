﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_KrediterFaktura : DbCmd
    {
        private Common.Faktura faktura;

        public DbCmd_KrediterFaktura(Common.Faktura faktura)
            : base()
        {
            this.faktura = faktura;
        }

        public override void Execute()
        {
            DbCmd_LagreFaktura cmd_LagreFaktura;

            if (faktura.Status == Common.FakturaStatus.Sendt)
            {
                faktura.Status = Common.FakturaStatus.Innbetalt;
                faktura.InnbetaltDato = DateTime.Now;
            }
            faktura.Kreditert = true;
            faktura.Kredittnota = false;

            cmd_LagreFaktura = new DbCmd_LagreFaktura(faktura, false);
            cmd_LagreFaktura.Connection = Connection;
            cmd_LagreFaktura.Transaction = Transaction;
            cmd_LagreFaktura.Execute();

            faktura.Id = -1;
            for (int nLinje = 0; nLinje < faktura.Linjer.Count; nLinje++)
            {
                faktura.Linjer[nLinje].Id = -1;
                faktura.Linjer[nLinje].Ant = -faktura.Linjer[nLinje].Ant;
                faktura.Linjer[nLinje].KalkulertBeløp = -faktura.Linjer[nLinje].KalkulertBeløp;
                faktura.Linjer[nLinje].KalkulertMva = -faktura.Linjer[nLinje].KalkulertMva;
                faktura.Linjer[nLinje].KalkulertSum = -faktura.Linjer[nLinje].KalkulertSum;
            }
            faktura.KalkulertBeløp = -faktura.KalkulertBeløp;
            faktura.KalkulertMva = -faktura.KalkulertMva;
            faktura.KalkulertSum = -faktura.KalkulertSum;

            DateTime dateTime = DateTime.Now;
            
            DbCmd_GenererFakturaNr cmd_GenererFakturaNr = new DbCmd_GenererFakturaNr(dateTime.Year);
            cmd_GenererFakturaNr.Connection = Connection;
            cmd_GenererFakturaNr.Transaction = Transaction;
            cmd_GenererFakturaNr.Execute();

            faktura.Nr = Convert.ToInt32(cmd_GenererFakturaNr.Result);
            faktura.Dato = dateTime;
            faktura.ForfallsDato = null;
            faktura.Status = Common.FakturaStatus.Innbetalt;
            faktura.Kreditert = false;
            faktura.Kredittnota = true;

            cmd_LagreFaktura = new DbCmd_LagreFaktura(faktura);
            cmd_LagreFaktura.Connection = Connection;
            cmd_LagreFaktura.Transaction = Transaction;
            cmd_LagreFaktura.Execute();
        }
    }
}
