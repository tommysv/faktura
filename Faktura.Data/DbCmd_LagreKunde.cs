﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_LagreKunde : DbCmd
    {
        private Common.Kunde kunde;

        public DbCmd_LagreKunde(Common.Kunde kunde)
            : base()
        {
            this.kunde = kunde;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@KU_ID", FbDbType.Integer),
                new FbParameter("@KU_NAVN", FbDbType.VarChar, 100),
                new FbParameter("@KU_POSTADRESSE", FbDbType.VarChar, 100),
                new FbParameter("@KU_POSTNR", FbDbType.VarChar, 4),
                new FbParameter("@KU_POSTSTED", FbDbType.VarChar, 100),
                new FbParameter("@KU_FAKTURAEPOST", FbDbType.VarChar, 200),
                new FbParameter("@KU_GRUNNLAGEPOST", FbDbType.VarChar, 200),
                new FbParameter("@KU_TYPE", FbDbType.Integer),
                new FbParameter("@KU_FA_METODE", FbDbType.Integer)
            };
            param[0].Value = kunde.Id;
            param[1].Value = kunde.Navn;
            param[2].Value = kunde.Postadresse;
            param[3].Value = kunde.PostNr;
            param[4].Value = kunde.PostSted;
            param[5].Value = kunde.FakturaEpost;
            param[6].Value = kunde.GrunnlagEpost;
            param[7].Value = kunde.KundeType;
            param[8].Value = kunde.FakturaMetode;
            DbHelper.ExecuteScalar("UP_KUNDE", param);
        }
    }
}
