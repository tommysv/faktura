﻿using System;
using System.Collections.Generic;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentProduktListe : DbCmd
    {
        public DbCmd_HentProduktListe()
            : base()
        {
        }

        public override void Execute()
        {
            List<Common.Produkt> produktListe = new List<Common.Produkt>();

            FbDataReader reader = null;
            try
            {
                reader = DbHelper.ExecuteDataReader("SP_PRODUKTLISTE", null);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Common.Produkt produkt = new Common.Produkt();
                        produkt.Id = Convert.ToInt32(reader["PR_ID"]);
                        produkt.ProduktKode = Convert.ToString(reader["PR_PRODUKTKODE"]);
                        produkt.Beskrivelse = Convert.ToString(reader["PR_BESKRIVELSE"]);
                        produkt.Enhet = Convert.ToString(reader["PR_ENHET"]);
                        produkt.EnhetsPris = Convert.ToDouble(reader["PR_ENHETSPRIS"]);
                        produkt.MvaSats = Convert.ToDouble(reader["PR_MVASATS"]);
                        produktListe.Add(produkt);
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = produktListe;
        }
    }
}
