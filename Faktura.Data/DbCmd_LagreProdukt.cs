﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_LagreProdukt : DbCmd
    {
        private Common.Produkt produkt;

        public DbCmd_LagreProdukt(Common.Produkt produkt)
            : base()
        {
            this.produkt = produkt;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@PR_ID", FbDbType.Integer),
                new FbParameter("@PR_PRODUKTKODE", FbDbType.VarChar, 6),
                new FbParameter("@PR_BESKRIVELSE", FbDbType.VarChar, 100),
                new FbParameter("@PR_ENHET", FbDbType.VarChar, 10),
                new FbParameter("@PR_ENHETSPRIS", FbDbType.Double),
                new FbParameter("@PR_MVASATS", FbDbType.Double)
            };
            param[0].Value = produkt.Id;
            param[1].Value = produkt.ProduktKode;
            param[2].Value = produkt.Beskrivelse;
            param[3].Value = produkt.Enhet;
            param[4].Value = produkt.EnhetsPris;
            param[5].Value = produkt.MvaSats;

            DbHelper.ExecuteScalar("UP_PRODUKT", param);
        }
    }
}
