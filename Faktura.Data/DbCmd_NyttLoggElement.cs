﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_NyttLoggElement : DbCmd
    {
        private Common.LoggElement loggElement;

        public DbCmd_NyttLoggElement(Common.LoggElement loggElement)
            : base()
        {
            this.loggElement = loggElement;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@LG_DATO", FbDbType.TimeStamp),
                new FbParameter("@LG_BESKRIVELSE", FbDbType.VarChar, 100)
            };
            param[0].Value = loggElement.Dato;
            param[1].Value = loggElement.Beskrivelse;
            DbHelper.ExecuteScalar("CP_LOGG_ELEMENT", param, Connection, Transaction);
        }
    }
}
