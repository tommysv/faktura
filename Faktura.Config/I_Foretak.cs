﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Config
{
    public class I_Foretak
    {
        private string orgNr;
        private string navn;
        private bool mvaPliktig;
        private string adresse1;
        private string adresse2;
        private string postNr;
        private string poststed;
        private string epost;
        private string telefon;
        private string nettsted;
        private string kontoNr;
        private int forfallsdager;
        private double fakturagebyr;

        public I_Foretak()
        {
            orgNr = Config.Instance().AppSettings["Foretak_OrgNr"];
            navn = Config.Instance().AppSettings["Foretak_Navn"];
            mvaPliktig = Convert.ToBoolean(
                Config.Instance().AppSettings["Foretak_MvaPliktig"]);
            adresse1 = Config.Instance().AppSettings["Foretak_Adresse1"];
            adresse2 = Config.Instance().AppSettings["Foretak_Adresse2"];
            postNr = Config.Instance().AppSettings["Foretak_Postnr"];
            poststed = Config.Instance().AppSettings["Foretak_Poststed"];
            epost = Config.Instance().AppSettings["Foretak_Epost"];
            telefon = Config.Instance().AppSettings["Foretak_Telefon"];
            nettsted = Config.Instance().AppSettings["Foretak_Nettsted"];
            kontoNr = Config.Instance().AppSettings["Foretak_KontoNr"];
            forfallsdager = Convert.ToInt32(
                Config.Instance().AppSettings["Foretak_Forfallsdager"]);
            fakturagebyr = Convert.ToDouble(
                Config.Instance().AppSettings["Foretak_Fakturagebyr"]);
        }

        public string OrgNr
        {
            get { return orgNr; }
            set 
            {
                orgNr = value;
                Config.Instance().AppSettings["Foretak_OrgNr"] = orgNr; 
            }
        }

        public string Navn
        {
            get { return navn; }
            set 
            {
                navn = value;
                Config.Instance().AppSettings["Foretak_Navn"] = navn;
            }
        }

        public bool MvaPliktig
        {
            get { return mvaPliktig; }
            set 
            {
                mvaPliktig = value;
                Config.Instance().AppSettings["Foretak_MvaPliktig"] = Convert.ToString(mvaPliktig);
            }
        }

        public string Adresse1
        {
            get { return adresse1; }
            set 
            {
                adresse1 = value;
                Config.Instance().AppSettings["Foretak_Adresse1"] = adresse1;
            }
        }

        public string Adresse2
        {
            get { return adresse2; }
            set
            {
                adresse2 = value;
                Config.Instance().AppSettings["Foretak_Adresse2"] = adresse2;
            }
        }

        public string PostNr
        {
            get { return postNr; }
            set
            {
                postNr = value;
                Config.Instance().AppSettings["Foretak_Postnr"] = postNr;
            }
        }

        public string Poststed
        {
            get { return poststed; }
            set
            {
                poststed = value;
                Config.Instance().AppSettings["Foretak_Poststed"] = poststed;
            }
        }

        public string Epost
        {
            get { return epost; }
            set
            {
                epost = value;
                Config.Instance().AppSettings["Foretak_Epost"] = epost;
            }
        }

        public string Telefon
        {
            get { return telefon; }
            set
            {
                telefon = value;
                Config.Instance().AppSettings["Foretak_Telefon"] = telefon;
            }
        }

        public string Nettsted
        {
            get { return nettsted; }
            set
            {
                nettsted = value;
                Config.Instance().AppSettings["Foretak_Nettsted"] = nettsted;
            }
        }

        public string KontoNr
        {
            get { return kontoNr; }
            set
            {
                kontoNr = value;
                Config.Instance().AppSettings["Foretak_KontoNr"] = kontoNr;
            }
        }

        public int Forfallsdager
        {
            get { return forfallsdager; }
            set
            {
                forfallsdager = value;
                Config.Instance().AppSettings["Foretak_Forfallsdager"] = Convert.ToString(forfallsdager);
            }
        }

        public double Fakturagebyr
        {
            get { return fakturagebyr; }
            set
            {
                fakturagebyr = value;
                Config.Instance().AppSettings["Foretak_Fakturagebyr"] = Convert.ToString(fakturagebyr);
            }
        }
    }
}
