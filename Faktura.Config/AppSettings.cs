﻿using System;
using System.Configuration;

namespace Faktura.Config
{
    public class AppSettings
    {
        private Configuration config;
        
        public AppSettings(Configuration config)
        {
            this.config = config;
        }

        public string this[string key]
        {
            get
            {
                return config.AppSettings.Settings[key].Value;
            }
            set
            {
                config.AppSettings.Settings[key].Value = value;
            }
        }
    }
}
