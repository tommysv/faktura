﻿using System;
using System.Drawing;

namespace Faktura.Config
{
    public class I_UI
    {
        private Color behandlerBakgrunn;
        private Color behandlerForgrunn;
        private Color sendtBakgrunn;
        private Color sendtForgrunn;
        private Color kreditertBakgrunn;
        private Color kreditertForgrunn;
        private Color innbetaltBakgrunn;
        private Color innbetaltForgrunn;

        public I_UI()
        {
            behandlerBakgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_BehandlerBakgrunn"]);
            behandlerForgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_BehandlerForgrunn"]);
            sendtBakgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_SendtBakgrunn"]);
            sendtForgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_SendtForgrunn"]);
            kreditertBakgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_KreditertBakgrunn"]);
            kreditertForgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_KreditertForgrunn"]);
            innbetaltBakgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_InnbetaltBakgrunn"]);
            innbetaltForgrunn = ColorTranslator.FromHtml(
                Config.Instance().AppSettings["UI_InnbetaltForgrunn"]);
        }

        public Color BehandlerBakgrunn
        {
            get { return behandlerBakgrunn; }
            set
            {
                behandlerBakgrunn = value;
                Config.Instance().AppSettings["UI_BehandlerBakgrunn"] = ColorTranslator.ToHtml(behandlerBakgrunn);
            }
        }

        public Color BehandlerForgrunn
        {
            get { return behandlerForgrunn; }
            set
            {
                behandlerForgrunn = value;
                Config.Instance().AppSettings["UI_BehandlerForgrunn"] = ColorTranslator.ToHtml(behandlerForgrunn);
            }
        }

        public Color SendtBakgrunn
        {
            get { return sendtBakgrunn; }
            set
            {
                sendtBakgrunn = value;
                Config.Instance().AppSettings["UI_SendtBakgrunn"] = ColorTranslator.ToHtml(sendtBakgrunn);
            }
        }

        public Color SendtForgrunn
        {
            get { return sendtForgrunn; }
            set
            {
                sendtForgrunn = value;
                Config.Instance().AppSettings["UI_SendtForgrunn"] = ColorTranslator.ToHtml(sendtForgrunn);
            }
        }

        public Color KreditertBakgrunn
        {
            get { return kreditertBakgrunn; }
            set
            {
                kreditertBakgrunn = value;
                Config.Instance().AppSettings["UI_KreditertBakgrunn"] = ColorTranslator.ToHtml(kreditertBakgrunn);
            }
        }

        public Color KreditertForgrunn
        {
            get { return kreditertForgrunn; }
            set
            {
                kreditertForgrunn = value;
                Config.Instance().AppSettings["UI_KreditertForgrunn"] = ColorTranslator.ToHtml(kreditertForgrunn);
            }
        }

        public Color InnbetaltBakgrunn
        {
            get { return innbetaltBakgrunn; }
            set
            {
                innbetaltBakgrunn = value;
                Config.Instance().AppSettings["UI_InnbetaltBakgrunn"] = ColorTranslator.ToHtml(innbetaltBakgrunn);
            }
        }

        public Color InnbetaltForgrunn
        {
            get { return innbetaltForgrunn; }
            set
            {
                innbetaltForgrunn = value;
                Config.Instance().AppSettings["UI_InnbetaltForgrunn"] = ColorTranslator.ToHtml(innbetaltForgrunn);
            }
        }
    }
}
