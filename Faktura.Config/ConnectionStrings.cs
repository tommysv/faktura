﻿using System;
using System.Configuration;

namespace Faktura.Config
{
    public class ConnectionStrings
    {
        private Configuration config;

        public ConnectionStrings(Configuration config)
        {
            this.config = config;
        }

        public string this[string name]
        {
            get
            {
                return config.ConnectionStrings.ConnectionStrings[name].ConnectionString;
            }
            set
            {
                config.ConnectionStrings.ConnectionStrings[name].ConnectionString = value;
            }
        }
    }
}
