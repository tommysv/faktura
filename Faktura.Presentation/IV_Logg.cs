﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_Logg : IView, IV_Oppdaterbar
    {
        IList<Common.LoggElement> Logg { set; }
    }
}
