﻿using System;

using Faktura.Model;

namespace Faktura.Presentation
{
    public class Presenter<T> where T : IView
    {
        protected static IModel Model { get; private set; }

        static Presenter()
        {
            Model = new Model.Model();
            System.Diagnostics.Debug.WriteLine("Static contructor for Presenter<T> loaded");
        }

        public Presenter(T view)
        {
            View = view;
            System.Diagnostics.Debug.WriteLine(
                String.Format("Dynamic contructor for {0} loaded", typeof(T).ToString()));
        }

        protected T View { get; private set; }
    }
}
