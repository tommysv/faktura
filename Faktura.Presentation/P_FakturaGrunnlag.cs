﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Common;

namespace Faktura.Presentation
{
    public class P_FakturaGrunnlag : Presenter<IV_FakturaGrunnlag>
    {
        public P_FakturaGrunnlag(IV_FakturaGrunnlag view) : base(view)
        {
        }

        public void Init(int fakturaGrunnlagId)
        {
            View.KundeListe = Model.HentKundeListe();
            View.ProduktListe = Model.HentProduktListe();
            if (fakturaGrunnlagId != -1)
            {
                View.FakturaGrunnlag = Model.HentFakturaGrunnlag(fakturaGrunnlagId);
            }
            else
            {
                Common.FakturaGrunnlag fakturaGrunnlag = new Common.FakturaGrunnlag();
                View.FakturaGrunnlag = fakturaGrunnlag;
            }
        }

        public void Kalkuler()
        {
            Common.FakturaGrunnlag fakturaGrunnlag = View.FakturaGrunnlag;
            double beløp = 0;
            double mva = 0;
            double sum = 0;

            for (int nLinje = 0; nLinje < fakturaGrunnlag.Linjer.Count; nLinje++)
            {
                if (!fakturaGrunnlag.Linjer[nLinje].SlettFlagg)
                {
                    beløp += fakturaGrunnlag.Linjer[nLinje].KalkulertBeløp;
                    mva += fakturaGrunnlag.Linjer[nLinje].KalkulertMva;
                    sum += fakturaGrunnlag.Linjer[nLinje].KalkulertSum;
                }
            }
            fakturaGrunnlag.Beløp = beløp;
            fakturaGrunnlag.Mva = mva;
            fakturaGrunnlag.Sum = sum;
        }

        public void LagreFaktura()
        {
            Model.LagreFakturaGrunnlag(View.FakturaGrunnlag);
        }
    }
}
