﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_FakturaGrunnlagListe : IView, IV_Oppdaterbar
    {
        int[] Aar { set; }
        IList<Common.Kunde> KundeListe { set; }
        Common.FakturaGrunnlagFilter Filter { get; }
        IList<Common.FakturaGrunnlag> FakturaGrunnlagListe { set; }
    }
}
