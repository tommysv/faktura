﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Model;
using Faktura.Common;

namespace Faktura.Presentation
{
    public class P_Rapport : Presenter<IV_Rapport>
    {
        public P_Rapport(IV_Rapport view)
            : base(view)
        {
        }

        public void Init()
        {
            View.RapportÅr = Model.HentFakturaAar();
        }

        public void GenererRapport(int år)
        {
            View.RapportFil = Model.GenererRapport(år);
        }
    }
}
