﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Model;

namespace Faktura.Presentation
{
    public class P_ProduktListe : Presenter<IV_ProduktListe>
    {
        public P_ProduktListe(IV_ProduktListe view)
            : base(view)
        {
        }

        public void HentProduktListe()
        {
            View.ProduktListe = Model.HentProduktListe();
        }

        public void SlettProdukt()
        {
            Model.SlettProdukt(View.ValgtProdukt.Id);
        }
    }
}
