﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Common;
using Faktura.Config;

namespace Faktura.Presentation
{
    public class P_FakturaGrunnlagListe : Presenter<IV_FakturaGrunnlagListe>
    {
        public P_FakturaGrunnlagListe(IV_FakturaGrunnlagListe view)
            : base(view)
        {
        }

        public void HentFakturaGrunnlagAar()
        {
            View.Aar = Model.HentFakturaGrunnlagAar();
        }

        public void HentKundeListe()
        {
            View.KundeListe = Model.HentKundeListe();
        }

        public void GodkjennFakturaGrunnlag(int fakturaGrunnlagId)
        {
            Model.GodkjennFakturaGrunnlag(fakturaGrunnlagId);
        }

        public void SlettFakturaGrunnlag(int fakturaGrunnlagId)
        {
            Model.SlettFakturaGrunnlag(fakturaGrunnlagId);
        }

        public void SendFakturaGrunnlag(int fakturaGrunnlagId)
        {
            Model.SendFakturaGrunnlag(fakturaGrunnlagId);
        }

        public void HentFakturaGrunnlagListe()
        {
            Common.FakturaGrunnlagFilter filter = View.Filter;
            View.FakturaGrunnlagListe = Model.HentFakturaGrunnlagListe(filter);
        }
    }
}
