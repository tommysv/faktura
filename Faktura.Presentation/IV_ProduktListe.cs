﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_ProduktListe : IView, IV_Oppdaterbar
    {
        IList<Produkt> ProduktListe { set; }
        Produkt ValgtProdukt { get; }
    }
}
