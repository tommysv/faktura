﻿using System;

namespace Faktura.Presentation
{
    public interface IPrintable
    {
        void Print();
    }
}
