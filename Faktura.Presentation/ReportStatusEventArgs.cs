﻿using System;

namespace Faktura.Presentation
{
    public class ReportStatusEventArgs : System.EventArgs
    {
        private string text;
        private bool isError;

        public ReportStatusEventArgs(string text, bool isError = false)
        {
            this.text = text;
            this.isError = isError;
        }

        public string Text
        {
            get 
            { 
                return text;
            }
        }

        public bool IsError
        {
            get
            {
                return isError;
            }
        }
    }
}
