﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Model;
using Faktura.Common;

namespace Faktura.Presentation
{
    public class P_Produkt : Presenter<IV_Produkt>
    {
        public P_Produkt(IV_Produkt view)
            : base(view)
        {
        }

        public void Init()
        {
            View.Produkt = new Produkt();
        }

        public void Lagre()
        {
            Model.LagreProdukt(View.Produkt);
        }
    }
}
