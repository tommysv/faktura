﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_FakturaLinje : IView
    {
        FakturaLinje FakturaLinje { get; set; }
        IList<Produkt> ProduktListe { get; set; }
    }
}
