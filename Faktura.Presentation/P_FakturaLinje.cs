﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Common;

namespace Faktura.Presentation
{
    public class P_FakturaLinje
    {
        private IV_FakturaLinje view;

        public P_FakturaLinje(IV_FakturaLinje view)
        {
            this.view = view;
        }

        public void SettProduktKode(string produktKode)
        {
            FakturaLinje fakturaLinje = view.FakturaLinje;
            IList<Produkt> produktListe = view.ProduktListe;
            for (int n = 0; n < produktListe.Count; n++)
            {
                if (produktListe[n].ProduktKode.Equals(produktKode))
                {
                    fakturaLinje.ProduktKode = produktKode;
                    fakturaLinje.Beskrivelse = produktListe[n].Beskrivelse;
                    fakturaLinje.Enhet = produktListe[n].Enhet;
                    fakturaLinje.EnhetsPris = produktListe[n].EnhetsPris;
                    view.FakturaLinje = fakturaLinje;
                    break;
                }
            }
        }

        public void Kalkuler()
        {
            FakturaLinje fakturaLinje = view.FakturaLinje;
            double ant = fakturaLinje.Ant;
            double enhetsPris = fakturaLinje.EnhetsPris;
            double rabattProsent = (fakturaLinje.RabattProsent < 0) ? -fakturaLinje.RabattProsent : -fakturaLinje.RabattProsent;
            double beløp = Math.Round(ant * enhetsPris * (1 - (rabattProsent / 100)), 1);
            double mva = Math.Round(((beløp * 25) / 100), 1);
            double sum = beløp + mva;
            fakturaLinje.KalkulertBeløp = beløp;
            fakturaLinje.KalkulertMva = mva;
            fakturaLinje.KalkulertSum = sum;
            view.FakturaLinje = fakturaLinje;
        }
    }
}
