﻿using System;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_Produkt : IView
    {
        Produkt Produkt { get; set; }
    }
}
