﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_KundeListe : IView, IV_Oppdaterbar
    {
        IList<Kunde> KundeListe { set; }
    }
}
