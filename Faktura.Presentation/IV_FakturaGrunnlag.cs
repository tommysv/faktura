﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_FakturaGrunnlag : IView
    {
        IList<Common.Kunde> KundeListe { set; }
        IList<Produkt> ProduktListe { set; }
        Common.FakturaGrunnlag FakturaGrunnlag { get; set; }  
    }
}
