﻿namespace Faktura
{
    partial class V_Logg
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lwLogg = new Faktura.ListViewEx();
            this.colDato = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBeskrivelse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hentLoggBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.tsLogg = new System.Windows.Forms.ToolStrip();
            this.tsBtnSlettLogg = new System.Windows.Forms.ToolStripButton();
            this.slettLoggBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.tsLogg.SuspendLayout();
            this.SuspendLayout();
            // 
            // lwLogg
            // 
            this.lwLogg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lwLogg.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDato,
            this.colTid,
            this.colBeskrivelse});
            this.lwLogg.ColumnWeights = null;
            this.lwLogg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwLogg.FullRowSelect = true;
            this.lwLogg.Location = new System.Drawing.Point(0, 0);
            this.lwLogg.MultiSelect = false;
            this.lwLogg.Name = "lwLogg";
            this.lwLogg.OwnerDraw = true;
            this.lwLogg.Size = new System.Drawing.Size(651, 458);
            this.lwLogg.TabIndex = 0;
            this.lwLogg.UseCompatibleStateImageBehavior = false;
            this.lwLogg.View = System.Windows.Forms.View.Details;
            // 
            // colDato
            // 
            this.colDato.Text = "Dato";
            // 
            // colTid
            // 
            this.colTid.Text = "Tid";
            // 
            // colBeskrivelse
            // 
            this.colBeskrivelse.Text = "Beskrivelse";
            // 
            // hentLoggBackgroundTask
            // 
            this.hentLoggBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.hentLoggBackgroundTask_DoWork);
            this.hentLoggBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.hentLoggBackgroundTask_RunWorkerCompleted);
            // 
            // tsLogg
            // 
            this.tsLogg.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnSlettLogg});
            this.tsLogg.Location = new System.Drawing.Point(0, 0);
            this.tsLogg.Name = "tsLogg";
            this.tsLogg.Size = new System.Drawing.Size(655, 37);
            this.tsLogg.TabIndex = 1;
            this.tsLogg.Visible = false;
            // 
            // tsBtnSlettLogg
            // 
            this.tsBtnSlettLogg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSlettLogg.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnSlettLogg.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnSlettLogg.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSlettLogg.Name = "tsBtnSlettLogg";
            this.tsBtnSlettLogg.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnSlettLogg.Size = new System.Drawing.Size(34, 34);
            this.tsBtnSlettLogg.ToolTipText = "Slett logg";
            this.tsBtnSlettLogg.Click += new System.EventHandler(this.tsBtnSlettLogg_Click);
            // 
            // slettLoggBackgroundTask
            // 
            this.slettLoggBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.slettLoggBackgroundTask_DoWork);
            this.slettLoggBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.slettLoggBackgroundTask_RunWorkerCompleted);
            // 
            // V_Logg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.lwLogg);
            this.Controls.Add(this.tsLogg);
            this.DoubleBuffered = true;
            this.Name = "V_Logg";
            this.Size = new System.Drawing.Size(651, 458);
            this.Load += new System.EventHandler(this.V_Logg_Load);
            this.VisibleChanged += new System.EventHandler(this.V_Logg_VisibleChanged);
            this.tsLogg.ResumeLayout(false);
            this.tsLogg.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ListViewEx lwLogg;
        private System.Windows.Forms.ColumnHeader colDato;
        private System.Windows.Forms.ColumnHeader colTid;
        private System.Windows.Forms.ColumnHeader colBeskrivelse;
        private System.ComponentModel.BackgroundWorker hentLoggBackgroundTask;
        private System.Windows.Forms.ToolStrip tsLogg;
        private System.Windows.Forms.ToolStripButton tsBtnSlettLogg;
        private System.ComponentModel.BackgroundWorker slettLoggBackgroundTask;

    }
}
