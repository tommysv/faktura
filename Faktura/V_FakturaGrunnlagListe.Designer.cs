﻿using Faktura.Presentation;

namespace Faktura
{
    partial class V_FakturaGrunnlagListe : IV_FakturaGrunnlagListe
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(V_FakturaGrunnlagListe));
            this.tsFakturaGrunnlag = new System.Windows.Forms.ToolStrip();
            this.tsBtnNyttFakturaGrunnlag = new System.Windows.Forms.ToolStripButton();
            this.tsBtnRedigerFakturaGrunnlag = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSlettFakturaGrunnlag = new System.Windows.Forms.ToolStripButton();
            this.tsBtnGodkjennFakturaGrunnlag = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSendFakturaGrunnlag = new System.Windows.Forms.ToolStripButton();
            this.tsBtnLagFaktura = new System.Windows.Forms.ToolStripButton();
            this.tsBtnFilter = new System.Windows.Forms.ToolStripButton();
            this.tsFilter = new System.Windows.Forms.ToolStrip();
            this.hentFakturaGrunnlagListeTask = new System.ComponentModel.BackgroundWorker();
            this.slettFakturaGrunnlagTask = new System.ComponentModel.BackgroundWorker();
            this.lwFakturaGrunnlag = new Faktura.ListViewEx();
            this.colOpprettetDato = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMottaker = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colReferanse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBeskrivelse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBeløp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMva = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tsFakturaGrunnlag.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsFakturaGrunnlag
            // 
            this.tsFakturaGrunnlag.BackColor = System.Drawing.Color.GhostWhite;
            this.tsFakturaGrunnlag.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnNyttFakturaGrunnlag,
            this.tsBtnRedigerFakturaGrunnlag,
            this.tsBtnSlettFakturaGrunnlag,
            this.tsBtnGodkjennFakturaGrunnlag,
            this.tsBtnSendFakturaGrunnlag,
            this.tsBtnLagFaktura,
            this.tsBtnFilter});
            this.tsFakturaGrunnlag.Location = new System.Drawing.Point(0, 0);
            this.tsFakturaGrunnlag.Name = "tsFakturaGrunnlag";
            this.tsFakturaGrunnlag.Size = new System.Drawing.Size(647, 37);
            this.tsFakturaGrunnlag.TabIndex = 0;
            this.tsFakturaGrunnlag.Visible = false;
            // 
            // tsBtnNyttFakturaGrunnlag
            // 
            this.tsBtnNyttFakturaGrunnlag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnNyttFakturaGrunnlag.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnNyttFakturaGrunnlag.Image")));
            this.tsBtnNyttFakturaGrunnlag.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnNyttFakturaGrunnlag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnNyttFakturaGrunnlag.Name = "tsBtnNyttFakturaGrunnlag";
            this.tsBtnNyttFakturaGrunnlag.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnNyttFakturaGrunnlag.Size = new System.Drawing.Size(34, 34);
            this.tsBtnNyttFakturaGrunnlag.Text = "Nytt fakturagrunnlag";
            this.tsBtnNyttFakturaGrunnlag.Click += new System.EventHandler(this.tsBtnNyttFakturaGrunnlag_Click);
            // 
            // tsBtnRedigerFakturaGrunnlag
            // 
            this.tsBtnRedigerFakturaGrunnlag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnRedigerFakturaGrunnlag.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnRedigerFakturaGrunnlag.Image")));
            this.tsBtnRedigerFakturaGrunnlag.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnRedigerFakturaGrunnlag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnRedigerFakturaGrunnlag.Name = "tsBtnRedigerFakturaGrunnlag";
            this.tsBtnRedigerFakturaGrunnlag.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnRedigerFakturaGrunnlag.Size = new System.Drawing.Size(34, 34);
            this.tsBtnRedigerFakturaGrunnlag.Text = "Rediger fakturagrunnlag";
            this.tsBtnRedigerFakturaGrunnlag.Click += new System.EventHandler(this.tsBtnRedigerFakturaGrunnlag_Click);
            // 
            // tsBtnSlettFakturaGrunnlag
            // 
            this.tsBtnSlettFakturaGrunnlag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSlettFakturaGrunnlag.Image = global::Faktura.Properties.Resources.invoice_delete;
            this.tsBtnSlettFakturaGrunnlag.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnSlettFakturaGrunnlag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSlettFakturaGrunnlag.Name = "tsBtnSlettFakturaGrunnlag";
            this.tsBtnSlettFakturaGrunnlag.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnSlettFakturaGrunnlag.Size = new System.Drawing.Size(34, 34);
            this.tsBtnSlettFakturaGrunnlag.Text = "Slett fakturagrunnlag";
            this.tsBtnSlettFakturaGrunnlag.Click += new System.EventHandler(this.tsBtnSlettFakturaGrunnlag_Click);
            // 
            // tsBtnGodkjennFakturaGrunnlag
            // 
            this.tsBtnGodkjennFakturaGrunnlag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnGodkjennFakturaGrunnlag.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnGodkjennFakturaGrunnlag.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnGodkjennFakturaGrunnlag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnGodkjennFakturaGrunnlag.Name = "tsBtnGodkjennFakturaGrunnlag";
            this.tsBtnGodkjennFakturaGrunnlag.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnGodkjennFakturaGrunnlag.Size = new System.Drawing.Size(34, 34);
            this.tsBtnGodkjennFakturaGrunnlag.Text = "Godkjenn fakturagrunnlag";
            // 
            // tsBtnSendFakturaGrunnlag
            // 
            this.tsBtnSendFakturaGrunnlag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSendFakturaGrunnlag.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnSendFakturaGrunnlag.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnSendFakturaGrunnlag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSendFakturaGrunnlag.Name = "tsBtnSendFakturaGrunnlag";
            this.tsBtnSendFakturaGrunnlag.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnSendFakturaGrunnlag.Size = new System.Drawing.Size(34, 34);
            this.tsBtnSendFakturaGrunnlag.Text = "Send fakturagrunnlag";
            // 
            // tsBtnLagFaktura
            // 
            this.tsBtnLagFaktura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnLagFaktura.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnLagFaktura.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnLagFaktura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnLagFaktura.Name = "tsBtnLagFaktura";
            this.tsBtnLagFaktura.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnLagFaktura.Size = new System.Drawing.Size(34, 34);
            this.tsBtnLagFaktura.Text = "Lag faktura";
            // 
            // tsBtnFilter
            // 
            this.tsBtnFilter.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsBtnFilter.CheckOnClick = true;
            this.tsBtnFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnFilter.Image = global::Faktura.Properties.Resources.filter;
            this.tsBtnFilter.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnFilter.Name = "tsBtnFilter";
            this.tsBtnFilter.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnFilter.Size = new System.Drawing.Size(34, 34);
            this.tsBtnFilter.CheckedChanged += new System.EventHandler(this.tsBtnFilter_CheckedChanged);
            // 
            // tsFilter
            // 
            this.tsFilter.BackColor = System.Drawing.Color.GhostWhite;
            this.tsFilter.Location = new System.Drawing.Point(0, 0);
            this.tsFilter.Name = "tsFilter";
            this.tsFilter.Size = new System.Drawing.Size(647, 25);
            this.tsFilter.TabIndex = 2;
            this.tsFilter.Visible = false;
            // 
            // hentFakturaGrunnlagListeTask
            // 
            this.hentFakturaGrunnlagListeTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.hentFakturaGrunnlagListeTask_DoWork);
            this.hentFakturaGrunnlagListeTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.hentFakturaGrunnlagListeTask_RunWorkerCompleted);
            // 
            // slettFakturaGrunnlagTask
            // 
            this.slettFakturaGrunnlagTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.slettFakturaGrunnlagTask_DoWork);
            this.slettFakturaGrunnlagTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.slettFakturaGrunnlagTask_RunWorkerCompleted);
            // 
            // lwFakturaGrunnlag
            // 
            this.lwFakturaGrunnlag.BackColor = System.Drawing.Color.White;
            this.lwFakturaGrunnlag.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lwFakturaGrunnlag.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colOpprettetDato,
            this.colMottaker,
            this.colReferanse,
            this.colBeskrivelse,
            this.colBeløp,
            this.colMva,
            this.colSum,
            this.colStatus});
            this.lwFakturaGrunnlag.ColumnWeights = null;
            this.lwFakturaGrunnlag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwFakturaGrunnlag.FullRowSelect = true;
            this.lwFakturaGrunnlag.Location = new System.Drawing.Point(0, 37);
            this.lwFakturaGrunnlag.Name = "lwFakturaGrunnlag";
            this.lwFakturaGrunnlag.OwnerDraw = true;
            this.lwFakturaGrunnlag.Size = new System.Drawing.Size(647, 417);
            this.lwFakturaGrunnlag.TabIndex = 3;
            this.lwFakturaGrunnlag.UseCompatibleStateImageBehavior = false;
            this.lwFakturaGrunnlag.View = System.Windows.Forms.View.Details;
            // 
            // colOpprettetDato
            // 
            this.colOpprettetDato.Text = "Opprettet dato";
            this.colOpprettetDato.Width = 80;
            // 
            // colMottaker
            // 
            this.colMottaker.Text = "Mottaker";
            this.colMottaker.Width = 80;
            // 
            // colReferanse
            // 
            this.colReferanse.Text = "Referanse";
            this.colReferanse.Width = 80;
            // 
            // colBeskrivelse
            // 
            this.colBeskrivelse.Text = "Beskrivelse";
            this.colBeskrivelse.Width = 120;
            // 
            // colBeløp
            // 
            this.colBeløp.Text = "Beløp";
            this.colBeløp.Width = 80;
            // 
            // colMva
            // 
            this.colMva.Text = "Mva";
            this.colMva.Width = 80;
            // 
            // colSum
            // 
            this.colSum.Text = "Sum";
            this.colSum.Width = 80;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.Width = 47;
            // 
            // V_FakturaGrunnlagListe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.lwFakturaGrunnlag);
            this.Controls.Add(this.tsFilter);
            this.Controls.Add(this.tsFakturaGrunnlag);
            this.DoubleBuffered = true;
            this.Name = "V_FakturaGrunnlagListe";
            this.Size = new System.Drawing.Size(647, 454);
            this.Load += new System.EventHandler(this.V_FakturaGrunnlagListe_Load);
            this.tsFakturaGrunnlag.ResumeLayout(false);
            this.tsFakturaGrunnlag.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsFakturaGrunnlag;
        private System.Windows.Forms.ColumnHeader colReferanse;
        private System.Windows.Forms.ColumnHeader colBeskrivelse;
        private System.Windows.Forms.ToolStripButton tsBtnNyttFakturaGrunnlag;
        private System.Windows.Forms.ToolStripButton tsBtnRedigerFakturaGrunnlag;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.ToolStripButton tsBtnSlettFakturaGrunnlag;
        private System.Windows.Forms.ToolStripButton tsBtnSendFakturaGrunnlag;
        private System.Windows.Forms.ColumnHeader colMottaker;
        private System.Windows.Forms.ColumnHeader colBeløp;
        private System.Windows.Forms.ColumnHeader colMva;
        private System.Windows.Forms.ColumnHeader colSum;
        private System.Windows.Forms.ToolStripButton tsBtnLagFaktura;
        private System.Windows.Forms.ToolStripButton tsBtnGodkjennFakturaGrunnlag;
        private System.Windows.Forms.ToolStrip tsFilter;
        private ListViewEx lwFakturaGrunnlag;
        private System.Windows.Forms.ToolStripButton tsBtnFilter;
        private System.Windows.Forms.ColumnHeader colOpprettetDato;
        private System.ComponentModel.BackgroundWorker hentFakturaGrunnlagListeTask;
        private System.ComponentModel.BackgroundWorker slettFakturaGrunnlagTask;
    }
}
