﻿namespace Faktura
{
    partial class V_FakturaGrunnlag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLagre = new System.Windows.Forms.Button();
            this.lblFakturaLinjer = new System.Windows.Forms.Label();
            this.pnFakturaLinjer = new System.Windows.Forms.Panel();
            this.lblReferanse = new System.Windows.Forms.Label();
            this.txtReferanse = new System.Windows.Forms.TextBox();
            this._lblBeløp = new System.Windows.Forms.Label();
            this.lblBeløp = new System.Windows.Forms.Label();
            this.lblMva = new System.Windows.Forms.Label();
            this._lblMva = new System.Windows.Forms.Label();
            this.lblTotalt = new System.Windows.Forms.Label();
            this._lblTotalt = new System.Windows.Forms.Label();
            this.lagreFakturaGrunnlagTask = new System.ComponentModel.BackgroundWorker();
            this.initFakturaGrunnlagTask = new System.ComponentModel.BackgroundWorker();
            this.lblBeskrivelse = new System.Windows.Forms.Label();
            this.txtBeskrivelse = new System.Windows.Forms.TextBox();
            this.uscKundeVelger = new Faktura.UserControls.UscKundeVelger();
            this.SuspendLayout();
            // 
            // btnLagre
            // 
            this.btnLagre.Location = new System.Drawing.Point(411, 434);
            this.btnLagre.Name = "btnLagre";
            this.btnLagre.Size = new System.Drawing.Size(75, 23);
            this.btnLagre.TabIndex = 19;
            this.btnLagre.Text = "Lagre";
            this.btnLagre.UseVisualStyleBackColor = true;
            this.btnLagre.Click += new System.EventHandler(this.btnLagre_Click);
            // 
            // lblFakturaLinjer
            // 
            this.lblFakturaLinjer.AutoSize = true;
            this.lblFakturaLinjer.Location = new System.Drawing.Point(5, 201);
            this.lblFakturaLinjer.Name = "lblFakturaLinjer";
            this.lblFakturaLinjer.Size = new System.Drawing.Size(67, 13);
            this.lblFakturaLinjer.TabIndex = 27;
            this.lblFakturaLinjer.Text = "Fakturalinjer:";
            // 
            // pnFakturaLinjer
            // 
            this.pnFakturaLinjer.AutoScroll = true;
            this.pnFakturaLinjer.Location = new System.Drawing.Point(8, 217);
            this.pnFakturaLinjer.Name = "pnFakturaLinjer";
            this.pnFakturaLinjer.Size = new System.Drawing.Size(478, 160);
            this.pnFakturaLinjer.TabIndex = 28;
            // 
            // lblReferanse
            // 
            this.lblReferanse.AutoSize = true;
            this.lblReferanse.Location = new System.Drawing.Point(302, 12);
            this.lblReferanse.Name = "lblReferanse";
            this.lblReferanse.Size = new System.Drawing.Size(59, 13);
            this.lblReferanse.TabIndex = 0;
            this.lblReferanse.Text = "Referanse:";
            // 
            // txtReferanse
            // 
            this.txtReferanse.Location = new System.Drawing.Point(367, 9);
            this.txtReferanse.Name = "txtReferanse";
            this.txtReferanse.Size = new System.Drawing.Size(119, 20);
            this.txtReferanse.TabIndex = 30;
            this.txtReferanse.TabStop = false;
            this.txtReferanse.TextChanged += new System.EventHandler(this.txtReferanse_TextChanged);
            // 
            // _lblBeløp
            // 
            this._lblBeløp.AutoSize = true;
            this._lblBeløp.Location = new System.Drawing.Point(13, 396);
            this._lblBeløp.Name = "_lblBeløp";
            this._lblBeløp.Size = new System.Drawing.Size(37, 13);
            this._lblBeløp.TabIndex = 31;
            this._lblBeløp.Text = "Beløp:";
            // 
            // lblBeløp
            // 
            this.lblBeløp.AutoSize = true;
            this.lblBeløp.Location = new System.Drawing.Point(65, 396);
            this.lblBeløp.Name = "lblBeløp";
            this.lblBeløp.Size = new System.Drawing.Size(30, 13);
            this.lblBeløp.TabIndex = 32;
            this.lblBeløp.Text = "csdc";
            // 
            // lblMva
            // 
            this.lblMva.AutoSize = true;
            this.lblMva.Location = new System.Drawing.Point(240, 396);
            this.lblMva.Name = "lblMva";
            this.lblMva.Size = new System.Drawing.Size(30, 13);
            this.lblMva.TabIndex = 34;
            this.lblMva.Text = "csdc";
            // 
            // _lblMva
            // 
            this._lblMva.AutoSize = true;
            this._lblMva.Location = new System.Drawing.Point(188, 396);
            this._lblMva.Name = "_lblMva";
            this._lblMva.Size = new System.Drawing.Size(31, 13);
            this._lblMva.TabIndex = 33;
            this._lblMva.Text = "Mva:";
            // 
            // lblTotalt
            // 
            this.lblTotalt.AutoSize = true;
            this.lblTotalt.Location = new System.Drawing.Point(449, 396);
            this.lblTotalt.Name = "lblTotalt";
            this.lblTotalt.Size = new System.Drawing.Size(30, 13);
            this.lblTotalt.TabIndex = 36;
            this.lblTotalt.Text = "csdc";
            // 
            // _lblTotalt
            // 
            this._lblTotalt.AutoSize = true;
            this._lblTotalt.Location = new System.Drawing.Point(397, 396);
            this._lblTotalt.Name = "_lblTotalt";
            this._lblTotalt.Size = new System.Drawing.Size(37, 13);
            this._lblTotalt.TabIndex = 35;
            this._lblTotalt.Text = "Totalt:";
            // 
            // lagreFakturaGrunnlagTask
            // 
            this.lagreFakturaGrunnlagTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.lagreFakturaGrunnlagTask_DoWork);
            this.lagreFakturaGrunnlagTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.lagreFakturaGrunnlagTask_RunWorkerCompleted);
            // 
            // initFakturaGrunnlagTask
            // 
            this.initFakturaGrunnlagTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.initFakturaGrunnlagTask_DoWork);
            this.initFakturaGrunnlagTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.initFakturaGrunnlagTask_RunWorkerCompleted);
            // 
            // lblBeskrivelse
            // 
            this.lblBeskrivelse.AutoSize = true;
            this.lblBeskrivelse.Location = new System.Drawing.Point(5, 133);
            this.lblBeskrivelse.Name = "lblBeskrivelse";
            this.lblBeskrivelse.Size = new System.Drawing.Size(64, 13);
            this.lblBeskrivelse.TabIndex = 37;
            this.lblBeskrivelse.Text = "Beskrivelse:";
            // 
            // txtBeskrivelse
            // 
            this.txtBeskrivelse.Location = new System.Drawing.Point(8, 149);
            this.txtBeskrivelse.Multiline = true;
            this.txtBeskrivelse.Name = "txtBeskrivelse";
            this.txtBeskrivelse.Size = new System.Drawing.Size(478, 40);
            this.txtBeskrivelse.TabIndex = 38;
            this.txtBeskrivelse.TextChanged += new System.EventHandler(this.txtBeskrivelse_TextChanged);
            // 
            // uscKundeVelger
            // 
            this.uscKundeVelger.BackColor = System.Drawing.Color.Transparent;
            this.uscKundeVelger.Kunde = null;
            this.uscKundeVelger.Location = new System.Drawing.Point(3, 2);
            this.uscKundeVelger.Name = "uscKundeVelger";
            this.uscKundeVelger.Size = new System.Drawing.Size(293, 116);
            this.uscKundeVelger.TabIndex = 25;
            // 
            // V_FakturaGrunnlag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(498, 469);
            this.Controls.Add(this.txtBeskrivelse);
            this.Controls.Add(this.lblBeskrivelse);
            this.Controls.Add(this.lblTotalt);
            this.Controls.Add(this._lblTotalt);
            this.Controls.Add(this.lblMva);
            this.Controls.Add(this._lblMva);
            this.Controls.Add(this.lblBeløp);
            this.Controls.Add(this._lblBeløp);
            this.Controls.Add(this.txtReferanse);
            this.Controls.Add(this.lblReferanse);
            this.Controls.Add(this.uscKundeVelger);
            this.Controls.Add(this.lblFakturaLinjer);
            this.Controls.Add(this.pnFakturaLinjer);
            this.Controls.Add(this.btnLagre);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "V_FakturaGrunnlag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nytt fakturagrunnlag";
            this.Load += new System.EventHandler(this.V_FakturaGrunnlag_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLagre;
        private UserControls.UscKundeVelger uscKundeVelger;
        private System.Windows.Forms.Label lblFakturaLinjer;
        private System.Windows.Forms.Panel pnFakturaLinjer;
        private System.Windows.Forms.Label lblReferanse;
        private System.Windows.Forms.TextBox txtReferanse;
        private System.Windows.Forms.Label _lblBeløp;
        private System.Windows.Forms.Label lblBeløp;
        private System.Windows.Forms.Label lblMva;
        private System.Windows.Forms.Label _lblMva;
        private System.Windows.Forms.Label lblTotalt;
        private System.Windows.Forms.Label _lblTotalt;
        private System.ComponentModel.BackgroundWorker lagreFakturaGrunnlagTask;
        private System.ComponentModel.BackgroundWorker initFakturaGrunnlagTask;
        private System.Windows.Forms.Label lblBeskrivelse;
        private System.Windows.Forms.TextBox txtBeskrivelse;
    }
}