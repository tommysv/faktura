﻿using System;
using System.Windows.Forms;

namespace Faktura
{
    public class ListViewItemEx : ListViewItem
    {
        private HorizontalAlignment textAlign;

        public ListViewItemEx() : base()
        {
            this.textAlign = HorizontalAlignment.Left;
            this.UseItemStyleForSubItems = false;
        }

        public HorizontalAlignment TextAlign
        {
            get
            {
                return textAlign;
            }
            set
            {
                textAlign = value;
            }
        }
    }
}
