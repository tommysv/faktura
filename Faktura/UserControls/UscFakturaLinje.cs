﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;

namespace Faktura.UserControls
{
    public partial class UscFakturaLinje : UserControl, IV_FakturaLinje
    {
        private P_FakturaLinje p_FakturaLinje;
        private IList<Produkt> produktListe;
        private FakturaLinje fakturaLinje;
        private bool redigerModus;
        private AutoCompleteStringCollection produktKoder = new AutoCompleteStringCollection();
        public event System.EventHandler LeggTil;
        public event System.EventHandler Fjern;
        private bool bLastet = false;

        public UscFakturaLinje()
        {
            InitializeComponent();
            p_FakturaLinje = new P_FakturaLinje(this);
            RedigerModus = true;
        }

        public bool RedigerModus
        {
            get
            {
                return redigerModus;
            }
            set
            {
                redigerModus = value;
                SettRedigerModus(redigerModus);
            }
        }

        public FakturaLinje FakturaLinje
        {
            get
            {
                return fakturaLinje;
            }
            set
            {
                fakturaLinje = value;
                if (bLastet)
                    VisFakturaLinje();
            }
        }

        public IList<Produkt> ProduktListe
        {
            get
            {
                return produktListe;
            }
            set
            {
                produktListe = value;
                if (produktListe != null)
                {
                    for (int nProdukt = 0; nProdukt < produktListe.Count; nProdukt++)
                    {
                        produktKoder.Add(produktListe[nProdukt].ProduktKode);
                        txtProduktkode.AutoCompleteCustomSource = produktKoder;
                    }
                }
            }
        }

        private void SettRedigerModus(bool bRedigerModus)
        {
            if (!bRedigerModus)
            {
                txtProduktkode.Enabled = false;
                txtProduktkode.TabStop = false;
                txtBeskrivelse.ReadOnly = true;
                txtBeskrivelse.TabStop = false;
                txtAnt.ReadOnly = true;
                txtAnt.TabStop = false;
                txtEnhet.ReadOnly = true;
                txtEnhet.TabStop = false;
                txtEnhetspris.ReadOnly = true;
                txtEnhetspris.TabStop = false;
                txtRabattProsent.ReadOnly = true;
                txtRabattProsent.TabStop = false;
                if (!lblFjern.Visible)
                {
                    lblFjern.Visible = true;
                }
                if (lblLeggTil.Visible)
                {
                    lblLeggTil.Visible = false;
                }
            }
        }

        private void VisFakturaLinje()
        {
            if (fakturaLinje != null)
            {
                txtProduktkode.Text = fakturaLinje.ProduktKode;
                txtBeskrivelse.Text = fakturaLinje.Beskrivelse;
                txtAnt.Text = Convert.ToString(fakturaLinje.Ant);
                txtEnhet.Text = fakturaLinje.Enhet;
                txtEnhetspris.Text = fakturaLinje.EnhetsPris.ToString("F");
                lblBeløp.Text = fakturaLinje.KalkulertBeløp.ToString("F");
                txtRabattProsent.Text = fakturaLinje.RabattProsent.ToString("F");
            }
        }

        protected void OnLeggTil()
        {
            if (LeggTil != null)
            {
                LeggTil(this, new EventArgs());
            }
        }

        protected void OnFjern()
        {
            if (Fjern != null)
            {
                Fjern(this, new EventArgs());
            }
        }

        private void lblFjern_Click(object sender, EventArgs e)
        {
            OnFjern();
        }

        private void lblLeggTil_Click(object sender, EventArgs e)
        {
            OnLeggTil();
        }

        private void UscFakturaLinje_Load(object sender, EventArgs e)
        {
            VisFakturaLinje();
            bLastet = true;
        }

        private void lblLeggTil_MouseEnter(object sender, EventArgs e)
        {
            lblLeggTil.BackColor = Color.AliceBlue;
        }

        private void lblLeggTil_MouseLeave(object sender, EventArgs e)
        {
            lblLeggTil.BackColor = Color.Transparent;
        }

        private void lblFjern_MouseEnter(object sender, EventArgs e)
        {
            lblFjern.BackColor = Color.AliceBlue;
        }

        private void lblFjern_MouseLeave(object sender, EventArgs e)
        {
            lblFjern.BackColor = Color.Transparent;
        }

        private void txtProduktkode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            if (produktKoder.Contains(txtProduktkode.Text))
                e.Cancel = false;
        }

        private void txtProduktkode_Validated(object sender, EventArgs e)
        {
            p_FakturaLinje.SettProduktKode(txtProduktkode.Text);
            p_FakturaLinje.Kalkuler();
        }

        private void txtAnt_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            double result = 0;
            e.Cancel = true;
            if (double.TryParse(txtAnt.Text, out result))
            {
                if (txtAnt.BackColor == Color.Salmon)
                    txtAnt.BackColor = Color.White;
                fakturaLinje.Ant = result;
                e.Cancel = false;
            }
            else
            {
                txtAnt.BackColor = Color.Salmon;
            }
        }

        private void txtAnt_Validated(object sender, EventArgs e)
        {
            p_FakturaLinje.Kalkuler();
        }

        private void txtRabattProsent_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            double result = 0;
            e.Cancel = true;
            if (double.TryParse(txtRabattProsent.Text, out result))
            {
                if (txtRabattProsent.BackColor == Color.Salmon)
                    txtRabattProsent.BackColor = Color.White;
                fakturaLinje.RabattProsent = result;
                e.Cancel = false;
            }
            else
            {
                txtRabattProsent.BackColor = Color.Salmon;
            }
        }

        private void txtRabattProsent_Validated(object sender, EventArgs e)
        {
            p_FakturaLinje.Kalkuler();
        }
    }
}
