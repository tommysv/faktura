﻿namespace Faktura.UserControls
{
    partial class UscFakturaLinje
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UscFakturaLinje));
            this.lblBeløp = new System.Windows.Forms.Label();
            this.txtEnhetspris = new System.Windows.Forms.TextBox();
            this.txtEnhet = new System.Windows.Forms.TextBox();
            this.txtAnt = new System.Windows.Forms.TextBox();
            this.txtProduktkode = new System.Windows.Forms.TextBox();
            this.txtBeskrivelse = new System.Windows.Forms.TextBox();
            this.txtRabattProsent = new System.Windows.Forms.TextBox();
            this.lblLeggTil = new System.Windows.Forms.Label();
            this.lblFjern = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBeløp
            // 
            this.lblBeløp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBeløp.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblBeløp.ForeColor = System.Drawing.Color.Black;
            this.lblBeløp.Location = new System.Drawing.Point(409, 0);
            this.lblBeløp.Name = "lblBeløp";
            this.lblBeløp.Size = new System.Drawing.Size(50, 20);
            this.lblBeløp.TabIndex = 0;
            this.lblBeløp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEnhetspris
            // 
            this.txtEnhetspris.BackColor = System.Drawing.Color.White;
            this.txtEnhetspris.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtEnhetspris.ForeColor = System.Drawing.Color.Black;
            this.txtEnhetspris.Location = new System.Drawing.Point(293, 0);
            this.txtEnhetspris.Name = "txtEnhetspris";
            this.txtEnhetspris.ReadOnly = true;
            this.txtEnhetspris.Size = new System.Drawing.Size(58, 20);
            this.txtEnhetspris.TabIndex = 4;
            this.txtEnhetspris.TabStop = false;
            this.txtEnhetspris.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtEnhet
            // 
            this.txtEnhet.BackColor = System.Drawing.Color.White;
            this.txtEnhet.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtEnhet.ForeColor = System.Drawing.Color.Black;
            this.txtEnhet.Location = new System.Drawing.Point(242, 0);
            this.txtEnhet.Name = "txtEnhet";
            this.txtEnhet.ReadOnly = true;
            this.txtEnhet.Size = new System.Drawing.Size(51, 20);
            this.txtEnhet.TabIndex = 3;
            this.txtEnhet.TabStop = false;
            this.txtEnhet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtAnt
            // 
            this.txtAnt.BackColor = System.Drawing.Color.White;
            this.txtAnt.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtAnt.ForeColor = System.Drawing.Color.Black;
            this.txtAnt.Location = new System.Drawing.Point(209, 0);
            this.txtAnt.Name = "txtAnt";
            this.txtAnt.Size = new System.Drawing.Size(33, 20);
            this.txtAnt.TabIndex = 2;
            this.txtAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnt.Validating += new System.ComponentModel.CancelEventHandler(this.txtAnt_Validating);
            this.txtAnt.Validated += new System.EventHandler(this.txtAnt_Validated);
            // 
            // txtProduktkode
            // 
            this.txtProduktkode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtProduktkode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtProduktkode.BackColor = System.Drawing.Color.White;
            this.txtProduktkode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProduktkode.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtProduktkode.ForeColor = System.Drawing.Color.Black;
            this.txtProduktkode.Location = new System.Drawing.Point(0, 0);
            this.txtProduktkode.Name = "txtProduktkode";
            this.txtProduktkode.Size = new System.Drawing.Size(68, 20);
            this.txtProduktkode.TabIndex = 0;
            this.txtProduktkode.Validating += new System.ComponentModel.CancelEventHandler(this.txtProduktkode_Validating);
            this.txtProduktkode.Validated += new System.EventHandler(this.txtProduktkode_Validated);
            // 
            // txtBeskrivelse
            // 
            this.txtBeskrivelse.BackColor = System.Drawing.Color.White;
            this.txtBeskrivelse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBeskrivelse.ForeColor = System.Drawing.Color.Black;
            this.txtBeskrivelse.Location = new System.Drawing.Point(68, 0);
            this.txtBeskrivelse.Name = "txtBeskrivelse";
            this.txtBeskrivelse.ReadOnly = true;
            this.txtBeskrivelse.Size = new System.Drawing.Size(141, 20);
            this.txtBeskrivelse.TabIndex = 1;
            this.txtBeskrivelse.TabStop = false;
            // 
            // txtRabattProsent
            // 
            this.txtRabattProsent.BackColor = System.Drawing.Color.White;
            this.txtRabattProsent.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtRabattProsent.ForeColor = System.Drawing.Color.Black;
            this.txtRabattProsent.Location = new System.Drawing.Point(351, 0);
            this.txtRabattProsent.Name = "txtRabattProsent";
            this.txtRabattProsent.Size = new System.Drawing.Size(58, 20);
            this.txtRabattProsent.TabIndex = 7;
            this.txtRabattProsent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRabattProsent.Validating += new System.ComponentModel.CancelEventHandler(this.txtRabattProsent_Validating);
            this.txtRabattProsent.Validated += new System.EventHandler(this.txtRabattProsent_Validated);
            // 
            // lblLeggTil
            // 
            this.lblLeggTil.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLeggTil.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblLeggTil.Image = global::Faktura.Properties.Resources.plus16x16;
            this.lblLeggTil.Location = new System.Drawing.Point(479, 0);
            this.lblLeggTil.Name = "lblLeggTil";
            this.lblLeggTil.Size = new System.Drawing.Size(20, 20);
            this.lblLeggTil.TabIndex = 10;
            this.lblLeggTil.Click += new System.EventHandler(this.lblLeggTil_Click);
            this.lblLeggTil.MouseEnter += new System.EventHandler(this.lblLeggTil_MouseEnter);
            this.lblLeggTil.MouseLeave += new System.EventHandler(this.lblLeggTil_MouseLeave);
            // 
            // lblFjern
            // 
            this.lblFjern.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFjern.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblFjern.Image = ((System.Drawing.Image)(resources.GetObject("lblFjern.Image")));
            this.lblFjern.Location = new System.Drawing.Point(459, 0);
            this.lblFjern.Name = "lblFjern";
            this.lblFjern.Size = new System.Drawing.Size(20, 20);
            this.lblFjern.TabIndex = 9;
            this.lblFjern.Visible = false;
            this.lblFjern.Click += new System.EventHandler(this.lblFjern_Click);
            this.lblFjern.MouseEnter += new System.EventHandler(this.lblFjern_MouseEnter);
            this.lblFjern.MouseLeave += new System.EventHandler(this.lblFjern_MouseLeave);
            // 
            // UscFakturaLinje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.txtBeskrivelse);
            this.Controls.Add(this.txtProduktkode);
            this.Controls.Add(this.txtAnt);
            this.Controls.Add(this.txtEnhet);
            this.Controls.Add(this.txtEnhetspris);
            this.Controls.Add(this.txtRabattProsent);
            this.Controls.Add(this.lblBeløp);
            this.Controls.Add(this.lblFjern);
            this.Controls.Add(this.lblLeggTil);
            this.DoubleBuffered = true;
            this.Name = "UscFakturaLinje";
            this.Size = new System.Drawing.Size(499, 20);
            this.Load += new System.EventHandler(this.UscFakturaLinje_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBeløp;
        private System.Windows.Forms.TextBox txtEnhetspris;
        private System.Windows.Forms.TextBox txtEnhet;
        private System.Windows.Forms.TextBox txtAnt;
        private System.Windows.Forms.TextBox txtProduktkode;
        private System.Windows.Forms.TextBox txtBeskrivelse;
        private System.Windows.Forms.TextBox txtRabattProsent;
        private System.Windows.Forms.Label lblFjern;
        private System.Windows.Forms.Label lblLeggTil;
    }
}
