﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;

namespace Faktura
{
    public partial class V_ProduktListe : UserControl, IV_ToolStrip, IV_ProduktListe, IPrintable, IReportStatus
    {
        private P_ProduktListe p_ProduktListe;
        private IList<Produkt> produktListe;
        private Produkt valgtProdukt;
        public event ReportStatusEventHandler ReportStatus;

        public V_ProduktListe()
        {
            InitializeComponent();
            lwProduktListe.ColumnWeights = new float[] { 0.1f, 0.6f, 0.1f, 0.1f, 0.1f };
            p_ProduktListe = new P_ProduktListe(this);
        }

        public ToolStrip ToolStrip
        {
            get
            {
                return tsProduktListe;
            }
        }

        public IList<Produkt> ProduktListe
        {
            set
            {
                produktListe = value;
            }
        }

        public Produkt ValgtProdukt
        {
            get
            {
                return valgtProdukt;
            }
        }

        private void UscProduktListe_Load(object sender, EventArgs e)
        {
            lwProduktListe.ReCalculateColumnWidths();
            hentProduktListeTask_Run();
        }

        private void tsBtnNyttProdukt_Click(object sender, EventArgs e)
        {
            V_Produkt v_NyttProdukt = new V_Produkt();
            DialogResult r = v_NyttProdukt.ShowDialog();
            if (r == DialogResult.OK)
            {
                ((IV_Oppdaterbar)this).Oppdater(true);
            }
        }

        private void tsBtnRedigerProdukt_Click(object sender, EventArgs e)
        {
            if (valgtProdukt != null)
            {
                V_Produkt v_RedigerProdukt = new V_Produkt();
                v_RedigerProdukt.Produkt = valgtProdukt;
                DialogResult r = v_RedigerProdukt.ShowDialog();
                if (r == DialogResult.OK)
                {
                    ((IV_Oppdaterbar)this).Oppdater(true);
                }
            }
        }

        private void tsBtnSlettProdukt_Click(object sender, EventArgs e)
        {
            if (valgtProdukt != null)
            {
                if (!slettProduktTask.IsBusy)
                {
                    slettProduktTask.RunWorkerAsync();
                }
            }
        }

        private void lwProduktListe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lwProduktListe.SelectedIndices.Count > 0)
            {
                valgtProdukt = produktListe[lwProduktListe.SelectedIndices[0]];
                tsBtnRedigerProdukt.Visible = true;
                tsBtnSlettProdukt.Visible = true;
            }
            else
            {
                tsBtnRedigerProdukt.Visible = false;
                tsBtnSlettProdukt.Visible = false;
            }
        }

        private void lwProduktListe_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                lwProduktListe.Select();
            }
        }

        private void slettProduktTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_ProduktListe.SlettProdukt();
        }

        private void slettProduktTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {    
                ((IV_Oppdaterbar)this).Oppdater(true);
                slettProduktTask_End();
            }
            else
            {
                slettProduktTask_Error();
            }
        }

        private void hentProduktListeTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_ProduktListe.HentProduktListe();
        }

        private void DrawListViewItems()
        {
            lwProduktListe.BeginUpdate();
            lwProduktListe.Items.Clear();
            if ((produktListe != null) && (produktListe.Count > 0))
            {
                for (int n = 0; n < produktListe.Count; n++)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = produktListe[n].ProduktKode;
                    item.SubItems.Add(produktListe[n].Beskrivelse);
                    item.SubItems.Add(produktListe[n].Enhet);
                    item.SubItems.Add(Convert.ToString(produktListe[n].EnhetsPris));
                    item.SubItems.Add(Convert.ToString(produktListe[n].MvaSats));
                    lwProduktListe.Items.Add(item);
                }
            }
            lwProduktListe.EndUpdate();
        }

        private void hentProduktListeTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                DrawListViewItems();
                hentProduktListeTask_End();
            }
            else
            {
                hentProduktListeTask_Error();
            }
        }

        private void hentProduktListeTask_Run()
        {
            if (!hentProduktListeTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Henter produktliste..."));
                hentProduktListeTask.RunWorkerAsync();
            }
        }

        private void hentProduktListeTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void hentProduktListeTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved henting av produktliste", true));
        }


        private void slettProduktTask_Run()
        {
            if (!slettProduktTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Sletter produkt..."));
                slettProduktTask.RunWorkerAsync();
            }
        }

        private void slettProduktTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void slettProduktTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved sletting av produkt", true));
        }

        void IPrintable.Print()
        {
            throw new NotImplementedException();
        }

        protected void OnReportStatus(ReportStatusEventArgs e)
        {
            if (ReportStatus != null)
            {
                ReportStatus(this, e);
            }
        }

        void IV_Oppdaterbar.Oppdater(bool bLast)
        {
            if (bLast)
                hentProduktListeTask_Run();
            else
                DrawListViewItems();
        }
    }
}
