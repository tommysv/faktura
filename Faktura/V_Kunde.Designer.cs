﻿namespace Faktura
{
    partial class V_Kunde
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNavn = new System.Windows.Forms.TextBox();
            this.lblNavn = new System.Windows.Forms.Label();
            this.txtPostadresse = new System.Windows.Forms.TextBox();
            this.lblPostAdresse = new System.Windows.Forms.Label();
            this.txtPostnr = new System.Windows.Forms.TextBox();
            this.lblPostnr = new System.Windows.Forms.Label();
            this.txtPoststed = new System.Windows.Forms.TextBox();
            this.lblPoststed = new System.Windows.Forms.Label();
            this.btnLagre = new System.Windows.Forms.Button();
            this.lblVelgKunde = new System.Windows.Forms.Label();
            this.rbPrivatKunde = new System.Windows.Forms.RadioButton();
            this.rbBedriftskunde = new System.Windows.Forms.RadioButton();
            this.txtFakturaEpost = new System.Windows.Forms.TextBox();
            this.lblFakturaEpost = new System.Windows.Forms.Label();
            this.lblKundeId = new System.Windows.Forms.Label();
            this.txtKundeId = new System.Windows.Forms.Label();
            this.lagreBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.lblFakturaMetode = new System.Windows.Forms.Label();
            this.rbFakturaMetodeEpost = new System.Windows.Forms.RadioButton();
            this.rbFakturaMetodePapir = new System.Windows.Forms.RadioButton();
            this.pnFakturaMetode = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblGrunnlagEpost = new System.Windows.Forms.Label();
            this.txtGrunnlagEpost = new System.Windows.Forms.TextBox();
            this.pnFakturaMetode.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNavn
            // 
            this.txtNavn.Location = new System.Drawing.Point(121, 84);
            this.txtNavn.MaxLength = 100;
            this.txtNavn.Name = "txtNavn";
            this.txtNavn.Size = new System.Drawing.Size(188, 20);
            this.txtNavn.TabIndex = 2;
            // 
            // lblNavn
            // 
            this.lblNavn.AutoSize = true;
            this.lblNavn.Location = new System.Drawing.Point(16, 84);
            this.lblNavn.Name = "lblNavn";
            this.lblNavn.Size = new System.Drawing.Size(36, 13);
            this.lblNavn.TabIndex = 0;
            this.lblNavn.Text = "Navn:";
            // 
            // txtPostadresse
            // 
            this.txtPostadresse.Location = new System.Drawing.Point(121, 110);
            this.txtPostadresse.MaxLength = 100;
            this.txtPostadresse.Name = "txtPostadresse";
            this.txtPostadresse.Size = new System.Drawing.Size(188, 20);
            this.txtPostadresse.TabIndex = 3;
            // 
            // lblPostAdresse
            // 
            this.lblPostAdresse.AutoSize = true;
            this.lblPostAdresse.Location = new System.Drawing.Point(16, 110);
            this.lblPostAdresse.Name = "lblPostAdresse";
            this.lblPostAdresse.Size = new System.Drawing.Size(68, 13);
            this.lblPostAdresse.TabIndex = 0;
            this.lblPostAdresse.Text = "Postadresse:";
            // 
            // txtPostnr
            // 
            this.txtPostnr.Location = new System.Drawing.Point(121, 136);
            this.txtPostnr.MaxLength = 4;
            this.txtPostnr.Name = "txtPostnr";
            this.txtPostnr.Size = new System.Drawing.Size(61, 20);
            this.txtPostnr.TabIndex = 4;
            // 
            // lblPostnr
            // 
            this.lblPostnr.AutoSize = true;
            this.lblPostnr.Location = new System.Drawing.Point(16, 136);
            this.lblPostnr.Name = "lblPostnr";
            this.lblPostnr.Size = new System.Drawing.Size(43, 13);
            this.lblPostnr.TabIndex = 0;
            this.lblPostnr.Text = "Post-nr:";
            // 
            // txtPoststed
            // 
            this.txtPoststed.Location = new System.Drawing.Point(121, 162);
            this.txtPoststed.MaxLength = 100;
            this.txtPoststed.Name = "txtPoststed";
            this.txtPoststed.Size = new System.Drawing.Size(188, 20);
            this.txtPoststed.TabIndex = 5;
            // 
            // lblPoststed
            // 
            this.lblPoststed.AutoSize = true;
            this.lblPoststed.Location = new System.Drawing.Point(16, 166);
            this.lblPoststed.Name = "lblPoststed";
            this.lblPoststed.Size = new System.Drawing.Size(51, 13);
            this.lblPoststed.TabIndex = 0;
            this.lblPoststed.Text = "Poststed:";
            // 
            // btnLagre
            // 
            this.btnLagre.Location = new System.Drawing.Point(233, 310);
            this.btnLagre.Name = "btnLagre";
            this.btnLagre.Size = new System.Drawing.Size(75, 23);
            this.btnLagre.TabIndex = 7;
            this.btnLagre.Text = "Lagre";
            this.btnLagre.UseVisualStyleBackColor = true;
            this.btnLagre.Click += new System.EventHandler(this.btnLagre_Click);
            // 
            // lblVelgKunde
            // 
            this.lblVelgKunde.AutoSize = true;
            this.lblVelgKunde.Location = new System.Drawing.Point(16, 52);
            this.lblVelgKunde.Name = "lblVelgKunde";
            this.lblVelgKunde.Size = new System.Drawing.Size(64, 13);
            this.lblVelgKunde.TabIndex = 0;
            this.lblVelgKunde.Text = "Velg kunde:";
            // 
            // rbPrivatKunde
            // 
            this.rbPrivatKunde.AutoSize = true;
            this.rbPrivatKunde.Checked = true;
            this.rbPrivatKunde.Location = new System.Drawing.Point(0, 7);
            this.rbPrivatKunde.Name = "rbPrivatKunde";
            this.rbPrivatKunde.Size = new System.Drawing.Size(82, 17);
            this.rbPrivatKunde.TabIndex = 0;
            this.rbPrivatKunde.TabStop = true;
            this.rbPrivatKunde.Text = "Privatkunde";
            this.rbPrivatKunde.UseVisualStyleBackColor = true;
            // 
            // rbBedriftskunde
            // 
            this.rbBedriftskunde.AutoSize = true;
            this.rbBedriftskunde.Location = new System.Drawing.Point(98, 7);
            this.rbBedriftskunde.Name = "rbBedriftskunde";
            this.rbBedriftskunde.Size = new System.Drawing.Size(90, 17);
            this.rbBedriftskunde.TabIndex = 1;
            this.rbBedriftskunde.Text = "Bedriftskunde";
            this.rbBedriftskunde.UseVisualStyleBackColor = true;
            // 
            // txtFakturaEpost
            // 
            this.txtFakturaEpost.Location = new System.Drawing.Point(121, 189);
            this.txtFakturaEpost.MaxLength = 200;
            this.txtFakturaEpost.Name = "txtFakturaEpost";
            this.txtFakturaEpost.Size = new System.Drawing.Size(188, 20);
            this.txtFakturaEpost.TabIndex = 6;
            // 
            // lblFakturaEpost
            // 
            this.lblFakturaEpost.AutoSize = true;
            this.lblFakturaEpost.Location = new System.Drawing.Point(16, 192);
            this.lblFakturaEpost.Name = "lblFakturaEpost";
            this.lblFakturaEpost.Size = new System.Drawing.Size(79, 13);
            this.lblFakturaEpost.TabIndex = 0;
            this.lblFakturaEpost.Text = "Epost (faktura):";
            // 
            // lblKundeId
            // 
            this.lblKundeId.AutoSize = true;
            this.lblKundeId.Location = new System.Drawing.Point(16, 18);
            this.lblKundeId.Name = "lblKundeId";
            this.lblKundeId.Size = new System.Drawing.Size(53, 13);
            this.lblKundeId.TabIndex = 0;
            this.lblKundeId.Text = "Kunde-Id:";
            // 
            // txtKundeId
            // 
            this.txtKundeId.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtKundeId.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.txtKundeId.Location = new System.Drawing.Point(121, 17);
            this.txtKundeId.Name = "txtKundeId";
            this.txtKundeId.Size = new System.Drawing.Size(61, 20);
            this.txtKundeId.TabIndex = 0;
            this.txtKundeId.Text = "Automatisk";
            this.txtKundeId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lagreBackgroundTask
            // 
            this.lagreBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.lagreBackgroundTask_DoWork);
            this.lagreBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.lagreBackgroundTask_RunWorkerCompleted);
            // 
            // lblFakturaMetode
            // 
            this.lblFakturaMetode.AutoSize = true;
            this.lblFakturaMetode.Location = new System.Drawing.Point(15, 269);
            this.lblFakturaMetode.Name = "lblFakturaMetode";
            this.lblFakturaMetode.Size = new System.Drawing.Size(84, 13);
            this.lblFakturaMetode.TabIndex = 8;
            this.lblFakturaMetode.Text = "Faktura-metode:";
            // 
            // rbFakturaMetodeEpost
            // 
            this.rbFakturaMetodeEpost.AutoSize = true;
            this.rbFakturaMetodeEpost.Checked = true;
            this.rbFakturaMetodeEpost.Location = new System.Drawing.Point(0, 9);
            this.rbFakturaMetodeEpost.Name = "rbFakturaMetodeEpost";
            this.rbFakturaMetodeEpost.Size = new System.Drawing.Size(52, 17);
            this.rbFakturaMetodeEpost.TabIndex = 9;
            this.rbFakturaMetodeEpost.TabStop = true;
            this.rbFakturaMetodeEpost.Text = "Epost";
            this.rbFakturaMetodeEpost.UseVisualStyleBackColor = true;
            // 
            // rbFakturaMetodePapir
            // 
            this.rbFakturaMetodePapir.AutoSize = true;
            this.rbFakturaMetodePapir.Location = new System.Drawing.Point(97, 10);
            this.rbFakturaMetodePapir.Name = "rbFakturaMetodePapir";
            this.rbFakturaMetodePapir.Size = new System.Drawing.Size(49, 17);
            this.rbFakturaMetodePapir.TabIndex = 10;
            this.rbFakturaMetodePapir.Text = "Papir";
            this.rbFakturaMetodePapir.UseVisualStyleBackColor = true;
            // 
            // pnFakturaMetode
            // 
            this.pnFakturaMetode.Controls.Add(this.rbFakturaMetodeEpost);
            this.pnFakturaMetode.Controls.Add(this.rbFakturaMetodePapir);
            this.pnFakturaMetode.Location = new System.Drawing.Point(121, 258);
            this.pnFakturaMetode.Name = "pnFakturaMetode";
            this.pnFakturaMetode.Size = new System.Drawing.Size(187, 35);
            this.pnFakturaMetode.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbPrivatKunde);
            this.panel1.Controls.Add(this.rbBedriftskunde);
            this.panel1.Location = new System.Drawing.Point(121, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 35);
            this.panel1.TabIndex = 12;
            // 
            // lblGrunnlagEpost
            // 
            this.lblGrunnlagEpost.AutoSize = true;
            this.lblGrunnlagEpost.Location = new System.Drawing.Point(16, 224);
            this.lblGrunnlagEpost.Name = "lblGrunnlagEpost";
            this.lblGrunnlagEpost.Size = new System.Drawing.Size(87, 13);
            this.lblGrunnlagEpost.TabIndex = 13;
            this.lblGrunnlagEpost.Text = "Epost (grunnlag):";
            // 
            // txtGrunnlagEpost
            // 
            this.txtGrunnlagEpost.Location = new System.Drawing.Point(122, 221);
            this.txtGrunnlagEpost.MaxLength = 200;
            this.txtGrunnlagEpost.Name = "txtGrunnlagEpost";
            this.txtGrunnlagEpost.Size = new System.Drawing.Size(188, 20);
            this.txtGrunnlagEpost.TabIndex = 14;
            // 
            // V_Kunde
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(332, 350);
            this.Controls.Add(this.txtGrunnlagEpost);
            this.Controls.Add(this.lblGrunnlagEpost);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnFakturaMetode);
            this.Controls.Add(this.lblFakturaMetode);
            this.Controls.Add(this.txtKundeId);
            this.Controls.Add(this.lblKundeId);
            this.Controls.Add(this.txtFakturaEpost);
            this.Controls.Add(this.lblFakturaEpost);
            this.Controls.Add(this.lblVelgKunde);
            this.Controls.Add(this.btnLagre);
            this.Controls.Add(this.txtPoststed);
            this.Controls.Add(this.lblPoststed);
            this.Controls.Add(this.txtPostnr);
            this.Controls.Add(this.lblPostnr);
            this.Controls.Add(this.txtPostadresse);
            this.Controls.Add(this.lblPostAdresse);
            this.Controls.Add(this.txtNavn);
            this.Controls.Add(this.lblNavn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "V_Kunde";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ny kunde";
            this.Load += new System.EventHandler(this.V_Kunde_Load);
            this.pnFakturaMetode.ResumeLayout(false);
            this.pnFakturaMetode.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNavn;
        private System.Windows.Forms.Label lblNavn;
        private System.Windows.Forms.TextBox txtPostadresse;
        private System.Windows.Forms.Label lblPostAdresse;
        private System.Windows.Forms.TextBox txtPostnr;
        private System.Windows.Forms.Label lblPostnr;
        private System.Windows.Forms.TextBox txtPoststed;
        private System.Windows.Forms.Label lblPoststed;
        private System.Windows.Forms.Button btnLagre;
        private System.Windows.Forms.Label lblVelgKunde;
        private System.Windows.Forms.RadioButton rbPrivatKunde;
        private System.Windows.Forms.RadioButton rbBedriftskunde;
        private System.Windows.Forms.TextBox txtFakturaEpost;
        private System.Windows.Forms.Label lblFakturaEpost;
        private System.Windows.Forms.Label lblKundeId;
        private System.Windows.Forms.Label txtKundeId;
        private System.ComponentModel.BackgroundWorker lagreBackgroundTask;
        private System.Windows.Forms.Label lblFakturaMetode;
        private System.Windows.Forms.RadioButton rbFakturaMetodeEpost;
        private System.Windows.Forms.RadioButton rbFakturaMetodePapir;
        private System.Windows.Forms.Panel pnFakturaMetode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblGrunnlagEpost;
        private System.Windows.Forms.TextBox txtGrunnlagEpost;
    }
}