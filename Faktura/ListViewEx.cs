﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace Faktura
{
    public class ListViewEx : ListView
    {
        private Color selectedColor;
        private float[] columnWeights;

        public ListViewEx()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            this.OwnerDraw = true;
            this.SmallImageList = new ImageList();
            this.SmallImageList.ImageSize = new Size(1, 24);
            this.selectedColor = Color.GhostWhite;
            this.columnWeights = null;
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        public float[] ColumnWeights
        {
            get
            {
                return columnWeights;
            }
            set
            {
                columnWeights = value;
            }
        }

        protected Color SelectedColor
        {
            get
            {
                return selectedColor;
            }
            set
            {
                selectedColor = value;
            }
        }

        public void ReCalculateColumnWidths()
        {
            if (this.Columns.Count == 0)
                return;
            if (this.Columns.Count == 1)
            {
                this.Columns[0].Width = this.ClientSize.Width;
                return;
            }
            if ((columnWeights != null) && (columnWeights.Length > 0))
            {
                if (columnWeights.Length == this.Columns.Count)
                {
                    int totalWidth = this.ClientSize.Width - SystemInformation.VerticalScrollBarWidth - 1;
                    int widthRemaining = totalWidth;
                    for (int nColumn = 0; nColumn < this.Columns.Count; nColumn++)
                    {
                        if (nColumn == this.Columns.Count - 1)
                        {
                            this.Columns[nColumn].Width = widthRemaining;
                        }
                        else
                        {
                            float columnWidth = columnWeights[nColumn] * (float)totalWidth;
                            this.Columns[nColumn].Width = (int)columnWidth;
                            widthRemaining -= this.Columns[nColumn].Width;
                        }
                    }
                }
            }
        }

        protected override void OnDrawColumnHeader(DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
            base.OnDrawColumnHeader(e);
        }

        protected override void OnDrawItem(DrawListViewItemEventArgs e)
        {
            e.DrawDefault = false;
            base.OnDrawItem(e);
        }

        protected override void OnDrawSubItem(DrawListViewSubItemEventArgs e)
        {
            ListViewSubItemEx lwSubItem = e.SubItem as ListViewSubItemEx;    
            string text;
            Color foreColor;
            Color backColor;
            Font font;
            HorizontalAlignment alignment;

            if (e.ColumnIndex == 0)
            {
                ListViewItemEx lwItem = e.Item as ListViewItemEx;
                alignment = (lwItem != null) ? lwItem.TextAlign : this.Columns[0].TextAlign;
            }
            else
            {
                alignment = (lwSubItem != null) ? lwSubItem.TextAlign : this.Columns[e.ColumnIndex].TextAlign;
            }
            
            text = e.SubItem.Text;
            if (e.Item.UseItemStyleForSubItems)
            {
                foreColor = e.SubItem.ForeColor;
                backColor = e.SubItem.BackColor;
                font = e.SubItem.Font;
            }
            else
            {
                foreColor = e.Item.ForeColor;
                backColor = e.Item.BackColor;
                font = e.Item.Font;
            }

            if (((e.ItemState & ListViewItemStates.Selected) != 0) || e.Item.Selected)
            {
                using (SolidBrush selectedBrush = new SolidBrush(this.selectedColor))
                {
                    e.Graphics.FillRectangle(selectedBrush, e.Bounds);
                    ControlPaint.DrawBorder(e.Graphics, e.Bounds,
                        Color.White, 2, ButtonBorderStyle.Outset,
                        Color.White, 2, ButtonBorderStyle.Outset,
                        Color.White, 2, ButtonBorderStyle.Outset,
                        Color.White, 2, ButtonBorderStyle.Outset);
                }
            }
            else
            {
                using (SolidBrush backBrush = new SolidBrush(backColor))
                {
                    e.Graphics.FillRectangle(backBrush, e.Bounds);
                    e.Graphics.DrawRectangle(Pens.White, e.Bounds);
                }
            }
            switch (alignment)
            {
                case HorizontalAlignment.Left:
                    TextRenderer.DrawText(e.Graphics, text, font, e.Bounds, foreColor,
                        TextFormatFlags.Left | TextFormatFlags.VerticalCenter);
                    break;
                case HorizontalAlignment.Center:
                    TextRenderer.DrawText(e.Graphics, text, font, e.Bounds, foreColor,
                        TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);
                    break;
                case HorizontalAlignment.Right:
                    TextRenderer.DrawText(e.Graphics, text, font, e.Bounds, foreColor,
                        TextFormatFlags.Right | TextFormatFlags.VerticalCenter);
                    break;
            }

            e.DrawDefault = false;
            base.OnDrawSubItem(e);
        }


    }
}
