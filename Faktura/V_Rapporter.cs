﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;

namespace Faktura
{
    public partial class V_Rapporter : UserControl, IV_Rapport, IPrintable, IReportStatus
    {
        private P_Rapport p_Rapport;
        private int[] rapportÅr;
        private string rapportFil;
        private ComboBox cbxRapportÅr;
        public event ReportStatusEventHandler ReportStatus;

        public V_Rapporter()
        {
            InitializeComponent();
            InitToolstrip();

            p_Rapport = new P_Rapport(this);
        }

        int[] IV_Rapport.RapportÅr
        {
            set
            {
                rapportÅr = value;
            }
        }

        string IV_Rapport.RapportFil
        {
            set
            {
                rapportFil = value;
            }
        }

        private void InitToolstrip()
        {
            Label lblRapportÅr = new Label();
            lblRapportÅr.Text = "Velg år:";
            lblRapportÅr.Padding = new Padding(3);
            lblRapportÅr.TextAlign = ContentAlignment.MiddleLeft;
            lblRapportÅr.BackColor = Color.Transparent;
            ToolStripControlHost _lblRapportÅr = new ToolStripControlHost(lblRapportÅr);

            cbxRapportÅr = new ComboBox();
            cbxRapportÅr.Text = DateTime.Now.Year.ToString();
            cbxRapportÅr.Width = 80;
            cbxRapportÅr.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxRapportÅr.Padding = new Padding(3);
            cbxRapportÅr.SelectedIndexChanged += new EventHandler(cbxRapportÅr_SelectedIndexChanged);
            ToolStripControlHost _cbxRapportÅr = new ToolStripControlHost(cbxRapportÅr);

            tsRapporter.Items.Add(_lblRapportÅr);
            tsRapporter.Items.Add(_cbxRapportÅr);
        }

        private void V_Rapporter_Load(object sender, EventArgs e)
        {
            initTask_Run();
        }

        private void cbxRapportÅr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxRapportÅr.SelectedIndex != -1)
            {
                int år = Convert.ToInt32(cbxRapportÅr.Items[cbxRapportÅr.SelectedIndex]);
                genererRapportTask_Run(år);
            }
        }

        private void initTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_Rapport.Init();
        }

        private void initTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                initTask_End();
                if (rapportÅr.Length > 0)
                {
                    for (int n = rapportÅr.Length - 1; n >= 0; n--)
                    {
                        cbxRapportÅr.Items.Add(rapportÅr[n]);
                    }
                    cbxRapportÅr.SelectedIndex = 0;
                }
            }
            else
                initTask_Error();
        }

        private void genererRapportTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int år = (int)e.Argument;
            p_Rapport.GenererRapport(år);
        }

        private void genererRapportTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                wbRapport.Navigate(rapportFil);
                genererRapportTask_End();
            }
            else
            {
                genererRapportTask_Error();
            }
        }

        private void initTask_Run()
        {
            if (!initTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Initialiserer..."));
                initTask.RunWorkerAsync();
            }
        }

        private void initTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void initTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved initialisering", true));
        }

        private void genererRapportTask_Run(int år)
        {
            if (!genererRapportTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Genererer rapport..."));
                genererRapportTask.RunWorkerAsync(år);
            }
        }

        private void genererRapportTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void genererRapportTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved generering av rapport", true));
        }

        void IPrintable.Print()
        {
            wbRapport.Print();
        }

        protected void OnReportStatus(ReportStatusEventArgs e)
        {
            if (ReportStatus != null)
            {
                ReportStatus(this, e);
            }
        }
    }
}
