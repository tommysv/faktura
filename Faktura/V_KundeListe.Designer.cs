﻿namespace Faktura
{
    partial class V_KundeListe
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tsKundeListe = new System.Windows.Forms.ToolStrip();
            this.tsBtnNyKunde = new System.Windows.Forms.ToolStripButton();
            this.tsBtnRedigerKunde = new System.Windows.Forms.ToolStripButton();
            this.hentKundeListeTask = new System.ComponentModel.BackgroundWorker();
            this.lwKundeListe = new Faktura.ListViewEx();
            this.colNavn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPostadresse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPostNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPoststed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEpost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colKundetype = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tsKundeListe.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsKundeListe
            // 
            this.tsKundeListe.BackColor = System.Drawing.Color.White;
            this.tsKundeListe.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnNyKunde,
            this.tsBtnRedigerKunde});
            this.tsKundeListe.Location = new System.Drawing.Point(0, 0);
            this.tsKundeListe.Name = "tsKundeListe";
            this.tsKundeListe.Size = new System.Drawing.Size(651, 37);
            this.tsKundeListe.TabIndex = 1;
            this.tsKundeListe.Visible = false;
            // 
            // tsBtnNyKunde
            // 
            this.tsBtnNyKunde.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnNyKunde.Image = global::Faktura.Properties.Resources.customer_add;
            this.tsBtnNyKunde.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnNyKunde.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnNyKunde.Name = "tsBtnNyKunde";
            this.tsBtnNyKunde.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnNyKunde.Size = new System.Drawing.Size(34, 34);
            this.tsBtnNyKunde.Click += new System.EventHandler(this.tsBtnNyKunde_Click);
            // 
            // tsBtnRedigerKunde
            // 
            this.tsBtnRedigerKunde.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnRedigerKunde.Image = global::Faktura.Properties.Resources.customer_edit;
            this.tsBtnRedigerKunde.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnRedigerKunde.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnRedigerKunde.Name = "tsBtnRedigerKunde";
            this.tsBtnRedigerKunde.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnRedigerKunde.Size = new System.Drawing.Size(34, 34);
            this.tsBtnRedigerKunde.Visible = false;
            this.tsBtnRedigerKunde.Click += new System.EventHandler(this.tsBtnRedigerKunde_Click);
            // 
            // hentKundeListeTask
            // 
            this.hentKundeListeTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.hentKundeListeTask_DoWork);
            this.hentKundeListeTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.hentKundeListeTask_RunWorkerCompleted);
            // 
            // lwKundeListe
            // 
            this.lwKundeListe.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lwKundeListe.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNavn,
            this.colPostadresse,
            this.colPostNr,
            this.colPoststed,
            this.colEpost,
            this.colKundetype});
            this.lwKundeListe.ColumnWeights = null;
            this.lwKundeListe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwKundeListe.FullRowSelect = true;
            this.lwKundeListe.Location = new System.Drawing.Point(0, 37);
            this.lwKundeListe.MultiSelect = false;
            this.lwKundeListe.Name = "lwKundeListe";
            this.lwKundeListe.OwnerDraw = true;
            this.lwKundeListe.Size = new System.Drawing.Size(651, 421);
            this.lwKundeListe.TabIndex = 2;
            this.lwKundeListe.UseCompatibleStateImageBehavior = false;
            this.lwKundeListe.View = System.Windows.Forms.View.Details;
            // 
            // colNavn
            // 
            this.colNavn.Text = "Navn";
            this.colNavn.Width = 100;
            // 
            // colPostadresse
            // 
            this.colPostadresse.Text = "Postadresse";
            this.colPostadresse.Width = 150;
            // 
            // colPostNr
            // 
            this.colPostNr.Text = "Post-nr";
            // 
            // colPoststed
            // 
            this.colPoststed.Text = "Poststed";
            this.colPoststed.Width = 100;
            // 
            // colEpost
            // 
            this.colEpost.Text = "Epost";
            this.colEpost.Width = 100;
            // 
            // colKundetype
            // 
            this.colKundetype.Text = "Kundetype";
            this.colKundetype.Width = 85;
            // 
            // V_KundeListe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.lwKundeListe);
            this.Controls.Add(this.tsKundeListe);
            this.DoubleBuffered = true;
            this.Name = "V_KundeListe";
            this.Size = new System.Drawing.Size(651, 458);
            this.Load += new System.EventHandler(this.UscKundeListe_Load);
            this.VisibleChanged += new System.EventHandler(this.V_KundeListe_VisibleChanged);
            this.tsKundeListe.ResumeLayout(false);
            this.tsKundeListe.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader colNavn;
        private System.Windows.Forms.ColumnHeader colPostadresse;
        private System.Windows.Forms.ColumnHeader colPostNr;
        private System.Windows.Forms.ColumnHeader colPoststed;
        private System.Windows.Forms.ColumnHeader colEpost;
        private System.Windows.Forms.ColumnHeader colKundetype;
        private System.Windows.Forms.ToolStrip tsKundeListe;
        private System.Windows.Forms.ToolStripButton tsBtnNyKunde;
        private System.Windows.Forms.ToolStripButton tsBtnRedigerKunde;
        private System.ComponentModel.BackgroundWorker hentKundeListeTask;
        private Faktura.ListViewEx lwKundeListe;
    }
}
