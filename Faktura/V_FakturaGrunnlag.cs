﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

using Faktura.Presentation;
using Faktura.UserControls;
using Faktura.Common;

namespace Faktura
{
    public partial class V_FakturaGrunnlag : Form, IV_FakturaGrunnlag
    {
        private int fakturaGrunnlagId;
        private P_FakturaGrunnlag p_FakturaGrunnlag;
        private IList<Kunde> kundeListe; 
        private IList<Produkt> produktListe;
        private Common.FakturaGrunnlag fakturaGrunnlag;

        public V_FakturaGrunnlag(int fakturaGrunnlagId = -1)
        {
            InitializeComponent();
            p_FakturaGrunnlag = new P_FakturaGrunnlag(this);
            this.fakturaGrunnlagId = fakturaGrunnlagId;

            uscKundeVelger.KundeValgt += new EventHandler(uscFakturaMottaker_KundeValgt);
        }

        public IList<Kunde> KundeListe
        {
            set { kundeListe = value; }
        }

        public IList<Produkt> ProduktListe
        {
            set { produktListe = value; }
        }

        public Common.FakturaGrunnlag FakturaGrunnlag
        {
            get { return fakturaGrunnlag; }
            set { fakturaGrunnlag = value; }
        }

        private void uscFakturaMottaker_KundeValgt(object sender, EventArgs e)
        {
            fakturaGrunnlag.Kunde = uscKundeVelger.Kunde;
        }

        private UscFakturaLinje InitFakturaLinje(FakturaLinje fakturaLinje, IList<Produkt> produktListe)
        {
            UscFakturaLinje uscFakturaLinje = new UscFakturaLinje();
            uscFakturaLinje.ProduktListe = produktListe;
            uscFakturaLinje.FakturaLinje = fakturaLinje;
            uscFakturaLinje.Dock = DockStyle.Top;
            uscFakturaLinje.LeggTil += new EventHandler(uscFakturaLinje_LeggTil);
            uscFakturaLinje.Fjern += new EventHandler(uscFakturaLinje_Fjern);
            uscFakturaLinje.Tag = fakturaLinje;
            return uscFakturaLinje;
        }

        private void uscFakturaLinje_LeggTil(object sender, EventArgs e)
        {
            pnFakturaLinjer.SuspendLayout();
            UscFakturaLinje uscKlikketFakturalinje = (UscFakturaLinje)sender;
            uscKlikketFakturalinje.RedigerModus = false;
            FakturaLinje fakturaLinje = uscKlikketFakturalinje.FakturaLinje;
            fakturaGrunnlag.Linjer.Add(fakturaLinje);
            uscKlikketFakturalinje.Tag = fakturaLinje;
            KalkulerFaktura();

            UscFakturaLinje uscSisteFakturaLinje = InitFakturaLinje(new FakturaLinje(), produktListe);
            pnFakturaLinjer.Controls.Add(uscSisteFakturaLinje);
            uscSisteFakturaLinje.BringToFront();
            pnFakturaLinjer.ResumeLayout();
        }

        private void KalkulerFaktura()
        {
            p_FakturaGrunnlag.Kalkuler();
            lblBeløp.Text = Convert.ToString(fakturaGrunnlag.Beløp);
            lblMva.Text = Convert.ToString(fakturaGrunnlag.Mva);
            lblTotalt.Text = Convert.ToString(fakturaGrunnlag.Sum);
        }

        private void uscFakturaLinje_Fjern(object sender, EventArgs e)
        {
            pnFakturaLinjer.SuspendLayout();
            UscFakturaLinje uscKlikketFakturalinje = (UscFakturaLinje)sender;
            uscKlikketFakturalinje.LeggTil -= uscFakturaLinje_LeggTil;
            uscKlikketFakturalinje.Fjern -= uscFakturaLinje_Fjern;
            pnFakturaLinjer.Controls.Remove(uscKlikketFakturalinje);
            FakturaLinje fakturaLinje = (FakturaLinje)uscKlikketFakturalinje.Tag;
            if (fakturaLinje.Id == -1)
                fakturaGrunnlag.Linjer.Remove(fakturaLinje);
            else
                fakturaLinje.SlettFlagg = true;
            KalkulerFaktura();

            uscKlikketFakturalinje.Dispose();
            pnFakturaLinjer.ResumeLayout();
        }

        private void btnLagre_Click(object sender, EventArgs e)
        {
            lagreFakturaGrunnlagTask_Run();
        }

        private void txtReferanse_TextChanged(object sender, EventArgs e)
        {
            fakturaGrunnlag.Referanse = txtReferanse.Text;
        }

        private void lagreFakturaGrunnlagTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_FakturaGrunnlag.LagreFaktura();
        }

        private void lagreFakturaGrunnlagTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void initFakturaGrunnlagTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int fakturaId = Convert.ToInt32(e.Argument);
            p_FakturaGrunnlag.Init(fakturaId);
        }

        private void initFakturaGrunnlagTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (fakturaGrunnlag != null)
            {
                uscKundeVelger.KundeListe = kundeListe;
                uscKundeVelger.Kunde = fakturaGrunnlag.Kunde;
                for (int i = 0; i < fakturaGrunnlag.Linjer.Count; i++)
                {
                    UscFakturaLinje uscFakturaLinje = InitFakturaLinje(fakturaGrunnlag.Linjer[i], produktListe);
                    uscFakturaLinje.RedigerModus = false;
                    pnFakturaLinjer.Controls.Add(uscFakturaLinje);
                    uscFakturaLinje.BringToFront();
                }
                UscFakturaLinje uscSisteFakturaLinje = InitFakturaLinje(new FakturaLinje(), produktListe);
                pnFakturaLinjer.Controls.Add(uscSisteFakturaLinje);
                uscSisteFakturaLinje.BringToFront();
                txtReferanse.Text = fakturaGrunnlag.Referanse;
                txtBeskrivelse.Text = fakturaGrunnlag.Beskrivelse;
                lblBeløp.Text = Convert.ToString(fakturaGrunnlag.Beløp);
                lblMva.Text = Convert.ToString(fakturaGrunnlag.Mva);
                lblTotalt.Text = Convert.ToString(fakturaGrunnlag.Sum);
            }
        }

        private void initFakturaGrunnlagTask_Run(object argument)
        {
            if (!initFakturaGrunnlagTask.IsBusy)
            {
                initFakturaGrunnlagTask.RunWorkerAsync(argument);
            }
        }

        private void lagreFakturaGrunnlagTask_Run()
        {
            if (!lagreFakturaGrunnlagTask.IsBusy)
            {
                lagreFakturaGrunnlagTask.RunWorkerAsync();
            }
        }

        private void txtBeskrivelse_TextChanged(object sender, EventArgs e)
        {
            fakturaGrunnlag.Beskrivelse = txtBeskrivelse.Text;
        }

        private void V_FakturaGrunnlag_Load(object sender, EventArgs e)
        {
            if (fakturaGrunnlagId != -1)
                this.Text = "Rediger fakturagrunnlag";
            else
                this.Text = "Nytt fakturagrunnlag";
            initFakturaGrunnlagTask_Run(fakturaGrunnlagId);
        } 
    }
}
