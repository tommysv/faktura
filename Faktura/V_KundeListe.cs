﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;

namespace Faktura
{
    public partial class V_KundeListe : UserControl, IV_ToolStrip, IV_KundeListe, IPrintable, IReportStatus
    {
        private P_KundeListe p_KundeListe;
        private IList<Kunde> kundeListe;
        private Kunde valgtKunde;
        public event ReportStatusEventHandler ReportStatus;

        public V_KundeListe()
        {
            InitializeComponent();
            lwKundeListe.ColumnWeights = new float[] { 0.2f, 0.3f, 0.1f, 0.2f, 0.1f, 0.1f };
            p_KundeListe = new P_KundeListe(this);
        }

        public ToolStrip ToolStrip
        {
            get
            {
                return tsKundeListe;
            }
        }

        public IList<Kunde> KundeListe
        {
            set
            {
                kundeListe = value;
            }
        }

        private void UscKundeListe_Load(object sender, EventArgs e)
        {
            lwKundeListe.ReCalculateColumnWidths();
            hentKundeListeTask_Run();
        }

        private void tsBtnNyKunde_Click(object sender, EventArgs e)
        {
            V_Kunde v_NyKunde = new V_Kunde();
            DialogResult r = v_NyKunde.ShowDialog();
            if (r == DialogResult.OK)
            {
                ((IV_Oppdaterbar)this).Oppdater(true);
            }
        }

        private void tsBtnRedigerKunde_Click(object sender, EventArgs e)
        {
            if (valgtKunde != null)
            {
                V_Kunde v_RedigerKunde = new V_Kunde();
                v_RedigerKunde.Kunde = valgtKunde;
                DialogResult r = v_RedigerKunde.ShowDialog();
                if (r == DialogResult.OK)
                {
                    ((IV_Oppdaterbar)this).Oppdater(true);
                }
            }
        }

        private void lwKundeListe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lwKundeListe.SelectedIndices.Count > 0)
            {
                valgtKunde = kundeListe[lwKundeListe.SelectedIndices[0]];
                tsBtnRedigerKunde.Visible = true;
            }
            else
            {
                tsBtnRedigerKunde.Visible = false;
            }
        }

        private void V_KundeListe_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                lwKundeListe.Select();
            }
        }

        private void hentKundeListeTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_KundeListe.HentKundeListe();
        }

        private void DrawListViewItems()
        {
            lwKundeListe.BeginUpdate();
            lwKundeListe.Items.Clear();
            if ((kundeListe != null) && (kundeListe.Count > 0))
            {
                for (int n = 0; n < kundeListe.Count; n++)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = kundeListe[n].Navn;
                    item.SubItems.Add(kundeListe[n].Postadresse);
                    item.SubItems.Add(kundeListe[n].PostNr);
                    item.SubItems.Add(kundeListe[n].PostSted);
                    item.SubItems.Add(kundeListe[n].FakturaEpost);
                    item.SubItems.Add(Convert.ToString(kundeListe[n].KundeType));
                    lwKundeListe.Items.Add(item);
                }
            }
            lwKundeListe.EndUpdate();
        }

        private void hentKundeListeTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                DrawListViewItems();
                hentKundeListeTask_End();
            }
            else
            {
                hentKundeListeTask_Error();
            }
        }

        private void hentKundeListeTask_Run()
        {
            if (!hentKundeListeTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Henter kundeliste..."));
                hentKundeListeTask.RunWorkerAsync();
            }
        }

        private void hentKundeListeTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void hentKundeListeTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved henting av kundeliste", true));
        }

        void IPrintable.Print()
        {
            throw new NotImplementedException();
        }

        protected void OnReportStatus(ReportStatusEventArgs e)
        {
            if (ReportStatus != null)
            {
                ReportStatus(this, e);
            }
        }

        void IV_Oppdaterbar.Oppdater(bool bLast)
        {
            if (bLast)
                hentKundeListeTask_Run();
            else
                DrawListViewItems();
        }

    }
}
