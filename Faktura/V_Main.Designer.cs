﻿namespace Faktura
{
    partial class V_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(V_Main));
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsButton_FakturagrunnlagListe = new System.Windows.Forms.ToolStripButton();
            this.tsButton_FakturaListe = new System.Windows.Forms.ToolStripButton();
            this.tsButton_KundeListe = new System.Windows.Forms.ToolStripButton();
            this.tsButton_ProduktListe = new System.Windows.Forms.ToolStripButton();
            this.tsButton_Logg = new System.Windows.Forms.ToolStripButton();
            this.tsButton_Rapporter = new System.Windows.Forms.ToolStripButton();
            this.tsButton_Utskrift = new System.Windows.Forms.ToolStripButton();
            this.tsButton_Konfig = new System.Windows.Forms.ToolStripButton();
            this.tsButton_Om = new System.Windows.Forms.ToolStripButton();
            this.ssMain = new System.Windows.Forms.StatusStrip();
            this.ssStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsMain.SuspendLayout();
            this.ssMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsMain
            // 
            this.tsMain.BackColor = System.Drawing.Color.GhostWhite;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsButton_FakturagrunnlagListe,
            this.tsButton_FakturaListe,
            this.tsButton_KundeListe,
            this.tsButton_ProduktListe,
            this.tsButton_Logg,
            this.tsButton_Rapporter,
            this.tsButton_Utskrift,
            this.tsButton_Konfig,
            this.tsButton_Om});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Padding = new System.Windows.Forms.Padding(0);
            this.tsMain.Size = new System.Drawing.Size(784, 37);
            this.tsMain.TabIndex = 1;
            // 
            // tsButton_FakturagrunnlagListe
            // 
            this.tsButton_FakturagrunnlagListe.Checked = true;
            this.tsButton_FakturagrunnlagListe.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsButton_FakturagrunnlagListe.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_FakturagrunnlagListe.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsButton_FakturagrunnlagListe.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_FakturagrunnlagListe.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_FakturagrunnlagListe.Name = "tsButton_FakturagrunnlagListe";
            this.tsButton_FakturagrunnlagListe.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_FakturagrunnlagListe.Size = new System.Drawing.Size(34, 34);
            this.tsButton_FakturagrunnlagListe.Click += new System.EventHandler(this.tsButton_FakturagrunnlagListe_Click);
            // 
            // tsButton_FakturaListe
            // 
            this.tsButton_FakturaListe.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_FakturaListe.Image = global::Faktura.Properties.Resources.invoices;
            this.tsButton_FakturaListe.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_FakturaListe.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_FakturaListe.Name = "tsButton_FakturaListe";
            this.tsButton_FakturaListe.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_FakturaListe.Size = new System.Drawing.Size(34, 34);
            this.tsButton_FakturaListe.Click += new System.EventHandler(this.tsButton_FakturaListe_Click);
            // 
            // tsButton_KundeListe
            // 
            this.tsButton_KundeListe.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_KundeListe.Image = global::Faktura.Properties.Resources.customers;
            this.tsButton_KundeListe.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_KundeListe.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_KundeListe.Name = "tsButton_KundeListe";
            this.tsButton_KundeListe.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_KundeListe.Size = new System.Drawing.Size(34, 34);
            this.tsButton_KundeListe.Click += new System.EventHandler(this.tsButton_KundeListe_Click);
            // 
            // tsButton_ProduktListe
            // 
            this.tsButton_ProduktListe.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_ProduktListe.Image = ((System.Drawing.Image)(resources.GetObject("tsButton_ProduktListe.Image")));
            this.tsButton_ProduktListe.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_ProduktListe.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_ProduktListe.Name = "tsButton_ProduktListe";
            this.tsButton_ProduktListe.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_ProduktListe.Size = new System.Drawing.Size(34, 34);
            this.tsButton_ProduktListe.Click += new System.EventHandler(this.tsButton_ProduktListe_Click);
            // 
            // tsButton_Logg
            // 
            this.tsButton_Logg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_Logg.Image = ((System.Drawing.Image)(resources.GetObject("tsButton_Logg.Image")));
            this.tsButton_Logg.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_Logg.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_Logg.Name = "tsButton_Logg";
            this.tsButton_Logg.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_Logg.Size = new System.Drawing.Size(34, 34);
            this.tsButton_Logg.Click += new System.EventHandler(this.tsButton_Logg_Click);
            // 
            // tsButton_Rapporter
            // 
            this.tsButton_Rapporter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_Rapporter.Image = global::Faktura.Properties.Resources.reports;
            this.tsButton_Rapporter.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_Rapporter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_Rapporter.Name = "tsButton_Rapporter";
            this.tsButton_Rapporter.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_Rapporter.Size = new System.Drawing.Size(34, 34);
            this.tsButton_Rapporter.Click += new System.EventHandler(this.tsButton_Rapporter_Click);
            // 
            // tsButton_Utskrift
            // 
            this.tsButton_Utskrift.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_Utskrift.Image = global::Faktura.Properties.Resources.print;
            this.tsButton_Utskrift.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_Utskrift.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_Utskrift.Name = "tsButton_Utskrift";
            this.tsButton_Utskrift.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_Utskrift.Size = new System.Drawing.Size(34, 34);
            this.tsButton_Utskrift.Click += new System.EventHandler(this.tsButton_Utskrift_Click);
            // 
            // tsButton_Konfig
            // 
            this.tsButton_Konfig.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_Konfig.Image = global::Faktura.Properties.Resources.configuration;
            this.tsButton_Konfig.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_Konfig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_Konfig.Name = "tsButton_Konfig";
            this.tsButton_Konfig.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_Konfig.Size = new System.Drawing.Size(34, 34);
            this.tsButton_Konfig.Click += new System.EventHandler(this.tsButton_Konfig_Click);
            // 
            // tsButton_Om
            // 
            this.tsButton_Om.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButton_Om.Image = global::Faktura.Properties.Resources.about;
            this.tsButton_Om.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButton_Om.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButton_Om.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.tsButton_Om.Name = "tsButton_Om";
            this.tsButton_Om.Padding = new System.Windows.Forms.Padding(3);
            this.tsButton_Om.Size = new System.Drawing.Size(34, 34);
            // 
            // ssMain
            // 
            this.ssMain.BackColor = System.Drawing.Color.GhostWhite;
            this.ssMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ssStatusLabel});
            this.ssMain.Location = new System.Drawing.Point(0, 540);
            this.ssMain.Name = "ssMain";
            this.ssMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.ssMain.Size = new System.Drawing.Size(784, 22);
            this.ssMain.TabIndex = 2;
            // 
            // ssStatusLabel
            // 
            this.ssStatusLabel.Name = "ssStatusLabel";
            this.ssStatusLabel.Size = new System.Drawing.Size(769, 17);
            this.ssStatusLabel.Spring = true;
            this.ssStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.ssMain);
            this.Controls.Add(this.tsMain);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "V_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Faktura";
            this.Load += new System.EventHandler(this.V_Main_Load);
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.ssMain.ResumeLayout(false);
            this.ssMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tsButton_FakturaListe;
        private System.Windows.Forms.ToolStripButton tsButton_KundeListe;
        private System.Windows.Forms.ToolStripButton tsButton_ProduktListe;
        private System.Windows.Forms.ToolStripButton tsButton_Konfig;
        private System.Windows.Forms.ToolStripButton tsButton_Rapporter;
        private System.Windows.Forms.ToolStripButton tsButton_Om;
        private System.Windows.Forms.ToolStripButton tsButton_Utskrift;
        private System.Windows.Forms.ToolStripButton tsButton_Logg;
        private System.Windows.Forms.StatusStrip ssMain;
        private System.Windows.Forms.ToolStripStatusLabel ssStatusLabel;
        private System.Windows.Forms.ToolStripButton tsButton_FakturagrunnlagListe;
    }
}

