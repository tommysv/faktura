﻿namespace Faktura
{
    partial class V_Innstillinger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcInnstillinger = new System.Windows.Forms.TabControl();
            this.tpForetak = new System.Windows.Forms.TabPage();
            this.nuFakturagebyr = new System.Windows.Forms.NumericUpDown();
            this.nuForfallsdager = new System.Windows.Forms.NumericUpDown();
            this.lblKroner = new System.Windows.Forms.Label();
            this.lblFakturaGebyr = new System.Windows.Forms.Label();
            this.lblNettsted = new System.Windows.Forms.Label();
            this.txtNettsted = new System.Windows.Forms.TextBox();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.cbMvaPliktig = new System.Windows.Forms.CheckBox();
            this.lblForfallsdager = new System.Windows.Forms.Label();
            this.txtEpost = new System.Windows.Forms.TextBox();
            this.lblEpost = new System.Windows.Forms.Label();
            this.txtKontonr = new System.Windows.Forms.TextBox();
            this.lblKontonr = new System.Windows.Forms.Label();
            this.txtOrgnr = new System.Windows.Forms.TextBox();
            this.lblOrgnr = new System.Windows.Forms.Label();
            this.txtPoststed = new System.Windows.Forms.TextBox();
            this.lblPoststed = new System.Windows.Forms.Label();
            this.txtPostnr = new System.Windows.Forms.TextBox();
            this.lblPostnr = new System.Windows.Forms.Label();
            this.txtAdresse2 = new System.Windows.Forms.TextBox();
            this.lblAdresse2 = new System.Windows.Forms.Label();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.lblAdresse = new System.Windows.Forms.Label();
            this.txtForetak = new System.Windows.Forms.TextBox();
            this.lblForetak = new System.Windows.Forms.Label();
            this.tpFarger = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMerking = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblTema = new System.Windows.Forms.Label();
            this.lblFargeKombinert = new System.Windows.Forms.Label();
            this.lblFargeTekst = new System.Windows.Forms.Label();
            this.lblFargeBakgrunn = new System.Windows.Forms.Label();
            this.lblFargeInnbetaltKombinert = new System.Windows.Forms.Label();
            this.lblFargeKreditertKombinert = new System.Windows.Forms.Label();
            this.lblFargeSendtKombinert = new System.Windows.Forms.Label();
            this.lblFargeBehandlerKombinert = new System.Windows.Forms.Label();
            this._lblFargeInnbetalt = new System.Windows.Forms.Label();
            this._lblFargeKreditert = new System.Windows.Forms.Label();
            this._lblFargeSendt = new System.Windows.Forms.Label();
            this._lblFargeBehandler = new System.Windows.Forms.Label();
            this.lblFargeInnbetaltTekst = new System.Windows.Forms.Label();
            this.lblFargeKreditertTekst = new System.Windows.Forms.Label();
            this.lblFargeSendtTekst = new System.Windows.Forms.Label();
            this.lblFargeBehandlerTekst = new System.Windows.Forms.Label();
            this.lblFargeInnbetaltBakgrunn = new System.Windows.Forms.Label();
            this.lblFargeKreditertBakgrunn = new System.Windows.Forms.Label();
            this.lblFargeSendtBakgrunn = new System.Windows.Forms.Label();
            this.lblFargeBehandlerBakgrunn = new System.Windows.Forms.Label();
            this.tpEpost = new System.Windows.Forms.TabPage();
            this.lblEpostAvsender = new System.Windows.Forms.Label();
            this.txtEpostAvsender = new System.Windows.Forms.TextBox();
            this.cbEpostBrukSSL = new System.Windows.Forms.CheckBox();
            this.lblEpostPassord = new System.Windows.Forms.Label();
            this.txtEpostPassord = new System.Windows.Forms.TextBox();
            this.lblEpostBrukernavn = new System.Windows.Forms.Label();
            this.txtEpostBrukernavn = new System.Windows.Forms.TextBox();
            this.lblEpostPort = new System.Windows.Forms.Label();
            this.txtEpostPort = new System.Windows.Forms.TextBox();
            this.lblEpostServer = new System.Windows.Forms.Label();
            this.txtEpostServer = new System.Windows.Forms.TextBox();
            this.lblEpostTilbyder = new System.Windows.Forms.Label();
            this.cbxEpostTilbyder = new System.Windows.Forms.ComboBox();
            this.btnLagre = new System.Windows.Forms.Button();
            this.fargeDialog = new System.Windows.Forms.ColorDialog();
            this.tcInnstillinger.SuspendLayout();
            this.tpForetak.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuFakturagebyr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nuForfallsdager)).BeginInit();
            this.tpFarger.SuspendLayout();
            this.tpEpost.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcInnstillinger
            // 
            this.tcInnstillinger.Controls.Add(this.tpForetak);
            this.tcInnstillinger.Controls.Add(this.tpFarger);
            this.tcInnstillinger.Controls.Add(this.tpEpost);
            this.tcInnstillinger.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcInnstillinger.Location = new System.Drawing.Point(0, 0);
            this.tcInnstillinger.Name = "tcInnstillinger";
            this.tcInnstillinger.SelectedIndex = 0;
            this.tcInnstillinger.Size = new System.Drawing.Size(379, 401);
            this.tcInnstillinger.TabIndex = 0;
            // 
            // tpForetak
            // 
            this.tpForetak.BackColor = System.Drawing.Color.White;
            this.tpForetak.Controls.Add(this.nuFakturagebyr);
            this.tpForetak.Controls.Add(this.nuForfallsdager);
            this.tpForetak.Controls.Add(this.lblKroner);
            this.tpForetak.Controls.Add(this.lblFakturaGebyr);
            this.tpForetak.Controls.Add(this.lblNettsted);
            this.tpForetak.Controls.Add(this.txtNettsted);
            this.tpForetak.Controls.Add(this.lblTelefon);
            this.tpForetak.Controls.Add(this.txtTelefon);
            this.tpForetak.Controls.Add(this.cbMvaPliktig);
            this.tpForetak.Controls.Add(this.lblForfallsdager);
            this.tpForetak.Controls.Add(this.txtEpost);
            this.tpForetak.Controls.Add(this.lblEpost);
            this.tpForetak.Controls.Add(this.txtKontonr);
            this.tpForetak.Controls.Add(this.lblKontonr);
            this.tpForetak.Controls.Add(this.txtOrgnr);
            this.tpForetak.Controls.Add(this.lblOrgnr);
            this.tpForetak.Controls.Add(this.txtPoststed);
            this.tpForetak.Controls.Add(this.lblPoststed);
            this.tpForetak.Controls.Add(this.txtPostnr);
            this.tpForetak.Controls.Add(this.lblPostnr);
            this.tpForetak.Controls.Add(this.txtAdresse2);
            this.tpForetak.Controls.Add(this.lblAdresse2);
            this.tpForetak.Controls.Add(this.txtAdresse);
            this.tpForetak.Controls.Add(this.lblAdresse);
            this.tpForetak.Controls.Add(this.txtForetak);
            this.tpForetak.Controls.Add(this.lblForetak);
            this.tpForetak.Location = new System.Drawing.Point(4, 22);
            this.tpForetak.Name = "tpForetak";
            this.tpForetak.Padding = new System.Windows.Forms.Padding(3);
            this.tpForetak.Size = new System.Drawing.Size(371, 375);
            this.tpForetak.TabIndex = 0;
            this.tpForetak.Text = "Foretak";
            // 
            // nuFakturagebyr
            // 
            this.nuFakturagebyr.DecimalPlaces = 2;
            this.nuFakturagebyr.Location = new System.Drawing.Point(120, 334);
            this.nuFakturagebyr.Name = "nuFakturagebyr";
            this.nuFakturagebyr.Size = new System.Drawing.Size(61, 20);
            this.nuFakturagebyr.TabIndex = 45;
            this.nuFakturagebyr.ValueChanged += new System.EventHandler(this.nuFakturagebyr_ValueChanged);
            // 
            // nuForfallsdager
            // 
            this.nuForfallsdager.Location = new System.Drawing.Point(120, 301);
            this.nuForfallsdager.Name = "nuForfallsdager";
            this.nuForfallsdager.Size = new System.Drawing.Size(61, 20);
            this.nuForfallsdager.TabIndex = 44;
            this.nuForfallsdager.ValueChanged += new System.EventHandler(this.nuForfallsdager_ValueChanged);
            // 
            // lblKroner
            // 
            this.lblKroner.AutoSize = true;
            this.lblKroner.Location = new System.Drawing.Point(187, 336);
            this.lblKroner.Name = "lblKroner";
            this.lblKroner.Size = new System.Drawing.Size(16, 13);
            this.lblKroner.TabIndex = 43;
            this.lblKroner.Text = "kr";
            // 
            // lblFakturaGebyr
            // 
            this.lblFakturaGebyr.AutoSize = true;
            this.lblFakturaGebyr.Location = new System.Drawing.Point(31, 334);
            this.lblFakturaGebyr.Name = "lblFakturaGebyr";
            this.lblFakturaGebyr.Size = new System.Drawing.Size(72, 13);
            this.lblFakturaGebyr.TabIndex = 41;
            this.lblFakturaGebyr.Text = "Fakturagebyr:";
            // 
            // lblNettsted
            // 
            this.lblNettsted.AutoSize = true;
            this.lblNettsted.Location = new System.Drawing.Point(31, 249);
            this.lblNettsted.Name = "lblNettsted";
            this.lblNettsted.Size = new System.Drawing.Size(50, 13);
            this.lblNettsted.TabIndex = 40;
            this.lblNettsted.Text = "Nettsted:";
            // 
            // txtNettsted
            // 
            this.txtNettsted.Location = new System.Drawing.Point(120, 246);
            this.txtNettsted.Name = "txtNettsted";
            this.txtNettsted.Size = new System.Drawing.Size(188, 20);
            this.txtNettsted.TabIndex = 39;
            this.txtNettsted.TextChanged += new System.EventHandler(this.txtNettsted_TextChanged);
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.Location = new System.Drawing.Point(31, 223);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(46, 13);
            this.lblTelefon.TabIndex = 38;
            this.lblTelefon.Text = "Telefon:";
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(120, 220);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(99, 20);
            this.txtTelefon.TabIndex = 37;
            this.txtTelefon.TextChanged += new System.EventHandler(this.txtTelefon_TextChanged);
            // 
            // cbMvaPliktig
            // 
            this.cbMvaPliktig.AutoSize = true;
            this.cbMvaPliktig.Location = new System.Drawing.Point(231, 24);
            this.cbMvaPliktig.Name = "cbMvaPliktig";
            this.cbMvaPliktig.Size = new System.Drawing.Size(77, 17);
            this.cbMvaPliktig.TabIndex = 36;
            this.cbMvaPliktig.Text = "Mva-pliktig";
            this.cbMvaPliktig.UseVisualStyleBackColor = true;
            this.cbMvaPliktig.CheckedChanged += new System.EventHandler(this.cbMvaPliktig_CheckedChanged);
            // 
            // lblForfallsdager
            // 
            this.lblForfallsdager.AutoSize = true;
            this.lblForfallsdager.Location = new System.Drawing.Point(31, 308);
            this.lblForfallsdager.Name = "lblForfallsdager";
            this.lblForfallsdager.Size = new System.Drawing.Size(70, 13);
            this.lblForfallsdager.TabIndex = 34;
            this.lblForfallsdager.Text = "Forfallsdager:";
            // 
            // txtEpost
            // 
            this.txtEpost.Location = new System.Drawing.Point(120, 194);
            this.txtEpost.Name = "txtEpost";
            this.txtEpost.Size = new System.Drawing.Size(188, 20);
            this.txtEpost.TabIndex = 33;
            this.txtEpost.TextChanged += new System.EventHandler(this.txtEpost_TextChanged);
            // 
            // lblEpost
            // 
            this.lblEpost.AutoSize = true;
            this.lblEpost.Location = new System.Drawing.Point(31, 197);
            this.lblEpost.Name = "lblEpost";
            this.lblEpost.Size = new System.Drawing.Size(37, 13);
            this.lblEpost.TabIndex = 32;
            this.lblEpost.Text = "Epost:";
            // 
            // txtKontonr
            // 
            this.txtKontonr.Location = new System.Drawing.Point(120, 272);
            this.txtKontonr.Name = "txtKontonr";
            this.txtKontonr.Size = new System.Drawing.Size(188, 20);
            this.txtKontonr.TabIndex = 31;
            this.txtKontonr.TextChanged += new System.EventHandler(this.txtKontonr_TextChanged);
            // 
            // lblKontonr
            // 
            this.lblKontonr.AutoSize = true;
            this.lblKontonr.Location = new System.Drawing.Point(31, 275);
            this.lblKontonr.Name = "lblKontonr";
            this.lblKontonr.Size = new System.Drawing.Size(50, 13);
            this.lblKontonr.TabIndex = 30;
            this.lblKontonr.Text = "Konto-nr:";
            // 
            // txtOrgnr
            // 
            this.txtOrgnr.Location = new System.Drawing.Point(120, 21);
            this.txtOrgnr.Name = "txtOrgnr";
            this.txtOrgnr.Size = new System.Drawing.Size(99, 20);
            this.txtOrgnr.TabIndex = 29;
            this.txtOrgnr.TextChanged += new System.EventHandler(this.txtOrgnr_TextChanged);
            // 
            // lblOrgnr
            // 
            this.lblOrgnr.AutoSize = true;
            this.lblOrgnr.Location = new System.Drawing.Point(31, 24);
            this.lblOrgnr.Name = "lblOrgnr";
            this.lblOrgnr.Size = new System.Drawing.Size(39, 13);
            this.lblOrgnr.TabIndex = 28;
            this.lblOrgnr.Text = "Org-nr:";
            // 
            // txtPoststed
            // 
            this.txtPoststed.Location = new System.Drawing.Point(120, 168);
            this.txtPoststed.Name = "txtPoststed";
            this.txtPoststed.Size = new System.Drawing.Size(188, 20);
            this.txtPoststed.TabIndex = 27;
            this.txtPoststed.TextChanged += new System.EventHandler(this.txtPoststed_TextChanged);
            // 
            // lblPoststed
            // 
            this.lblPoststed.AutoSize = true;
            this.lblPoststed.Location = new System.Drawing.Point(31, 171);
            this.lblPoststed.Name = "lblPoststed";
            this.lblPoststed.Size = new System.Drawing.Size(51, 13);
            this.lblPoststed.TabIndex = 26;
            this.lblPoststed.Text = "Poststed:";
            // 
            // txtPostnr
            // 
            this.txtPostnr.Location = new System.Drawing.Point(120, 141);
            this.txtPostnr.Name = "txtPostnr";
            this.txtPostnr.Size = new System.Drawing.Size(38, 20);
            this.txtPostnr.TabIndex = 25;
            this.txtPostnr.TextChanged += new System.EventHandler(this.txtPostnr_TextChanged);
            // 
            // lblPostnr
            // 
            this.lblPostnr.AutoSize = true;
            this.lblPostnr.Location = new System.Drawing.Point(31, 145);
            this.lblPostnr.Name = "lblPostnr";
            this.lblPostnr.Size = new System.Drawing.Size(40, 13);
            this.lblPostnr.TabIndex = 24;
            this.lblPostnr.Text = "Postnr:";
            // 
            // txtAdresse2
            // 
            this.txtAdresse2.Location = new System.Drawing.Point(120, 115);
            this.txtAdresse2.Name = "txtAdresse2";
            this.txtAdresse2.Size = new System.Drawing.Size(188, 20);
            this.txtAdresse2.TabIndex = 23;
            this.txtAdresse2.TextChanged += new System.EventHandler(this.txtAdresse2_TextChanged);
            // 
            // lblAdresse2
            // 
            this.lblAdresse2.AutoSize = true;
            this.lblAdresse2.Location = new System.Drawing.Point(31, 118);
            this.lblAdresse2.Name = "lblAdresse2";
            this.lblAdresse2.Size = new System.Drawing.Size(57, 13);
            this.lblAdresse2.TabIndex = 22;
            this.lblAdresse2.Text = "Adresse 2:";
            // 
            // txtAdresse
            // 
            this.txtAdresse.Location = new System.Drawing.Point(120, 88);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(188, 20);
            this.txtAdresse.TabIndex = 21;
            this.txtAdresse.TextChanged += new System.EventHandler(this.txtAdresse_TextChanged);
            // 
            // lblAdresse
            // 
            this.lblAdresse.AutoSize = true;
            this.lblAdresse.Location = new System.Drawing.Point(31, 92);
            this.lblAdresse.Name = "lblAdresse";
            this.lblAdresse.Size = new System.Drawing.Size(48, 13);
            this.lblAdresse.TabIndex = 20;
            this.lblAdresse.Text = "Adresse:";
            // 
            // txtForetak
            // 
            this.txtForetak.Location = new System.Drawing.Point(120, 62);
            this.txtForetak.Name = "txtForetak";
            this.txtForetak.Size = new System.Drawing.Size(188, 20);
            this.txtForetak.TabIndex = 19;
            this.txtForetak.TextChanged += new System.EventHandler(this.txtForetak_TextChanged);
            // 
            // lblForetak
            // 
            this.lblForetak.AutoSize = true;
            this.lblForetak.Location = new System.Drawing.Point(31, 62);
            this.lblForetak.Name = "lblForetak";
            this.lblForetak.Size = new System.Drawing.Size(46, 13);
            this.lblForetak.TabIndex = 18;
            this.lblForetak.Text = "Foretak:";
            // 
            // tpFarger
            // 
            this.tpFarger.Controls.Add(this.label1);
            this.tpFarger.Controls.Add(this.lblMerking);
            this.tpFarger.Controls.Add(this.comboBox1);
            this.tpFarger.Controls.Add(this.lblTema);
            this.tpFarger.Controls.Add(this.lblFargeKombinert);
            this.tpFarger.Controls.Add(this.lblFargeTekst);
            this.tpFarger.Controls.Add(this.lblFargeBakgrunn);
            this.tpFarger.Controls.Add(this.lblFargeInnbetaltKombinert);
            this.tpFarger.Controls.Add(this.lblFargeKreditertKombinert);
            this.tpFarger.Controls.Add(this.lblFargeSendtKombinert);
            this.tpFarger.Controls.Add(this.lblFargeBehandlerKombinert);
            this.tpFarger.Controls.Add(this._lblFargeInnbetalt);
            this.tpFarger.Controls.Add(this._lblFargeKreditert);
            this.tpFarger.Controls.Add(this._lblFargeSendt);
            this.tpFarger.Controls.Add(this._lblFargeBehandler);
            this.tpFarger.Controls.Add(this.lblFargeInnbetaltTekst);
            this.tpFarger.Controls.Add(this.lblFargeKreditertTekst);
            this.tpFarger.Controls.Add(this.lblFargeSendtTekst);
            this.tpFarger.Controls.Add(this.lblFargeBehandlerTekst);
            this.tpFarger.Controls.Add(this.lblFargeInnbetaltBakgrunn);
            this.tpFarger.Controls.Add(this.lblFargeKreditertBakgrunn);
            this.tpFarger.Controls.Add(this.lblFargeSendtBakgrunn);
            this.tpFarger.Controls.Add(this.lblFargeBehandlerBakgrunn);
            this.tpFarger.Location = new System.Drawing.Point(4, 22);
            this.tpFarger.Name = "tpFarger";
            this.tpFarger.Size = new System.Drawing.Size(371, 375);
            this.tpFarger.TabIndex = 1;
            this.tpFarger.Text = "Farger";
            this.tpFarger.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(114, 272);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 22;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMerking
            // 
            this.lblMerking.AutoSize = true;
            this.lblMerking.Location = new System.Drawing.Point(52, 276);
            this.lblMerking.Name = "lblMerking";
            this.lblMerking.Size = new System.Drawing.Size(48, 13);
            this.lblMerking.TabIndex = 21;
            this.lblMerking.Text = "Merking:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(114, 48);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(183, 21);
            this.comboBox1.TabIndex = 20;
            // 
            // lblTema
            // 
            this.lblTema.AutoSize = true;
            this.lblTema.Location = new System.Drawing.Point(51, 48);
            this.lblTema.Name = "lblTema";
            this.lblTema.Size = new System.Drawing.Size(37, 13);
            this.lblTema.TabIndex = 19;
            this.lblTema.Text = "Tema:";
            // 
            // lblFargeKombinert
            // 
            this.lblFargeKombinert.AutoSize = true;
            this.lblFargeKombinert.Location = new System.Drawing.Point(237, 136);
            this.lblFargeKombinert.Name = "lblFargeKombinert";
            this.lblFargeKombinert.Size = new System.Drawing.Size(54, 13);
            this.lblFargeKombinert.TabIndex = 18;
            this.lblFargeKombinert.Text = "Kombinert";
            // 
            // lblFargeTekst
            // 
            this.lblFargeTekst.AutoSize = true;
            this.lblFargeTekst.Location = new System.Drawing.Point(174, 136);
            this.lblFargeTekst.Name = "lblFargeTekst";
            this.lblFargeTekst.Size = new System.Drawing.Size(34, 13);
            this.lblFargeTekst.TabIndex = 17;
            this.lblFargeTekst.Text = "Tekst";
            // 
            // lblFargeBakgrunn
            // 
            this.lblFargeBakgrunn.AutoSize = true;
            this.lblFargeBakgrunn.Location = new System.Drawing.Point(111, 136);
            this.lblFargeBakgrunn.Name = "lblFargeBakgrunn";
            this.lblFargeBakgrunn.Size = new System.Drawing.Size(53, 13);
            this.lblFargeBakgrunn.TabIndex = 16;
            this.lblFargeBakgrunn.Text = "Bakgrunn";
            // 
            // lblFargeInnbetaltKombinert
            // 
            this.lblFargeInnbetaltKombinert.BackColor = System.Drawing.Color.White;
            this.lblFargeInnbetaltKombinert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeInnbetaltKombinert.ForeColor = System.Drawing.Color.Black;
            this.lblFargeInnbetaltKombinert.Location = new System.Drawing.Point(240, 234);
            this.lblFargeInnbetaltKombinert.Name = "lblFargeInnbetaltKombinert";
            this.lblFargeInnbetaltKombinert.Size = new System.Drawing.Size(57, 20);
            this.lblFargeInnbetaltKombinert.TabIndex = 15;
            this.lblFargeInnbetaltKombinert.Text = "Tekst";
            this.lblFargeInnbetaltKombinert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFargeKreditertKombinert
            // 
            this.lblFargeKreditertKombinert.BackColor = System.Drawing.Color.White;
            this.lblFargeKreditertKombinert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeKreditertKombinert.ForeColor = System.Drawing.Color.Black;
            this.lblFargeKreditertKombinert.Location = new System.Drawing.Point(240, 211);
            this.lblFargeKreditertKombinert.Name = "lblFargeKreditertKombinert";
            this.lblFargeKreditertKombinert.Size = new System.Drawing.Size(57, 20);
            this.lblFargeKreditertKombinert.TabIndex = 14;
            this.lblFargeKreditertKombinert.Text = "Tekst";
            this.lblFargeKreditertKombinert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFargeSendtKombinert
            // 
            this.lblFargeSendtKombinert.BackColor = System.Drawing.Color.White;
            this.lblFargeSendtKombinert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeSendtKombinert.ForeColor = System.Drawing.Color.Black;
            this.lblFargeSendtKombinert.Location = new System.Drawing.Point(240, 188);
            this.lblFargeSendtKombinert.Name = "lblFargeSendtKombinert";
            this.lblFargeSendtKombinert.Size = new System.Drawing.Size(57, 20);
            this.lblFargeSendtKombinert.TabIndex = 13;
            this.lblFargeSendtKombinert.Text = "Tekst";
            this.lblFargeSendtKombinert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFargeBehandlerKombinert
            // 
            this.lblFargeBehandlerKombinert.BackColor = System.Drawing.Color.White;
            this.lblFargeBehandlerKombinert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeBehandlerKombinert.ForeColor = System.Drawing.Color.Black;
            this.lblFargeBehandlerKombinert.Location = new System.Drawing.Point(240, 165);
            this.lblFargeBehandlerKombinert.Name = "lblFargeBehandlerKombinert";
            this.lblFargeBehandlerKombinert.Size = new System.Drawing.Size(57, 20);
            this.lblFargeBehandlerKombinert.TabIndex = 12;
            this.lblFargeBehandlerKombinert.Text = "Tekst";
            this.lblFargeBehandlerKombinert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblFargeInnbetalt
            // 
            this._lblFargeInnbetalt.AutoSize = true;
            this._lblFargeInnbetalt.Location = new System.Drawing.Point(48, 238);
            this._lblFargeInnbetalt.Name = "_lblFargeInnbetalt";
            this._lblFargeInnbetalt.Size = new System.Drawing.Size(51, 13);
            this._lblFargeInnbetalt.TabIndex = 6;
            this._lblFargeInnbetalt.Text = "Innbetalt:";
            // 
            // _lblFargeKreditert
            // 
            this._lblFargeKreditert.AutoSize = true;
            this._lblFargeKreditert.Location = new System.Drawing.Point(50, 215);
            this._lblFargeKreditert.Name = "_lblFargeKreditert";
            this._lblFargeKreditert.Size = new System.Drawing.Size(49, 13);
            this._lblFargeKreditert.TabIndex = 4;
            this._lblFargeKreditert.Text = "Kreditert:";
            // 
            // _lblFargeSendt
            // 
            this._lblFargeSendt.AutoSize = true;
            this._lblFargeSendt.Location = new System.Drawing.Point(61, 192);
            this._lblFargeSendt.Name = "_lblFargeSendt";
            this._lblFargeSendt.Size = new System.Drawing.Size(38, 13);
            this._lblFargeSendt.TabIndex = 2;
            this._lblFargeSendt.Text = "Sendt:";
            // 
            // _lblFargeBehandler
            // 
            this._lblFargeBehandler.AutoSize = true;
            this._lblFargeBehandler.Location = new System.Drawing.Point(41, 166);
            this._lblFargeBehandler.Name = "_lblFargeBehandler";
            this._lblFargeBehandler.Size = new System.Drawing.Size(58, 13);
            this._lblFargeBehandler.TabIndex = 0;
            this._lblFargeBehandler.Text = "Behandler:";
            // 
            // lblFargeInnbetaltTekst
            // 
            this.lblFargeInnbetaltTekst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeInnbetaltTekst.Location = new System.Drawing.Point(177, 234);
            this.lblFargeInnbetaltTekst.Name = "lblFargeInnbetaltTekst";
            this.lblFargeInnbetaltTekst.Size = new System.Drawing.Size(57, 20);
            this.lblFargeInnbetaltTekst.TabIndex = 11;
            this.lblFargeInnbetaltTekst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeInnbetaltTekst.Click += new System.EventHandler(this.lblFargeInnbetaltTekst_Click);
            // 
            // lblFargeKreditertTekst
            // 
            this.lblFargeKreditertTekst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeKreditertTekst.Location = new System.Drawing.Point(177, 211);
            this.lblFargeKreditertTekst.Name = "lblFargeKreditertTekst";
            this.lblFargeKreditertTekst.Size = new System.Drawing.Size(57, 20);
            this.lblFargeKreditertTekst.TabIndex = 10;
            this.lblFargeKreditertTekst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeKreditertTekst.Click += new System.EventHandler(this.lblFargeKreditertTekst_Click);
            // 
            // lblFargeSendtTekst
            // 
            this.lblFargeSendtTekst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeSendtTekst.Location = new System.Drawing.Point(177, 188);
            this.lblFargeSendtTekst.Name = "lblFargeSendtTekst";
            this.lblFargeSendtTekst.Size = new System.Drawing.Size(57, 20);
            this.lblFargeSendtTekst.TabIndex = 9;
            this.lblFargeSendtTekst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeSendtTekst.Click += new System.EventHandler(this.lblFargeSendtTekst_Click);
            // 
            // lblFargeBehandlerTekst
            // 
            this.lblFargeBehandlerTekst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeBehandlerTekst.Location = new System.Drawing.Point(177, 165);
            this.lblFargeBehandlerTekst.Name = "lblFargeBehandlerTekst";
            this.lblFargeBehandlerTekst.Size = new System.Drawing.Size(57, 20);
            this.lblFargeBehandlerTekst.TabIndex = 8;
            this.lblFargeBehandlerTekst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeBehandlerTekst.Click += new System.EventHandler(this.lblFargeBehandlerTekst_Click);
            // 
            // lblFargeInnbetaltBakgrunn
            // 
            this.lblFargeInnbetaltBakgrunn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeInnbetaltBakgrunn.Location = new System.Drawing.Point(114, 234);
            this.lblFargeInnbetaltBakgrunn.Name = "lblFargeInnbetaltBakgrunn";
            this.lblFargeInnbetaltBakgrunn.Size = new System.Drawing.Size(57, 20);
            this.lblFargeInnbetaltBakgrunn.TabIndex = 7;
            this.lblFargeInnbetaltBakgrunn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeInnbetaltBakgrunn.Click += new System.EventHandler(this.lblFargeInnbetaltBakgrunn_Click);
            // 
            // lblFargeKreditertBakgrunn
            // 
            this.lblFargeKreditertBakgrunn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeKreditertBakgrunn.Location = new System.Drawing.Point(114, 211);
            this.lblFargeKreditertBakgrunn.Name = "lblFargeKreditertBakgrunn";
            this.lblFargeKreditertBakgrunn.Size = new System.Drawing.Size(57, 20);
            this.lblFargeKreditertBakgrunn.TabIndex = 5;
            this.lblFargeKreditertBakgrunn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeKreditertBakgrunn.Click += new System.EventHandler(this.lblFargeKreditertBakgrunn_Click);
            // 
            // lblFargeSendtBakgrunn
            // 
            this.lblFargeSendtBakgrunn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeSendtBakgrunn.Location = new System.Drawing.Point(114, 188);
            this.lblFargeSendtBakgrunn.Name = "lblFargeSendtBakgrunn";
            this.lblFargeSendtBakgrunn.Size = new System.Drawing.Size(57, 20);
            this.lblFargeSendtBakgrunn.TabIndex = 3;
            this.lblFargeSendtBakgrunn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeSendtBakgrunn.Click += new System.EventHandler(this.lblFargeSendtBakgrunn_Click);
            // 
            // lblFargeBehandlerBakgrunn
            // 
            this.lblFargeBehandlerBakgrunn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFargeBehandlerBakgrunn.Location = new System.Drawing.Point(114, 165);
            this.lblFargeBehandlerBakgrunn.Name = "lblFargeBehandlerBakgrunn";
            this.lblFargeBehandlerBakgrunn.Size = new System.Drawing.Size(57, 20);
            this.lblFargeBehandlerBakgrunn.TabIndex = 1;
            this.lblFargeBehandlerBakgrunn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFargeBehandlerBakgrunn.Click += new System.EventHandler(this.lblFargeBehandlerBakgrunn_Click);
            // 
            // tpEpost
            // 
            this.tpEpost.Controls.Add(this.lblEpostAvsender);
            this.tpEpost.Controls.Add(this.txtEpostAvsender);
            this.tpEpost.Controls.Add(this.cbEpostBrukSSL);
            this.tpEpost.Controls.Add(this.lblEpostPassord);
            this.tpEpost.Controls.Add(this.txtEpostPassord);
            this.tpEpost.Controls.Add(this.lblEpostBrukernavn);
            this.tpEpost.Controls.Add(this.txtEpostBrukernavn);
            this.tpEpost.Controls.Add(this.lblEpostPort);
            this.tpEpost.Controls.Add(this.txtEpostPort);
            this.tpEpost.Controls.Add(this.lblEpostServer);
            this.tpEpost.Controls.Add(this.txtEpostServer);
            this.tpEpost.Controls.Add(this.lblEpostTilbyder);
            this.tpEpost.Controls.Add(this.cbxEpostTilbyder);
            this.tpEpost.Location = new System.Drawing.Point(4, 22);
            this.tpEpost.Name = "tpEpost";
            this.tpEpost.Size = new System.Drawing.Size(371, 375);
            this.tpEpost.TabIndex = 2;
            this.tpEpost.Text = "Epost";
            this.tpEpost.UseVisualStyleBackColor = true;
            // 
            // lblEpostAvsender
            // 
            this.lblEpostAvsender.AutoSize = true;
            this.lblEpostAvsender.Location = new System.Drawing.Point(31, 224);
            this.lblEpostAvsender.Name = "lblEpostAvsender";
            this.lblEpostAvsender.Size = new System.Drawing.Size(55, 13);
            this.lblEpostAvsender.TabIndex = 16;
            this.lblEpostAvsender.Text = "Avsender:";
            // 
            // txtEpostAvsender
            // 
            this.txtEpostAvsender.Location = new System.Drawing.Point(120, 218);
            this.txtEpostAvsender.Name = "txtEpostAvsender";
            this.txtEpostAvsender.Size = new System.Drawing.Size(121, 20);
            this.txtEpostAvsender.TabIndex = 15;
            // 
            // cbEpostBrukSSL
            // 
            this.cbEpostBrukSSL.AutoSize = true;
            this.cbEpostBrukSSL.Location = new System.Drawing.Point(261, 78);
            this.cbEpostBrukSSL.Name = "cbEpostBrukSSL";
            this.cbEpostBrukSSL.Size = new System.Drawing.Size(71, 17);
            this.cbEpostBrukSSL.TabIndex = 14;
            this.cbEpostBrukSSL.Text = "Bruk SSL";
            this.cbEpostBrukSSL.UseVisualStyleBackColor = true;
            // 
            // lblEpostPassord
            // 
            this.lblEpostPassord.AutoSize = true;
            this.lblEpostPassord.Location = new System.Drawing.Point(31, 187);
            this.lblEpostPassord.Name = "lblEpostPassord";
            this.lblEpostPassord.Size = new System.Drawing.Size(48, 13);
            this.lblEpostPassord.TabIndex = 12;
            this.lblEpostPassord.Text = "Passord:";
            // 
            // txtEpostPassord
            // 
            this.txtEpostPassord.Location = new System.Drawing.Point(120, 181);
            this.txtEpostPassord.Name = "txtEpostPassord";
            this.txtEpostPassord.Size = new System.Drawing.Size(121, 20);
            this.txtEpostPassord.TabIndex = 11;
            this.txtEpostPassord.UseSystemPasswordChar = true;
            // 
            // lblEpostBrukernavn
            // 
            this.lblEpostBrukernavn.AutoSize = true;
            this.lblEpostBrukernavn.Location = new System.Drawing.Point(31, 161);
            this.lblEpostBrukernavn.Name = "lblEpostBrukernavn";
            this.lblEpostBrukernavn.Size = new System.Drawing.Size(65, 13);
            this.lblEpostBrukernavn.TabIndex = 9;
            this.lblEpostBrukernavn.Text = "Brukernavn:";
            // 
            // txtEpostBrukernavn
            // 
            this.txtEpostBrukernavn.Location = new System.Drawing.Point(120, 155);
            this.txtEpostBrukernavn.Name = "txtEpostBrukernavn";
            this.txtEpostBrukernavn.Size = new System.Drawing.Size(121, 20);
            this.txtEpostBrukernavn.TabIndex = 8;
            // 
            // lblEpostPort
            // 
            this.lblEpostPort.AutoSize = true;
            this.lblEpostPort.Location = new System.Drawing.Point(31, 114);
            this.lblEpostPort.Name = "lblEpostPort";
            this.lblEpostPort.Size = new System.Drawing.Size(29, 13);
            this.lblEpostPort.TabIndex = 5;
            this.lblEpostPort.Text = "Port:";
            // 
            // txtEpostPort
            // 
            this.txtEpostPort.Location = new System.Drawing.Point(120, 111);
            this.txtEpostPort.Name = "txtEpostPort";
            this.txtEpostPort.Size = new System.Drawing.Size(33, 20);
            this.txtEpostPort.TabIndex = 4;
            // 
            // lblEpostServer
            // 
            this.lblEpostServer.AutoSize = true;
            this.lblEpostServer.Location = new System.Drawing.Point(31, 79);
            this.lblEpostServer.Name = "lblEpostServer";
            this.lblEpostServer.Size = new System.Drawing.Size(41, 13);
            this.lblEpostServer.TabIndex = 3;
            this.lblEpostServer.Text = "Server:";
            // 
            // txtEpostServer
            // 
            this.txtEpostServer.Location = new System.Drawing.Point(120, 76);
            this.txtEpostServer.Name = "txtEpostServer";
            this.txtEpostServer.Size = new System.Drawing.Size(121, 20);
            this.txtEpostServer.TabIndex = 2;
            // 
            // lblEpostTilbyder
            // 
            this.lblEpostTilbyder.AutoSize = true;
            this.lblEpostTilbyder.Location = new System.Drawing.Point(31, 38);
            this.lblEpostTilbyder.Name = "lblEpostTilbyder";
            this.lblEpostTilbyder.Size = new System.Drawing.Size(73, 13);
            this.lblEpostTilbyder.TabIndex = 1;
            this.lblEpostTilbyder.Text = "Epost-tilbyder:";
            // 
            // cbxEpostTilbyder
            // 
            this.cbxEpostTilbyder.FormattingEnabled = true;
            this.cbxEpostTilbyder.Location = new System.Drawing.Point(120, 35);
            this.cbxEpostTilbyder.Name = "cbxEpostTilbyder";
            this.cbxEpostTilbyder.Size = new System.Drawing.Size(121, 21);
            this.cbxEpostTilbyder.TabIndex = 0;
            // 
            // btnLagre
            // 
            this.btnLagre.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnLagre.Location = new System.Drawing.Point(300, 407);
            this.btnLagre.Name = "btnLagre";
            this.btnLagre.Size = new System.Drawing.Size(75, 23);
            this.btnLagre.TabIndex = 1;
            this.btnLagre.Text = "Lagre";
            this.btnLagre.UseVisualStyleBackColor = true;
            this.btnLagre.Click += new System.EventHandler(this.btnLagre_Click);
            // 
            // V_Innstillinger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(379, 442);
            this.Controls.Add(this.btnLagre);
            this.Controls.Add(this.tcInnstillinger);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "V_Innstillinger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Innstillinger";
            this.Load += new System.EventHandler(this.V_Innstillinger_Load);
            this.tcInnstillinger.ResumeLayout(false);
            this.tpForetak.ResumeLayout(false);
            this.tpForetak.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuFakturagebyr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nuForfallsdager)).EndInit();
            this.tpFarger.ResumeLayout(false);
            this.tpFarger.PerformLayout();
            this.tpEpost.ResumeLayout(false);
            this.tpEpost.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcInnstillinger;
        private System.Windows.Forms.TabPage tpForetak;
        private System.Windows.Forms.TextBox txtAdresse2;
        private System.Windows.Forms.Label lblAdresse2;
        private System.Windows.Forms.TextBox txtAdresse;
        private System.Windows.Forms.Label lblAdresse;
        private System.Windows.Forms.TextBox txtForetak;
        private System.Windows.Forms.Label lblForetak;
        private System.Windows.Forms.TextBox txtPoststed;
        private System.Windows.Forms.Label lblPoststed;
        private System.Windows.Forms.TextBox txtPostnr;
        private System.Windows.Forms.Label lblPostnr;
        private System.Windows.Forms.TextBox txtKontonr;
        private System.Windows.Forms.Label lblKontonr;
        private System.Windows.Forms.TextBox txtOrgnr;
        private System.Windows.Forms.Label lblOrgnr;
        private System.Windows.Forms.TextBox txtEpost;
        private System.Windows.Forms.Label lblEpost;
        private System.Windows.Forms.Button btnLagre;
        private System.Windows.Forms.TabPage tpFarger;
        private System.Windows.Forms.ColorDialog fargeDialog;
        private System.Windows.Forms.Label lblFargeBehandlerBakgrunn;
        private System.Windows.Forms.Label _lblFargeBehandler;
        private System.Windows.Forms.Label lblFargeSendtBakgrunn;
        private System.Windows.Forms.Label _lblFargeSendt;
        private System.Windows.Forms.Label lblFargeKreditertBakgrunn;
        private System.Windows.Forms.Label _lblFargeKreditert;
        private System.Windows.Forms.CheckBox cbMvaPliktig;
        private System.Windows.Forms.Label lblForfallsdager;
        private System.Windows.Forms.Label lblFargeInnbetaltBakgrunn;
        private System.Windows.Forms.Label _lblFargeInnbetalt;
        private System.Windows.Forms.Label lblFargeInnbetaltTekst;
        private System.Windows.Forms.Label lblFargeKreditertTekst;
        private System.Windows.Forms.Label lblFargeSendtTekst;
        private System.Windows.Forms.Label lblFargeBehandlerTekst;
        private System.Windows.Forms.Label lblFargeInnbetaltKombinert;
        private System.Windows.Forms.Label lblFargeKreditertKombinert;
        private System.Windows.Forms.Label lblFargeSendtKombinert;
        private System.Windows.Forms.Label lblFargeBehandlerKombinert;
        private System.Windows.Forms.Label lblFargeKombinert;
        private System.Windows.Forms.Label lblFargeTekst;
        private System.Windows.Forms.Label lblFargeBakgrunn;
        private System.Windows.Forms.Label lblTema;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lblMerking;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tpEpost;
        private System.Windows.Forms.Label lblEpostTilbyder;
        private System.Windows.Forms.ComboBox cbxEpostTilbyder;
        private System.Windows.Forms.Label lblEpostBrukernavn;
        private System.Windows.Forms.TextBox txtEpostBrukernavn;
        private System.Windows.Forms.Label lblEpostPort;
        private System.Windows.Forms.TextBox txtEpostPort;
        private System.Windows.Forms.Label lblEpostServer;
        private System.Windows.Forms.TextBox txtEpostServer;
        private System.Windows.Forms.Label lblEpostPassord;
        private System.Windows.Forms.TextBox txtEpostPassord;
        private System.Windows.Forms.CheckBox cbEpostBrukSSL;
        private System.Windows.Forms.Label lblEpostAvsender;
        private System.Windows.Forms.TextBox txtEpostAvsender;
        private System.Windows.Forms.Label lblNettsted;
        private System.Windows.Forms.TextBox txtNettsted;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.Label lblFakturaGebyr;
        private System.Windows.Forms.Label lblKroner;
        private System.Windows.Forms.NumericUpDown nuFakturagebyr;
        private System.Windows.Forms.NumericUpDown nuForfallsdager;
    }
}