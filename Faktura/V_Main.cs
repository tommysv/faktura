﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

using Faktura.Presentation;

namespace Faktura
{
    public partial class V_Main : Form
    {
        private List<UserControl> views = new List<UserControl>();
        private UserControl activeView;

        public V_Main()
        {
            CultureInfo cultureInfo = new CultureInfo("nb-NO");
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            InitializeComponent();
            InitViews();
        }

        private void InitViews()
        {
            V_FakturaGrunnlagListe v_FakturaGrunnlagListe = new V_FakturaGrunnlagListe();
            v_FakturaGrunnlagListe.Name = "V_FakturaGrunnlagListe";
            v_FakturaGrunnlagListe.Location = new Point(0, this.tsMain.Height);
            v_FakturaGrunnlagListe.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - this.tsMain.Height - this.ssMain.Height);
            v_FakturaGrunnlagListe.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            v_FakturaGrunnlagListe.Visible = false;
            v_FakturaGrunnlagListe.ReportStatus += new ReportStatusEventHandler(v_ReportStatus);
            this.Controls.Add(v_FakturaGrunnlagListe);

            V_FakturaListe v_FakturaListe = new V_FakturaListe();
            v_FakturaListe.Name = "V_FakturaListe";
            v_FakturaListe.Location = new Point(0, this.tsMain.Height);
            v_FakturaListe.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - this.tsMain.Height - this.ssMain.Height);
            v_FakturaListe.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            v_FakturaListe.Visible = false;
            v_FakturaListe.ReportStatus += new ReportStatusEventHandler(v_ReportStatus);
            this.Controls.Add(v_FakturaListe);

            V_ProduktListe v_ProduktListe = new V_ProduktListe();
            v_ProduktListe.Name = "V_ProduktListe";
            v_ProduktListe.Location = new Point(0, this.tsMain.Height);
            v_ProduktListe.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - this.tsMain.Height - this.ssMain.Height);
            v_ProduktListe.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            v_ProduktListe.Visible = false;
            v_ProduktListe.ReportStatus += new ReportStatusEventHandler(v_ReportStatus);
            this.Controls.Add(v_ProduktListe);

            V_KundeListe v_KundeListe = new V_KundeListe();
            v_KundeListe.Name = "V_KundeListe";
            v_KundeListe.Location = new Point(0, this.tsMain.Height);
            v_KundeListe.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - this.tsMain.Height - this.ssMain.Height);
            v_KundeListe.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            v_KundeListe.Visible = false;
            v_KundeListe.ReportStatus += new ReportStatusEventHandler(v_ReportStatus);
            this.Controls.Add(v_KundeListe);

            V_Logg v_Logg = new V_Logg();
            v_Logg.Name = "V_Logg";
            v_Logg.Location = new Point(0, this.tsMain.Height);
            v_Logg.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - this.tsMain.Height - this.ssMain.Height);
            v_Logg.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            v_Logg.Visible = false;
            v_Logg.ReportStatus += new ReportStatusEventHandler(v_ReportStatus);
            this.Controls.Add(v_Logg);

            V_Rapporter v_Rapporter = new V_Rapporter();
            v_Rapporter.Name = "V_Rapporter";
            v_Rapporter.Location = new Point(0, this.tsMain.Height);
            v_Rapporter.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - this.tsMain.Height - this.ssMain.Height);
            v_Rapporter.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            v_Rapporter.Visible = false;
            v_Rapporter.ReportStatus += new ReportStatusEventHandler(v_ReportStatus);
            this.Controls.Add(v_Rapporter);

            views.Add(v_FakturaGrunnlagListe);
            views.Add(v_FakturaListe);
            views.Add(v_ProduktListe);
            views.Add(v_KundeListe);
            views.Add(v_Logg);
            views.Add(v_Rapporter);
        }

        private void ActivateView(string name)
        {
            for (int nView = 0; nView < views.Count; nView++)
            {
                if (views[nView].Name.Equals(name))
                {
                    if (!views[nView].Visible)
                    {
                        views[nView].Visible = true;
                        activeView = views[nView];
                        views[nView].BringToFront();
                        IV_ToolStrip iv_ToolStrip = views[nView] as IV_ToolStrip;
                        if (iv_ToolStrip != null)
                        {
                            ToolStripManager.RevertMerge(tsMain);
                            ToolStripManager.Merge(iv_ToolStrip.ToolStrip, tsMain);
                        }
                        else
                        {
                            ToolStripManager.RevertMerge(tsMain);
                        }
                    }
                }
                else
                {
                    if (views[nView].Visible)
                        views[nView].Visible = false;
                }
            }
        }

        private void tsButton_FakturagrunnlagListe_Click(object sender, EventArgs e)
        {
            ActivateView("V_FakturaGrunnlagListe");
            tsButton_FakturagrunnlagListe.Checked = true;
            tsButton_KundeListe.Checked = false;
            tsButton_ProduktListe.Checked = false;
            tsButton_Logg.Checked = false;
            tsButton_Rapporter.Checked = false;
            tsButton_FakturaListe.Checked = false;
        }

        private void tsButton_FakturaListe_Click(object sender, EventArgs e)
        {
            if (!tsButton_FakturaListe.Checked)
            {
                ActivateView("V_FakturaListe");
                tsButton_FakturagrunnlagListe.Checked = false;
                tsButton_KundeListe.Checked = false;
                tsButton_ProduktListe.Checked = false;
                tsButton_Logg.Checked = false;
                tsButton_Rapporter.Checked = false;
                tsButton_FakturaListe.Checked = true;
            }
        }

        private void tsButton_KundeListe_Click(object sender, EventArgs e)
        {
            if (!tsButton_KundeListe.Checked)
            {
                ActivateView("V_KundeListe");
                tsButton_FakturagrunnlagListe.Checked = false;
                tsButton_ProduktListe.Checked = false;
                tsButton_FakturaListe.Checked = false;
                tsButton_Logg.Checked = false;
                tsButton_Rapporter.Checked = false;
                tsButton_KundeListe.Checked = true;
            }
        }

        private void tsButton_ProduktListe_Click(object sender, EventArgs e)
        {
            if (!tsButton_ProduktListe.Checked)
            {
                ActivateView("V_ProduktListe");
                tsButton_FakturagrunnlagListe.Checked = false;
                tsButton_FakturaListe.Checked = false;
                tsButton_KundeListe.Checked = false;
                tsButton_Logg.Checked = false;
                tsButton_Rapporter.Checked = false;
                tsButton_ProduktListe.Checked = true;
            }
        }

        private void tsButton_Logg_Click(object sender, EventArgs e)
        {
            if (!tsButton_Logg.Checked)
            {
                ActivateView("V_Logg");
                tsButton_FakturagrunnlagListe.Checked = false;
                tsButton_FakturaListe.Checked = false;
                tsButton_KundeListe.Checked = false;
                tsButton_ProduktListe.Checked = false;
                tsButton_Rapporter.Checked = false;
                tsButton_Logg.Checked = true;
            }
        }

        private void tsButton_Rapporter_Click(object sender, EventArgs e)
        {
            if (!tsButton_Rapporter.Checked)
            {
                ActivateView("V_Rapporter");
                tsButton_FakturagrunnlagListe.Checked = false;
                tsButton_FakturaListe.Checked = false;
                tsButton_KundeListe.Checked = false;
                tsButton_Logg.Checked = false;
                tsButton_ProduktListe.Checked = false;
                tsButton_Rapporter.Checked = true;
            }
        }

        private void tsButton_Konfig_Click(object sender, EventArgs e)
        {
            V_Innstillinger v_Innstillinger = new V_Innstillinger();
            DialogResult r = v_Innstillinger.ShowDialog();
            if (r == DialogResult.OK)
            {
                if (activeView != null)
                {
                    IV_Oppdaterbar view = (IV_Oppdaterbar)activeView;
                    if (view != null)
                        view.Oppdater(false);
                }
            }
        }

        void v_ReportStatus(object sender, ReportStatusEventArgs e)
        {
            if (e.IsError)
                ssStatusLabel.ForeColor = Color.Red;
            else
                ssStatusLabel.ForeColor = Color.Black;
            ssStatusLabel.Text = e.Text;
        }

        private void V_Main_Load(object sender, EventArgs e)
        {
            ActivateView("V_FakturaGrunnlagListe");
        }

        private void tsButton_Utskrift_Click(object sender, EventArgs e)
        {
            if (activeView != null)
            {
                IPrintable iv_Printable = activeView as IPrintable;
                if (iv_Printable != null)
                    iv_Printable.Print();
            }
        }
    }
}
