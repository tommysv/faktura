﻿namespace Faktura
{
    partial class V_Bekreft
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnJa = new System.Windows.Forms.Button();
            this.btnNei = new System.Windows.Forms.Button();
            this.lblImgQuestion = new System.Windows.Forms.Label();
            this.lblQuestion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnJa
            // 
            this.btnJa.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnJa.Location = new System.Drawing.Point(61, 114);
            this.btnJa.Name = "btnJa";
            this.btnJa.Size = new System.Drawing.Size(54, 23);
            this.btnJa.TabIndex = 0;
            this.btnJa.Text = "Ja";
            this.btnJa.UseVisualStyleBackColor = true;
            // 
            // btnNei
            // 
            this.btnNei.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNei.Location = new System.Drawing.Point(121, 114);
            this.btnNei.Name = "btnNei";
            this.btnNei.Size = new System.Drawing.Size(54, 23);
            this.btnNei.TabIndex = 1;
            this.btnNei.Text = "Nei";
            this.btnNei.UseVisualStyleBackColor = true;
            // 
            // lblImgQuestion
            // 
            this.lblImgQuestion.Image = global::Faktura.Properties.Resources.question32x32;
            this.lblImgQuestion.Location = new System.Drawing.Point(21, 38);
            this.lblImgQuestion.Name = "lblImgQuestion";
            this.lblImgQuestion.Size = new System.Drawing.Size(32, 32);
            this.lblImgQuestion.TabIndex = 3;
            // 
            // lblQuestion
            // 
            this.lblQuestion.AutoSize = true;
            this.lblQuestion.Location = new System.Drawing.Point(69, 48);
            this.lblQuestion.Name = "lblQuestion";
            this.lblQuestion.Size = new System.Drawing.Size(72, 13);
            this.lblQuestion.TabIndex = 4;
            this.lblQuestion.Text = "Er du sikker ?";
            // 
            // V_Bekreft
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(187, 149);
            this.ControlBox = false;
            this.Controls.Add(this.lblQuestion);
            this.Controls.Add(this.lblImgQuestion);
            this.Controls.Add(this.btnNei);
            this.Controls.Add(this.btnJa);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "V_Bekreft";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bekreft";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnJa;
        private System.Windows.Forms.Button btnNei;
        private System.Windows.Forms.Label lblImgQuestion;
        private System.Windows.Forms.Label lblQuestion;
    }
}