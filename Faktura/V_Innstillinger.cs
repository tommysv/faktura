﻿using System;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;
using Faktura.Config;

namespace Faktura
{
    public partial class V_Innstillinger : Form
    {
        public V_Innstillinger()
        {
            InitializeComponent();
        }

        private void lblFargeBehandlerBakgrunn_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeBehandlerBakgrunn.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeBehandlerBakgrunn.BackColor = fargeDialog.Color;
                lblFargeBehandlerKombinert.BackColor = lblFargeBehandlerBakgrunn.BackColor;
                I_Konfig.UI.BehandlerBakgrunn = lblFargeBehandlerBakgrunn.BackColor;
            }
        }

        private void lblFargeBehandlerTekst_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeBehandlerTekst.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeBehandlerTekst.BackColor = fargeDialog.Color;
                lblFargeBehandlerKombinert.ForeColor = lblFargeBehandlerTekst.BackColor;
                I_Konfig.UI.BehandlerForgrunn = lblFargeBehandlerTekst.BackColor;
            }
        }

        private void lblFargeSendtBakgrunn_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeSendtBakgrunn.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeSendtBakgrunn.BackColor = fargeDialog.Color;
                lblFargeSendtKombinert.BackColor = lblFargeSendtBakgrunn.BackColor;
                I_Konfig.UI.SendtBakgrunn = lblFargeSendtBakgrunn.BackColor;
            }
        }

        private void lblFargeSendtTekst_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeSendtTekst.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeSendtTekst.BackColor = fargeDialog.Color;
                lblFargeSendtKombinert.ForeColor = lblFargeSendtTekst.BackColor;
                I_Konfig.UI.SendtForgrunn = lblFargeSendtTekst.BackColor;
            }
        }

        private void lblFargeKreditertBakgrunn_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeKreditertBakgrunn.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeKreditertBakgrunn.BackColor = fargeDialog.Color;
                lblFargeKreditertKombinert.BackColor = lblFargeKreditertBakgrunn.BackColor;
                I_Konfig.UI.KreditertBakgrunn = lblFargeKreditertBakgrunn.BackColor;
            }
        }

        private void lblFargeKreditertTekst_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeKreditertTekst.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeKreditertTekst.BackColor = fargeDialog.Color;
                lblFargeKreditertKombinert.ForeColor = lblFargeKreditertTekst.BackColor;
                I_Konfig.UI.KreditertForgrunn = lblFargeKreditertTekst.BackColor;
            }
        }

        private void lblFargeInnbetaltBakgrunn_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeInnbetaltBakgrunn.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeInnbetaltBakgrunn.BackColor = fargeDialog.Color;
                lblFargeInnbetaltKombinert.BackColor = lblFargeInnbetaltBakgrunn.BackColor;
                I_Konfig.UI.InnbetaltBakgrunn = lblFargeInnbetaltBakgrunn.BackColor;
            }
        }

        private void lblFargeInnbetaltTekst_Click(object sender, EventArgs e)
        {
            fargeDialog.Color = lblFargeInnbetaltTekst.BackColor;
            DialogResult r = fargeDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                lblFargeInnbetaltTekst.BackColor = fargeDialog.Color;
                lblFargeInnbetaltKombinert.ForeColor = lblFargeInnbetaltTekst.BackColor;
                I_Konfig.UI.InnbetaltForgrunn = lblFargeInnbetaltTekst.BackColor;
            }
        }

        private void V_Innstillinger_Load(object sender, EventArgs e)
        {
            txtForetak.Text = I_Konfig.Foretak.Navn;
            txtOrgnr.Text = I_Konfig.Foretak.OrgNr;
            cbMvaPliktig.Checked = I_Konfig.Foretak.MvaPliktig;
            txtAdresse.Text = I_Konfig.Foretak.Adresse1;
            txtAdresse2.Text = I_Konfig.Foretak.Adresse2;
            txtPostnr.Text = I_Konfig.Foretak.PostNr;
            txtPoststed.Text = I_Konfig.Foretak.Poststed;
            txtEpost.Text = I_Konfig.Foretak.Epost;
            txtTelefon.Text = I_Konfig.Foretak.Telefon;
            txtNettsted.Text = I_Konfig.Foretak.Nettsted;
            txtKontonr.Text = I_Konfig.Foretak.Nettsted;
            nuForfallsdager.Value = I_Konfig.Foretak.Forfallsdager;
            nuFakturagebyr.Value = Convert.ToDecimal(I_Konfig.Foretak.Fakturagebyr);

            lblFargeBehandlerBakgrunn.BackColor = I_Konfig.UI.BehandlerBakgrunn;
            lblFargeBehandlerTekst.BackColor = I_Konfig.UI.BehandlerForgrunn;
            lblFargeBehandlerKombinert.BackColor = lblFargeBehandlerBakgrunn.BackColor;
            lblFargeBehandlerKombinert.ForeColor = lblFargeBehandlerTekst.BackColor;

            lblFargeSendtBakgrunn.BackColor = I_Konfig.UI.SendtBakgrunn;
            lblFargeSendtTekst.BackColor = I_Konfig.UI.SendtForgrunn;
            lblFargeSendtKombinert.BackColor = lblFargeSendtBakgrunn.BackColor;
            lblFargeSendtKombinert.ForeColor = lblFargeSendtTekst.BackColor;

            lblFargeKreditertBakgrunn.BackColor = I_Konfig.UI.KreditertBakgrunn;
            lblFargeKreditertTekst.BackColor = I_Konfig.UI.KreditertForgrunn;
            lblFargeKreditertKombinert.BackColor = lblFargeKreditertBakgrunn.BackColor;
            lblFargeKreditertKombinert.ForeColor = lblFargeKreditertTekst.BackColor;

            lblFargeInnbetaltBakgrunn.BackColor = I_Konfig.UI.InnbetaltBakgrunn;
            lblFargeInnbetaltTekst.BackColor = I_Konfig.UI.InnbetaltForgrunn;
            lblFargeInnbetaltKombinert.BackColor = lblFargeInnbetaltBakgrunn.BackColor;
            lblFargeInnbetaltKombinert.ForeColor = lblFargeInnbetaltTekst.BackColor;
        }

        private void cbMvaPliktig_CheckedChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.MvaPliktig = cbMvaPliktig.Checked;
        }

        private void btnLagre_Click(object sender, EventArgs e)
        {
            I_Konfig.Lagre();
        }

        private void txtOrgnr_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.OrgNr = txtOrgnr.Text;
        }

        private void txtForetak_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Navn = txtForetak.Text;
        }

        private void txtAdresse_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Adresse1 = txtAdresse.Text;
        }

        private void txtAdresse2_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Adresse2 = txtAdresse2.Text;
        }

        private void txtPostnr_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.PostNr = txtPostnr.Text;
        }

        private void txtPoststed_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Poststed = txtPoststed.Text;
        }

        private void txtEpost_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Epost = txtEpost.Text;
        }

        private void txtTelefon_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Telefon = txtTelefon.Text;
        }

        private void txtNettsted_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Nettsted = txtNettsted.Text;
        }

        private void txtKontonr_TextChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.KontoNr = txtKontonr.Text;
        }

        private void nuForfallsdager_ValueChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Forfallsdager = Convert.ToInt32(nuForfallsdager.Value);
        }

        private void nuFakturagebyr_ValueChanged(object sender, EventArgs e)
        {
            I_Konfig.Foretak.Fakturagebyr = Convert.ToDouble(nuFakturagebyr.Value);
        }
    }
}
