﻿namespace Faktura
{
    partial class V_FakturaListe
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(V_FakturaListe));
            this.tsFakturaListe = new System.Windows.Forms.ToolStrip();
            this.tsBtnKrediterFaktura = new System.Windows.Forms.ToolStripButton();
            this.tsBtnRegistrerInnbetaling = new System.Windows.Forms.ToolStripButton();
            this.tsBtnFilter = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSendFaktura = new System.Windows.Forms.ToolStripButton();
            this.tsBtnVisFaktura = new System.Windows.Forms.ToolStripButton();
            this.tsFilter = new System.Windows.Forms.ToolStrip();
            this.slettFakturaBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.hentFakturaListeBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.sendFakturaBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.sendPrisoverslagBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.lwFakturaListe = new Faktura.ListViewEx();
            this.colFakturaNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMottaker = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colFakturaDato = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBeløp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMva = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colKreditert = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colKredittnota = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tsFakturaListe.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsFakturaListe
            // 
            this.tsFakturaListe.BackColor = System.Drawing.Color.White;
            this.tsFakturaListe.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnKrediterFaktura,
            this.tsBtnRegistrerInnbetaling,
            this.tsBtnFilter,
            this.tsBtnSendFaktura,
            this.tsBtnVisFaktura});
            this.tsFakturaListe.Location = new System.Drawing.Point(0, 0);
            this.tsFakturaListe.Name = "tsFakturaListe";
            this.tsFakturaListe.Size = new System.Drawing.Size(629, 37);
            this.tsFakturaListe.TabIndex = 1;
            this.tsFakturaListe.Visible = false;
            // 
            // tsBtnKrediterFaktura
            // 
            this.tsBtnKrediterFaktura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnKrediterFaktura.Enabled = false;
            this.tsBtnKrediterFaktura.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnKrediterFaktura.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnKrediterFaktura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnKrediterFaktura.Name = "tsBtnKrediterFaktura";
            this.tsBtnKrediterFaktura.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnKrediterFaktura.Size = new System.Drawing.Size(34, 34);
            this.tsBtnKrediterFaktura.Text = "Krediter faktura";
            this.tsBtnKrediterFaktura.ToolTipText = "Krediter faktura";
            this.tsBtnKrediterFaktura.Click += new System.EventHandler(this.tsBtnKrediterFaktura_Click);
            // 
            // tsBtnRegistrerInnbetaling
            // 
            this.tsBtnRegistrerInnbetaling.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnRegistrerInnbetaling.Enabled = false;
            this.tsBtnRegistrerInnbetaling.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnRegistrerInnbetaling.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnRegistrerInnbetaling.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnRegistrerInnbetaling.Name = "tsBtnRegistrerInnbetaling";
            this.tsBtnRegistrerInnbetaling.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnRegistrerInnbetaling.Size = new System.Drawing.Size(34, 34);
            this.tsBtnRegistrerInnbetaling.Text = "Registrer innbetaling";
            this.tsBtnRegistrerInnbetaling.ToolTipText = "Registrer innbetaling";
            this.tsBtnRegistrerInnbetaling.Click += new System.EventHandler(this.tsBtnRegistrerInnbetaling_Click);
            // 
            // tsBtnFilter
            // 
            this.tsBtnFilter.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsBtnFilter.CheckOnClick = true;
            this.tsBtnFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnFilter.Image = global::Faktura.Properties.Resources.filter;
            this.tsBtnFilter.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnFilter.Name = "tsBtnFilter";
            this.tsBtnFilter.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnFilter.Size = new System.Drawing.Size(34, 34);
            this.tsBtnFilter.CheckedChanged += new System.EventHandler(this.tsBtnFilter_CheckedChanged);
            // 
            // tsBtnSendFaktura
            // 
            this.tsBtnSendFaktura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSendFaktura.Enabled = false;
            this.tsBtnSendFaktura.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnSendFaktura.Image")));
            this.tsBtnSendFaktura.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnSendFaktura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSendFaktura.Name = "tsBtnSendFaktura";
            this.tsBtnSendFaktura.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnSendFaktura.Size = new System.Drawing.Size(34, 34);
            this.tsBtnSendFaktura.Text = "Send faktura";
            this.tsBtnSendFaktura.Click += new System.EventHandler(this.tsBtnSendFaktura_Click);
            // 
            // tsBtnVisFaktura
            // 
            this.tsBtnVisFaktura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnVisFaktura.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnVisFaktura.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnVisFaktura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnVisFaktura.Name = "tsBtnVisFaktura";
            this.tsBtnVisFaktura.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnVisFaktura.Size = new System.Drawing.Size(34, 34);
            this.tsBtnVisFaktura.Text = "Vis faktura";
            this.tsBtnVisFaktura.Click += new System.EventHandler(this.tsBtnVisFaktura_Click);
            // 
            // tsFilter
            // 
            this.tsFilter.BackColor = System.Drawing.Color.GhostWhite;
            this.tsFilter.Location = new System.Drawing.Point(0, 0);
            this.tsFilter.Name = "tsFilter";
            this.tsFilter.Size = new System.Drawing.Size(629, 25);
            this.tsFilter.TabIndex = 2;
            this.tsFilter.Visible = false;
            // 
            // slettFakturaBackgroundTask
            // 
            this.slettFakturaBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.slettFakturaBackgroundTask_DoWork);
            this.slettFakturaBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.slettFakturaBackgroundTask_RunWorkerCompleted);
            // 
            // hentFakturaListeBackgroundTask
            // 
            this.hentFakturaListeBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.hentFakturaListeBackgroundTask_DoWork);
            this.hentFakturaListeBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.hentFakturaListeBackgroundTask_RunWorkerCompleted);
            // 
            // sendFakturaBackgroundTask
            // 
            this.sendFakturaBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.sendFakturaBackgroundTask_DoWork);
            this.sendFakturaBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.sendFakturaBackgroundTask_RunWorkerCompleted);
            // 
            // sendPrisoverslagBackgroundTask
            // 
            this.sendPrisoverslagBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.sendPrisoverslagBackgroundTask_DoWork);
            this.sendPrisoverslagBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.sendPrisoverslagBackgroundTask_RunWorkerCompleted);
            // 
            // lwFakturaListe
            // 
            this.lwFakturaListe.BackColor = System.Drawing.Color.White;
            this.lwFakturaListe.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lwFakturaListe.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFakturaNr,
            this.colMottaker,
            this.colFakturaDato,
            this.colBeløp,
            this.colMva,
            this.colSum,
            this.colStatus,
            this.colKreditert,
            this.colKredittnota});
            this.lwFakturaListe.ColumnWeights = null;
            this.lwFakturaListe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwFakturaListe.FullRowSelect = true;
            this.lwFakturaListe.Location = new System.Drawing.Point(0, 37);
            this.lwFakturaListe.MultiSelect = false;
            this.lwFakturaListe.Name = "lwFakturaListe";
            this.lwFakturaListe.OwnerDraw = true;
            this.lwFakturaListe.Size = new System.Drawing.Size(629, 385);
            this.lwFakturaListe.TabIndex = 0;
            this.lwFakturaListe.UseCompatibleStateImageBehavior = false;
            this.lwFakturaListe.View = System.Windows.Forms.View.Details;
            this.lwFakturaListe.SelectedIndexChanged += new System.EventHandler(this.lwFakturaListe_SelectedIndexChanged);
            // 
            // colFakturaNr
            // 
            this.colFakturaNr.Text = "Faktura-nr";
            this.colFakturaNr.Width = 80;
            // 
            // colMottaker
            // 
            this.colMottaker.Text = "Mottaker";
            this.colMottaker.Width = 100;
            // 
            // colFakturaDato
            // 
            this.colFakturaDato.Text = "Faktura-dato";
            this.colFakturaDato.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // colBeløp
            // 
            this.colBeløp.Text = "Beløp";
            this.colBeløp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // colMva
            // 
            this.colMva.Text = "Mva";
            this.colMva.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // colSum
            // 
            this.colSum.Text = "Sum";
            this.colSum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // colKreditert
            // 
            this.colKreditert.Text = "Kreditert";
            this.colKreditert.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colKreditert.Width = 93;
            // 
            // colKredittnota
            // 
            this.colKredittnota.Text = "Kredittnota";
            this.colKredittnota.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // V_FakturaListe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.lwFakturaListe);
            this.Controls.Add(this.tsFilter);
            this.Controls.Add(this.tsFakturaListe);
            this.Name = "V_FakturaListe";
            this.Size = new System.Drawing.Size(629, 422);
            this.Load += new System.EventHandler(this.V_FakturaListe_Load);
            this.VisibleChanged += new System.EventHandler(this.V_FakturaListe_VisibleChanged);
            this.tsFakturaListe.ResumeLayout(false);
            this.tsFakturaListe.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Faktura.ListViewEx lwFakturaListe;
        private System.Windows.Forms.ColumnHeader colFakturaNr;
        private System.Windows.Forms.ColumnHeader colFakturaDato;
        private System.Windows.Forms.ColumnHeader colBeløp;
        private System.Windows.Forms.ColumnHeader colMottaker;
        private System.Windows.Forms.ColumnHeader colKreditert;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.ToolStrip tsFakturaListe;
        private System.Windows.Forms.ToolStripButton tsBtnKrediterFaktura;
        private System.Windows.Forms.ToolStripButton tsBtnRegistrerInnbetaling;
        private System.Windows.Forms.ToolStripButton tsBtnSendFaktura;
        private System.Windows.Forms.ColumnHeader colMva;
        private System.Windows.Forms.ColumnHeader colSum;
        private System.Windows.Forms.ToolStrip tsFilter;
        private System.Windows.Forms.ToolStripButton tsBtnFilter;
        private System.ComponentModel.BackgroundWorker slettFakturaBackgroundTask;
        private System.Windows.Forms.ColumnHeader colKredittnota;
        private System.ComponentModel.BackgroundWorker hentFakturaListeBackgroundTask;
        private System.ComponentModel.BackgroundWorker sendFakturaBackgroundTask;
        private System.ComponentModel.BackgroundWorker sendPrisoverslagBackgroundTask;
        private System.Windows.Forms.ToolStripButton tsBtnVisFaktura;

    }
}
