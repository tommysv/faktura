﻿namespace Faktura
{
    partial class V_ProduktListe
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tsProduktListe = new System.Windows.Forms.ToolStrip();
            this.tsBtnNyttProdukt = new System.Windows.Forms.ToolStripButton();
            this.tsBtnRedigerProdukt = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSlettProdukt = new System.Windows.Forms.ToolStripButton();
            this.slettProduktTask = new System.ComponentModel.BackgroundWorker();
            this.hentProduktListeTask = new System.ComponentModel.BackgroundWorker();
            this.lwProduktListe = new Faktura.ListViewEx();
            this.colProduktkode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProduktBeskrivelse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEnhet = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEnhetspris = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMvaSats = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tsProduktListe.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsProduktListe
            // 
            this.tsProduktListe.BackColor = System.Drawing.Color.White;
            this.tsProduktListe.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnNyttProdukt,
            this.tsBtnRedigerProdukt,
            this.tsBtnSlettProdukt});
            this.tsProduktListe.Location = new System.Drawing.Point(0, 0);
            this.tsProduktListe.Name = "tsProduktListe";
            this.tsProduktListe.Size = new System.Drawing.Size(497, 37);
            this.tsProduktListe.TabIndex = 2;
            this.tsProduktListe.Visible = false;
            // 
            // tsBtnNyttProdukt
            // 
            this.tsBtnNyttProdukt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnNyttProdukt.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnNyttProdukt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnNyttProdukt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnNyttProdukt.Name = "tsBtnNyttProdukt";
            this.tsBtnNyttProdukt.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnNyttProdukt.Size = new System.Drawing.Size(34, 34);
            this.tsBtnNyttProdukt.Click += new System.EventHandler(this.tsBtnNyttProdukt_Click);
            // 
            // tsBtnRedigerProdukt
            // 
            this.tsBtnRedigerProdukt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnRedigerProdukt.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnRedigerProdukt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnRedigerProdukt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnRedigerProdukt.Name = "tsBtnRedigerProdukt";
            this.tsBtnRedigerProdukt.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnRedigerProdukt.Size = new System.Drawing.Size(34, 34);
            this.tsBtnRedigerProdukt.Visible = false;
            this.tsBtnRedigerProdukt.Click += new System.EventHandler(this.tsBtnRedigerProdukt_Click);
            // 
            // tsBtnSlettProdukt
            // 
            this.tsBtnSlettProdukt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSlettProdukt.Image = global::Faktura.Properties.Resources.placeholder;
            this.tsBtnSlettProdukt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnSlettProdukt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSlettProdukt.Name = "tsBtnSlettProdukt";
            this.tsBtnSlettProdukt.Padding = new System.Windows.Forms.Padding(3);
            this.tsBtnSlettProdukt.Size = new System.Drawing.Size(34, 34);
            this.tsBtnSlettProdukt.Visible = false;
            this.tsBtnSlettProdukt.Click += new System.EventHandler(this.tsBtnSlettProdukt_Click);
            // 
            // slettProduktTask
            // 
            this.slettProduktTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.slettProduktTask_DoWork);
            this.slettProduktTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.slettProduktTask_RunWorkerCompleted);
            // 
            // hentProduktListeTask
            // 
            this.hentProduktListeTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.hentProduktListeTask_DoWork);
            this.hentProduktListeTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.hentProduktListeTask_RunWorkerCompleted);
            // 
            // lwProduktListe
            // 
            this.lwProduktListe.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lwProduktListe.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colProduktkode,
            this.colProduktBeskrivelse,
            this.colEnhet,
            this.colEnhetspris,
            this.colMvaSats});
            this.lwProduktListe.ColumnWeights = null;
            this.lwProduktListe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwProduktListe.FullRowSelect = true;
            this.lwProduktListe.Location = new System.Drawing.Point(0, 0);
            this.lwProduktListe.MultiSelect = false;
            this.lwProduktListe.Name = "lwProduktListe";
            this.lwProduktListe.OwnerDraw = true;
            this.lwProduktListe.Size = new System.Drawing.Size(651, 458);
            this.lwProduktListe.TabIndex = 1;
            this.lwProduktListe.UseCompatibleStateImageBehavior = false;
            this.lwProduktListe.View = System.Windows.Forms.View.Details;
            this.lwProduktListe.SelectedIndexChanged += new System.EventHandler(this.lwProduktListe_SelectedIndexChanged);
            this.lwProduktListe.VisibleChanged += new System.EventHandler(this.lwProduktListe_VisibleChanged);
            // 
            // colProduktkode
            // 
            this.colProduktkode.Text = "Produktkode";
            this.colProduktkode.Width = 80;
            // 
            // colProduktBeskrivelse
            // 
            this.colProduktBeskrivelse.Text = "Beskrivelse";
            this.colProduktBeskrivelse.Width = 200;
            // 
            // colEnhet
            // 
            this.colEnhet.Text = "Enhet";
            // 
            // colEnhetspris
            // 
            this.colEnhetspris.Text = "Enhetspris";
            this.colEnhetspris.Width = 80;
            // 
            // colMvaSats
            // 
            this.colMvaSats.Text = "Mva-%";
            this.colMvaSats.Width = 77;
            // 
            // V_ProduktListe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.lwProduktListe);
            this.Controls.Add(this.tsProduktListe);
            this.DoubleBuffered = true;
            this.Name = "V_ProduktListe";
            this.Size = new System.Drawing.Size(651, 458);
            this.Load += new System.EventHandler(this.UscProduktListe_Load);
            this.tsProduktListe.ResumeLayout(false);
            this.tsProduktListe.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Faktura.ListViewEx lwProduktListe;
        private System.Windows.Forms.ColumnHeader colProduktkode;
        private System.Windows.Forms.ColumnHeader colProduktBeskrivelse;
        private System.Windows.Forms.ColumnHeader colEnhetspris;
        private System.Windows.Forms.ColumnHeader colEnhet;
        private System.Windows.Forms.ToolStrip tsProduktListe;
        private System.Windows.Forms.ToolStripButton tsBtnNyttProdukt;
        private System.Windows.Forms.ToolStripButton tsBtnRedigerProdukt;
        private System.Windows.Forms.ToolStripButton tsBtnSlettProdukt;
        private System.Windows.Forms.ColumnHeader colMvaSats;
        private System.ComponentModel.BackgroundWorker slettProduktTask;
        private System.ComponentModel.BackgroundWorker hentProduktListeTask;
    }
}
