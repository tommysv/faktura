﻿using System;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;

namespace Faktura
{
    public partial class V_Produkt : Form, IV_Produkt
    {
        private P_Produkt p_Produkt;
        private Produkt produkt;

        public V_Produkt()
        {
            InitializeComponent();
            this.p_Produkt = new P_Produkt(this);
            this.p_Produkt.Init();
        }

        public Produkt Produkt
        {
            get
            {
                return produkt;
            }
            set
            {
                produkt = value;
            }
        }

        private void V_Produkt_Load(object sender, EventArgs e)
        {
            if (produkt.Id != -1)
            {
                this.Text = "Rediger produkt";
                txtProduktId.Text = Convert.ToString(produkt.Id);
            }
            else
            {
                this.Text = "Nytt produkt";
                txtProduktId.Text = "Automatisk";
            }
            txtProduktkode.Text = produkt.ProduktKode;
            txtBeskrivelse.Text = produkt.Beskrivelse;
            txtEnhet.Text = produkt.Enhet;
            txtEnhetspris.Text = Convert.ToString(produkt.EnhetsPris);
            txtMvaSats.Text = Convert.ToString(produkt.MvaSats);
        }

        private void btnLagre_Click(object sender, EventArgs e)
        {
            produkt.ProduktKode = txtProduktkode.Text;
            produkt.Beskrivelse = txtBeskrivelse.Text;
            produkt.Enhet = txtEnhet.Text;
            produkt.EnhetsPris = Convert.ToDouble(txtEnhetspris.Text);
            produkt.MvaSats = Convert.ToDouble(txtMvaSats.Text);
            if (!lagreBackgroundTask.IsBusy)
            {
                lagreBackgroundTask.RunWorkerAsync();
            }
        }

        private void lagreBackgroundTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_Produkt.Lagre();
        }

        private void lagreBackgroundTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}
