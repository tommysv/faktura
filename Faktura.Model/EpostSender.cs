﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace Faktura.Model
{
    internal class EpostSender
    {
        private string emne;
        private string melding;
        private List<string> vedlegg;
        private string mottaker;

        internal EpostSender()
        {
            this.emne = String.Empty;
            this.melding = String.Empty;
            this.vedlegg = new List<string>();
            this.mottaker = String.Empty;
        }

        public string Emne
        {
            get { return emne; }
            set { emne = value; }
        }

        public string Melding
        {
            get { return melding; }
            set { melding = value; }
        }

        public List<string> Vedlegg
        {
            get { return vedlegg; }
            set { vedlegg = value; }
        }

        public string Mottaker
        {
            get { return mottaker; }
            set { mottaker = value; }
        }

        internal void Send()
        {
            string server = ""; //Faktura.Konfig.Default.Epost.Server;
            int port = 25;// Faktura.Konfig.Default.Epost.Port;
            bool brukSSL = false;// Faktura.Konfig.Default.Epost.BrukSSL;
            string fraEpost = ""; //Faktura.Konfig.Default.Epost.Avsender;
            string tilEpost = mottaker;
            string passord = "";//Faktura.Konfig.Default.Epost.Passord;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = server;
            smtpClient.Port = port;
            smtpClient.EnableSsl = brukSSL;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            if (brukSSL)
            {
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(fraEpost, passord);
            }
            else
            {
                smtpClient.UseDefaultCredentials = true;
            }
            using (MailMessage message = new MailMessage(fraEpost, tilEpost))
            {
                message.Body = melding;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = false;
                message.Priority = MailPriority.High;
                message.Subject = emne;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                for (int nVedlegg = 0; nVedlegg < vedlegg.Count; nVedlegg++)
                    message.Attachments.Add(new Attachment(vedlegg[nVedlegg]));
                smtpClient.Send(message);
            }
        }
    }
}
