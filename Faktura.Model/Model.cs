﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.IO;

using Faktura.Data;
using Faktura.Generators;

namespace Faktura.Model
{
    public class Model : IModel
    {
        public Common.Produkt HentProdukt(int produktId)
        {
            DbCmd_HentProdukt cmd_HentProdukt = new DbCmd_HentProdukt(produktId);
            cmd_HentProdukt.Execute();
            Common.Produkt produkt = (Common.Produkt)cmd_HentProdukt.Result;
            return produkt;
        }

        public void LagreProdukt(Common.Produkt produkt)
        {
            DbCmd_LagreProdukt cmd_LagreProdukt = new DbCmd_LagreProdukt(produkt);
            cmd_LagreProdukt.Execute();
        }

        public IList<Common.FakturaPreview> HentFakturaListe(Common.FakturaFilter fakturaFilter)
        {
            DbCmd_HentFakturaListe cmd_HentFakturaListe = new DbCmd_HentFakturaListe(fakturaFilter);
            cmd_HentFakturaListe.Execute();
            List<Common.FakturaPreview> fakturaListe = (List<Common.FakturaPreview>)cmd_HentFakturaListe.Result;
            return fakturaListe;
        }

        public IList<Common.Kunde> HentKundeListe()
        {
            DbCmd_HentKundeListe cmd_HentKundeListe = new DbCmd_HentKundeListe();
            cmd_HentKundeListe.Execute();
            List<Common.Kunde> kundeListe = (List<Common.Kunde>)cmd_HentKundeListe.Result;
            return kundeListe;
        }

        public IList<Common.Produkt> HentProduktListe()
        {
            DbCmd_HentProduktListe cmd_HentProduktListe = new DbCmd_HentProduktListe();
            cmd_HentProduktListe.Execute();
            List<Common.Produkt> produktListe = (List<Common.Produkt>)cmd_HentProduktListe.Result;
            return produktListe;
        }

        private Common.Kunde HentKunde(int kundeId)
        {
            DbCmd_HentKunde cmd_HentKunde = new DbCmd_HentKunde(kundeId);
            cmd_HentKunde.Execute();
            Common.Kunde kunde = (Common.Kunde)cmd_HentKunde.Result;
            return kunde;
        }

        public void LagreKunde(Common.Kunde kunde)
        {
            DbCmd_LagreKunde cmd_LagreKunde = new DbCmd_LagreKunde(kunde);
            cmd_LagreKunde.Execute();
        }

        public Common.Faktura HentFaktura(int fakturaId, bool bHentFakturaLinjer = true)
        {
            DbCmd_HentFaktura cmd_HentFaktura = new DbCmd_HentFaktura(fakturaId, bHentFakturaLinjer);
            cmd_HentFaktura.Execute();
            Common.Faktura faktura = (Common.Faktura)cmd_HentFaktura.Result;
            return faktura;
        }

        public void SlettFaktura(int fakturaId)
        {
            DbCmd_HentFaktura cmd_HentFaktura = new DbCmd_HentFaktura(fakturaId, false);
            cmd_HentFaktura.Execute();
            Common.Faktura faktura = (Common.Faktura)cmd_HentFaktura.Result;
            if (faktura.Status == Common.FakturaStatus.KlarForSending)
            {
                DbCmd_SlettFaktura cmd_SlettFaktura = new DbCmd_SlettFaktura(fakturaId);
                cmd_SlettFaktura.Execute();
            }
        }

        public void LagreFaktura(Common.Faktura faktura, bool bLagreFakturaLinjer = true)
        {
            using (DbTransact trans = new DbTransact())
            {
                DbCmd_LagreFaktura cmd_LagreFaktura = new DbCmd_LagreFaktura(faktura, bLagreFakturaLinjer);
                trans.ExecuteCommand(cmd_LagreFaktura);
                trans.Commit();
            }
        }

        public void SlettProdukt(int produktId)
        {
            DbCmd_SlettProdukt cmd_SlettProdukt = new DbCmd_SlettProdukt(produktId);
            cmd_SlettProdukt.Execute();
        }

        public void SendFaktura(int fakturaId)
        {
            DbCmd_HentFaktura cmd_HentFaktura = new DbCmd_HentFaktura(fakturaId);
            cmd_HentFaktura.Execute();
            Common.Faktura faktura = (Common.Faktura)cmd_HentFaktura.Result;
            if (faktura.Status == Common.FakturaStatus.KlarForSending)
            {
                using (DbTransact trans = new DbTransact())
                {
                    DateTime dateTime = DateTime.Now;
                    DbCmd_GenererFakturaNr cmd_GenererFakturaNr = new DbCmd_GenererFakturaNr(dateTime.Year);
                    trans.ExecuteCommand(cmd_GenererFakturaNr);
                    faktura.Nr = Convert.ToInt32(cmd_GenererFakturaNr.Result);
                    faktura.Dato = dateTime;
                    faktura.ForfallsDato = dateTime.AddDays(14);
                    faktura.Status = Common.FakturaStatus.Sendt;

                    DbCmd_LagreFaktura cmd_LagreFaktura = new DbCmd_LagreFaktura(faktura, false);
                    trans.ExecuteCommand(cmd_LagreFaktura);

                    G_Faktura g_Faktura = new G_Faktura(faktura);
                    g_Faktura.Generer();

                    EpostSender epostSender = new EpostSender();
                    epostSender.Emne = "Send faktura";
                    epostSender.Melding = "Send faktura melding";
                    epostSender.Mottaker = faktura.Kunde.FakturaEpost;
                    epostSender.Vedlegg.Add(g_Faktura.Filnavn);
                    epostSender.Send();

                    Common.LoggElement loggElement = new Common.LoggElement();
                    loggElement.Beskrivelse = String.Format("Sendt faktura nr. {0} til {1}", faktura.Nr, faktura.Kunde.Navn);
                    DbCmd_NyttLoggElement cmd_NyttLoggElement = new DbCmd_NyttLoggElement(loggElement);
                    trans.ExecuteCommand(cmd_NyttLoggElement);

                    trans.Commit();
                }
            }
            else
            {
                using (DbTransact trans = new DbTransact())
                {
                    G_Faktura g_Faktura = new G_Faktura(faktura);
                    g_Faktura.Generer();

                    EpostSender epostSender = new EpostSender();
                    epostSender.Emne = "Send faktura på nytt";
                    epostSender.Melding = "Send faktura på nytt melding";
                    epostSender.Mottaker = faktura.Kunde.FakturaEpost;
                    epostSender.Vedlegg.Add(g_Faktura.Filnavn);
                    epostSender.Send();

                    Common.LoggElement loggElement = new Common.LoggElement();
                    loggElement.Beskrivelse = String.Format("Sendt faktura nr. {0} på nytt til {1}", faktura.Nr, faktura.Kunde.Navn);
                    DbCmd_NyttLoggElement cmd_NyttLoggElement = new DbCmd_NyttLoggElement(loggElement);
                    trans.ExecuteCommand(cmd_NyttLoggElement);

                    trans.Commit();
                }
            }
        }

        public void KrediterFaktura(int fakturaId)
        {
            DbCmd_HentFaktura cmd_HentFaktura = new DbCmd_HentFaktura(fakturaId);
            cmd_HentFaktura.Execute();
            Common.Faktura faktura = (Common.Faktura)cmd_HentFaktura.Result;
            if (faktura.Status != Common.FakturaStatus.KlarForSending)
            {
                using (DbTransact trans = new DbTransact())
                {
                    Common.LoggElement loggElement = new Common.LoggElement();
                    loggElement.Beskrivelse = String.Format("Kreditert faktura nr. {0} til {1}", faktura.Nr, faktura.Kunde.Navn);
                    DbCmd_NyttLoggElement cmd_NyttLoggElement = new DbCmd_NyttLoggElement(loggElement);
                    trans.ExecuteCommand(cmd_NyttLoggElement);

                    DbCmd_KrediterFaktura cmd_KrediterFaktura = new DbCmd_KrediterFaktura(faktura);
                    trans.ExecuteCommand(cmd_KrediterFaktura);

                    trans.Commit();
                }
            }
        }

        public void RegistrerInnbetaling(int fakturaId)
        {
            DbCmd_HentFaktura cmd_HentFaktura = new DbCmd_HentFaktura(fakturaId, false);
            cmd_HentFaktura.Execute();
            Common.Faktura faktura = (Common.Faktura)cmd_HentFaktura.Result;
            if (faktura.Status == Common.FakturaStatus.Sendt)
            {
                using (DbTransact trans = new DbTransact())
                {
                    faktura.Status = Common.FakturaStatus.Innbetalt;
                    faktura.InnbetaltDato = DateTime.Now;
                    DbCmd_LagreFaktura cmd_LagreFaktura = new DbCmd_LagreFaktura(faktura, false);
                    trans.ExecuteCommand(cmd_LagreFaktura);

                    Common.LoggElement loggElement = new Common.LoggElement();
                    loggElement.Beskrivelse = String.Format("Registrert innbetaling på faktura nr. {0} til {1}", faktura.Nr, faktura.Kunde.Navn);
                    DbCmd_NyttLoggElement cmd_NyttLoggElement = new DbCmd_NyttLoggElement(loggElement);
                    trans.ExecuteCommand(cmd_NyttLoggElement);

                    trans.Commit();
                }
            }
        }

        public int[] HentFakturaAar()
        {
            DbCmd_HentFakturaAar cmd_HentFakturaAar = new DbCmd_HentFakturaAar();
            cmd_HentFakturaAar.Execute();
            return (int[])cmd_HentFakturaAar.Result;
        }


        public IList<Common.KundeNavn> HentKundeNavn()
        {
            DbCmd_HentKundeNavn cmd_HentKundeNavn = new DbCmd_HentKundeNavn();
            cmd_HentKundeNavn.Execute();
            return (List<Common.KundeNavn>)cmd_HentKundeNavn.Result;
        }

        public string GenererFaktura(int fakturaId)
        {
            DbCmd_HentFaktura cmd_HentFaktura = new DbCmd_HentFaktura(fakturaId);
            cmd_HentFaktura.Execute();
            Common.Faktura faktura = (Common.Faktura)cmd_HentFaktura.Result;
            G_KredittNota g_KredittNota = new G_KredittNota(faktura);
            g_KredittNota.Generer();
           // G_Faktura g_Faktura = new G_Faktura(faktura);
           // g_Faktura.Generer();
            return g_KredittNota.Filnavn;
        }


        public IList<Common.LoggElement> HentLogg()
        {
            DbCmd_HentLogg cmd_HentLogg = new DbCmd_HentLogg();
            cmd_HentLogg.Execute();
            return (List<Common.LoggElement>)cmd_HentLogg.Result;
        }


        public void SlettLogg()
        {
            DbCmd_SlettLogg cmd_SlettLogg = new DbCmd_SlettLogg();
            cmd_SlettLogg.Execute();
        }


        public string GenererRapport(int år)
        {
            DbCmd_HentRapport cmd_HentRapport = new DbCmd_HentRapport(år);
            cmd_HentRapport.Execute();
            Common.Rapport rapport = (Common.Rapport)cmd_HentRapport.Result;

            string[] måneder = new string[] { "Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", 
                "September", "Oktober", "November", "Desember" };
            string[] terminer = new string[] { "1. Jan-Feb", "2. Mar-Apr", "3. Mai-Jun", "4. Jul-Aug", "5. Sep-Okt", "6. Nov-Des"};

            string rapportFil = Path.GetTempFileName();

            StringBuilder htmlRapport = new StringBuilder();
            htmlRapport.AppendLine("<!DOCTYPE html>");
            htmlRapport.AppendLine("<html>");
            htmlRapport.AppendLine("<head>");
            htmlRapport.AppendLine("<title>Årsrapport 2015</title>");
            htmlRapport.AppendLine("<meta charset=\"UTF-8\">");
            htmlRapport.AppendLine("<style>");
            htmlRapport.AppendLine("h1 { font-family: Arial; font-size: 18pt; }");
            htmlRapport.AppendLine("h2 { font-family: Arial; font-size: 12pt; }");
            htmlRapport.AppendLine("table { font-family: Microsoft sans serif; font-size: 10pt; width: 100%; }");
            htmlRapport.AppendLine("td { padding: 4px; }");
            htmlRapport.AppendLine(".border_inset { border: 1px #e3e3e3 inset; }");
            htmlRapport.AppendLine(".border_outset { border: 1px #e3e3e3 outset; }");
            htmlRapport.AppendLine(".col1 { width:34%; }");
            htmlRapport.AppendLine(".col2, .col3 { width:33%; }");
            htmlRapport.AppendLine(".align-center { text-align: center; }");
            htmlRapport.AppendLine(".align-right { text-align: right; }");
            htmlRapport.AppendLine(".align-left { text-align: left; }");
            htmlRapport.AppendLine("</style>");
            htmlRapport.AppendLine("</head>");

            htmlRapport.AppendLine("<body>");
            htmlRapport.AppendLine(String.Format("<h1 class=\"align-center\">Årsrapport - {0}</h1>", rapport.År));
            htmlRapport.AppendLine("<h2 class=\"align-left\">Oversikt - terminer</h2>");
            htmlRapport.AppendLine("<table>");
            htmlRapport.AppendLine("<tr>");
            htmlRapport.AppendLine("<td class=\"col1 border_outset align-center\">Termin</td>");
            htmlRapport.AppendLine("<td class=\"col2 border_outset align-center\">Brutto beløp</td>");
            htmlRapport.AppendLine("<td class=\"col3 border_outset align-center\">Moms</td>");
            htmlRapport.AppendLine("</tr>");

            for (int nTermin = 0; nTermin < 6; nTermin++)
            {
                htmlRapport.AppendLine("<tr>");
                htmlRapport.AppendLine(String.Format("<td class=\"border_inset align-left\">{0}</td>", terminer[nTermin]));
                htmlRapport.AppendLine(String.Format("<td class=\"border_inset align-right\">{0}</td>", rapport.TerminRapport[nTermin].BruttoBeløp.ToString("F")));
                htmlRapport.AppendLine(String.Format("<td class=\"border_inset align-right\">{0}</td>", rapport.TerminRapport[nTermin].Mva.ToString("F")));
                htmlRapport.AppendLine("</tr>");
            }
            htmlRapport.AppendLine("</table>");

            htmlRapport.AppendLine("<h2 class=\"align-left\">Oversikt - måneder</h2>");
            htmlRapport.AppendLine("<table>");
            htmlRapport.AppendLine("<tr>");
            htmlRapport.AppendLine("<td class=\"col1 border_outset align-center\">Måned</td>");
            htmlRapport.AppendLine("<td class=\"col2 border_outset align-center\">Brutto beløp</td>");
            htmlRapport.AppendLine("<td class=\"col3 border_outset align-center\">Moms</td>");
            htmlRapport.AppendLine("</tr>");

            for (int nMåned = 0; nMåned < 12; nMåned++)
            {
                htmlRapport.AppendLine("<tr>");
                htmlRapport.AppendLine(String.Format("<td class=\"border_inset align-left\">{0}</td>", måneder[nMåned]));
                htmlRapport.AppendLine(String.Format("<td class=\"border_inset align-right\">{0}</td>", rapport.MånedsRapport[nMåned].BruttoBeløp.ToString("F")));
                htmlRapport.AppendLine(String.Format("<td class=\"border_inset align-right\">{0}</td>", rapport.MånedsRapport[nMåned].Mva.ToString("F")));
                htmlRapport.AppendLine("</tr>");
            }

            htmlRapport.AppendLine("</table>");
            htmlRapport.AppendLine("</body>");
            htmlRapport.AppendLine("</html>");

            using (StreamWriter sw = File.CreateText(rapportFil))
            {
                sw.Write(htmlRapport.ToString());
            }
            return rapportFil;
        }

        public IList<Common.FakturaGrunnlag> HentFakturaGrunnlagListe(Common.FakturaGrunnlagFilter filter)
        {
            DbCmd_HentFakturaGrunnlagListe cmd_HentFakturaGrunnlagListe = new DbCmd_HentFakturaGrunnlagListe(filter);
            cmd_HentFakturaGrunnlagListe.Execute();
            List<Common.FakturaGrunnlag> fakturaGrunnlagListe = (List<Common.FakturaGrunnlag>)cmd_HentFakturaGrunnlagListe.Result;
            return fakturaGrunnlagListe;
        }

        public void SendFakturaGrunnlag(int fakturaGrunnlagId)
        {/*
            DbCmd_HentFaktura cmd_HentFaktura = new DbCmd_HentFaktura(fakturaId, false);
            cmd_HentFaktura.Execute();
            Common.Faktura faktura = (Common.Faktura)cmd_HentFaktura.Result;
            if (faktura.Status == Common.FakturaStatus.Behandler)
            {
                using (DbTransact trans = new DbTransact())
                {
                    Common.LoggElement loggElement = new Common.LoggElement();
                    loggElement.Beskrivelse = String.Format("Sendt prisoverslag til {0}", faktura.Kunde.Navn);
                    DbCmd_NyttLoggElement cmd_NyttLoggElement = new DbCmd_NyttLoggElement(loggElement);
                    trans.ExecuteCommand(cmd_NyttLoggElement);

                    G_Prisoverslag g_Prisoverslag = new G_Prisoverslag(faktura);
                    g_Prisoverslag.Generer();

                    EpostSender epostSender = new EpostSender();
                    epostSender.Emne = "Prisoverslag";
                    epostSender.Melding = "Melding for prisoverslag";
                    epostSender.Mottaker = faktura.Kunde.Epost;
                    epostSender.Vedlegg.Add(g_Prisoverslag.Filnavn);
                    epostSender.Send();

                    trans.Commit();
                }
            }*/
        }

        public void GodkjennFakturaGrunnlag(int fakturaGrunnlagId)
        {
            DbCmd_GodkjennFakturaGrunnlag cmd_GodkjennFakturaGrunnlag = new DbCmd_GodkjennFakturaGrunnlag(fakturaGrunnlagId);
            cmd_GodkjennFakturaGrunnlag.Execute();
        }

        public int[] HentFakturaGrunnlagAar()
        {
            DbCmd_HentFakturaGrunnlagAar cmd_HentFakturaGrunnlagAar = new DbCmd_HentFakturaGrunnlagAar();
            cmd_HentFakturaGrunnlagAar.Execute();
            return (int[])cmd_HentFakturaGrunnlagAar.Result;
        }

        public Common.FakturaGrunnlag HentFakturaGrunnlag(int fakturaGrunnlagId)
        {
            DbCmd_HentFakturaGrunnlag cmd_HentFakturaGrunnlag = new DbCmd_HentFakturaGrunnlag(fakturaGrunnlagId);
            cmd_HentFakturaGrunnlag.Execute();
            return (Common.FakturaGrunnlag)cmd_HentFakturaGrunnlag.Result;
        }

        public void LagreFakturaGrunnlag(Common.FakturaGrunnlag fakturaGrunnlag)
        {
            using (DbTransact trans = new DbTransact())
            {
                DbCmd_LagreFakturaGrunnlag cmd_LagreFakturaGrunnlag = new DbCmd_LagreFakturaGrunnlag(fakturaGrunnlag);
                trans.ExecuteCommand(cmd_LagreFakturaGrunnlag);
                trans.Commit();
            }
        }

        public void SlettFakturaGrunnlag(int fakturaGrunnlagId)
        {
            DbCmd_SlettFakturaGrunnlag cmd_SlettFakturaGrunnlag = new DbCmd_SlettFakturaGrunnlag(fakturaGrunnlagId);
            cmd_SlettFakturaGrunnlag.Execute();
        }
    }
}
