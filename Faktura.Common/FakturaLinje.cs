﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public class FakturaLinje
    {
        private int id;
        private string produktKode;
        private string beskrivelse;
        private double ant;
        private string enhet;
        private double enhetsPris;
        private double kalkulertBeløp;
        private double kalkulertMva;
        private double kalkulertSum;
        private bool slettFlagg;
        private double rabattProsent;

        public FakturaLinje()
        {
            this.id = -1;
            this.produktKode = String.Empty;
            this.beskrivelse = String.Empty;
            this.ant = 0.0;
            this.enhet = String.Empty;
            this.enhetsPris = 0.0;
            this.kalkulertBeløp = 0;
            this.kalkulertMva = 0;
            this.kalkulertSum = 0;
            this.slettFlagg = false;
            this.rabattProsent = 0.0;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string ProduktKode
        {
            get { return produktKode; }
            set { produktKode = value; }
        }

        public string Beskrivelse
        {
            get { return beskrivelse; }
            set { beskrivelse = value; }
        }

        public double Ant
        {
            get { return ant; }
            set { ant = value; }
        }

        public string Enhet
        {
            get { return enhet; }
            set { enhet = value; }
        }

        public double EnhetsPris
        {
            get { return enhetsPris; }
            set { enhetsPris = value; }
        }

        public double KalkulertBeløp
        {
            get { return kalkulertBeløp; }
            set { kalkulertBeløp = value; }
        }

        public double KalkulertMva
        {
            get { return kalkulertMva; }
            set { kalkulertMva = value; }
        }

        public double KalkulertSum
        {
            get { return kalkulertSum; }
            set { kalkulertSum = value; }
        }
        
        public bool SlettFlagg
        {
            get { return slettFlagg; }
            set { slettFlagg = value; }
        }
        
        public double RabattProsent
        {
            get { return rabattProsent; }
            set { rabattProsent = value; }
        }
    }
}
