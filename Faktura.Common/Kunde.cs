﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public enum KundeType
    {
        Privat,
        Bedrift
    }

    public enum FakturaMetode
    {
        Epost,
        Papir
    }

    public class Kunde
    {
        private int id;
        private string navn;
        private string postAdresse;
        private string postNr;
        private string postSted;
        private string fakturaEpost;
        private string grunnlagEpost;
        private KundeType kundeType;
        private FakturaMetode fakturaMetode;

        public Kunde()
        {
            this.id = -1;
            this.navn = String.Empty;
            this.postAdresse = String.Empty;
            this.postNr = String.Empty;
            this.postSted = String.Empty;
            this.fakturaEpost = String.Empty;
            this.grunnlagEpost = String.Empty;
            this.kundeType = KundeType.Privat;
            this.fakturaMetode = FakturaMetode.Epost;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Navn
        {
            get { return navn; }
            set { navn = value; }
        }

        public string Postadresse
        {
            get { return postAdresse; }
            set { postAdresse = value; }
        }

        public string PostNr
        {
            get { return postNr; }
            set { postNr = value; }
        }

        public string PostSted
        {
            get { return postSted; }
            set { postSted = value; }
        }

        public string FakturaEpost
        {
            get { return fakturaEpost; }
            set { fakturaEpost = value; }
        }

        public string GrunnlagEpost
        {
            get { return grunnlagEpost; }
            set { grunnlagEpost = value; }
        }

        public KundeType KundeType
        {
            get { return kundeType; }
            set { kundeType = value; }
        }

        public FakturaMetode FakturaMetode
        {
            get { return fakturaMetode; }
            set { fakturaMetode = value; }
        }
    }
}
