﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public class Produkt
    {
        private int id;
        private string produktKode;
        private string beskrivelse;
        private string enhet;
        private double enhetsPris;
        private double mvaSats;

        public Produkt()
        {
            this.id = -1;
            this.produktKode = String.Empty;
            this.beskrivelse = String.Empty;
            this.enhet = String.Empty;
            this.enhetsPris = 0;
            this.mvaSats = 0;
        }

        public Produkt(int produktId)
        {
            this.id = produktId;
            this.produktKode = String.Empty;
            this.beskrivelse = String.Empty;
            this.enhet = String.Empty;
            this.enhetsPris = 0;
            this.mvaSats = 0;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string ProduktKode
        {
            get { return produktKode; }
            set { produktKode = value; }
        }

        public string Beskrivelse
        {
            get { return beskrivelse; }
            set { beskrivelse = value; }
        }

        public string Enhet
        {
            get { return enhet; }
            set { enhet = value; }
        }

        public double EnhetsPris
        {
            get { return enhetsPris; }
            set { enhetsPris = value; }
        }
        
        public double MvaSats
        {
            get { return mvaSats; }
            set { mvaSats = value; }
        }
    }
}
