﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public class FakturaPreview
    {
        private int id;
        private int nr;
        private DateTime? dato;
        private DateTime modifisertDato;
        private DateTime? innbetaltDato;
        private DateTime? forfallsDato;
        private string beskrivelse;
        private string referanse;
        private decimal kalkulertBeløp;
        private decimal kalkulertMva;
        private decimal kalkulertSum;
        private FakturaStatus status;
        private bool kredittnota;
        private bool kreditert;
        private int kundeId;
        private string kundeNavn;

        public FakturaPreview()
        {
            this.id = -1;
            this.nr = -1;
            this.dato = null;
            this.modifisertDato = DateTime.Now;
            this.innbetaltDato = null;
            this.forfallsDato = null;
            this.beskrivelse = String.Empty;
            this.referanse = String.Empty;
            this.kalkulertBeløp = 0;
            this.kalkulertMva = 0;
            this.kalkulertSum = 0;
            this.status = FakturaStatus.KlarForSending;
            this.kredittnota = false;
            this.kreditert = false;
            this.kundeId = -1;
            this.kundeNavn = String.Empty;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public int Nr
        {
            get { return nr; }
            set { nr = value; }
        }

        public DateTime? Dato
        {
            get { return dato; }
            set { dato = value; }
        }

        public DateTime ModifisertDato
        {
            get { return modifisertDato; }
            set { modifisertDato = value; }
        }

        public DateTime? InnbetaltDato
        {
            get { return innbetaltDato; }
            set { innbetaltDato = value; }
        }

        public DateTime? ForfallsDato
        {
            get { return forfallsDato; }
            set { forfallsDato = value; }
        }

        public string Beskrivelse
        {
            get { return beskrivelse; }
            set { beskrivelse = value; }
        }

        public string Referanse
        {
            get { return referanse; }
            set { referanse = value; }
        }

        public decimal KalkulertBeløp
        {
            get { return kalkulertBeløp; }
            set { kalkulertBeløp = value; }
        }

        public decimal KalkulertMva
        {
            get { return kalkulertMva; }
            set { kalkulertMva = value; }
        }

        public decimal KalkulertSum
        {
            get { return kalkulertSum; }
            set { kalkulertSum = value; }
        }

        public FakturaStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public bool Kredittnota
        {
            get { return kredittnota; }
            set { kredittnota = value; }
        }

        public bool Kreditert
        {
            get { return kreditert; }
            set { kreditert = value; }
        }

        public int KundeId
        {
            get { return kundeId; }
            set { kundeId = value; }
        }

        public string KundeNavn
        {
            get { return kundeNavn; }
            set { kundeNavn = value; }
        }
    }
}
