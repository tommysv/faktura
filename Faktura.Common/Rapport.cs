﻿using System;
using System.Collections.Generic;

namespace Faktura.Common
{
    public class TerminRapport
    {
        private RapportElement[] rapportElementer;

        public TerminRapport()
        {
            rapportElementer = new RapportElement[6];
        }

        public RapportElement this[int indeks]
        {
            get
            {
                return rapportElementer[indeks];
            }
            set
            {
                rapportElementer[indeks] = value;
            }
        }
    }

    public class MånedsRapport
    {
        private RapportElement[] rapportElementer;

        public MånedsRapport()
        {
            rapportElementer = new RapportElement[12];
        }

        public RapportElement this[int indeks]
        {
            get
            {
                return rapportElementer[indeks];
            }
            set
            {
                rapportElementer[indeks] = value;
            }
        }
    }

    public class RapportElement
    {
        private double bruttoBeløp;
        private double mva;

        public RapportElement()
        {
            this.bruttoBeløp = 0;
            this.mva = 0;
        }

        public double BruttoBeløp
        {
            get { return bruttoBeløp; }
            set { bruttoBeløp = value; }
        }

        public double Mva
        {
            get { return mva; }
            set { mva = value; }
        }
    }

    public class Rapport
    {
        private int år;
        private RapportElement rapportElement;
        private TerminRapport terminRapport;
        private MånedsRapport månedsRapport;

        public Rapport()
        {
            this.år = -1;
            this.rapportElement = new RapportElement();
            this.terminRapport = new TerminRapport();
            this.månedsRapport = new MånedsRapport();
        }

        public int År
        {
            get { return år; }
            set { år = value; }
        }

        public double BruttoBeløp
        {
            get { return rapportElement.BruttoBeløp; }
            set { rapportElement.BruttoBeløp = value; }
        }

        public double Mva 
        {
            get { return rapportElement.Mva; }
            set { rapportElement.Mva = value; }
        }

        public TerminRapport TerminRapport
        {
            get { return terminRapport; }
        }

        public MånedsRapport MånedsRapport
        {
            get { return månedsRapport; }
        }
    }
}
