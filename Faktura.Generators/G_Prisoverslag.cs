﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace Faktura.Generators
{
    public class G_Prisoverslag : IGenerator
    {
        private Common.Faktura faktura;
        private string filnavn;

        public G_Prisoverslag(Common.Faktura faktura)
        {
            this.faktura = faktura;
            this.filnavn = "Prisoverslag.pdf";
            this.filnavn = Path.Combine(Path.GetTempPath(), filnavn);
            if (File.Exists(this.filnavn))
                File.Delete(this.filnavn);
        }

        public string Filnavn
        {
            get
            {
                return filnavn;
            }
        }

        public void Generer()
        {
            // Create a new PDF document
            using (PdfDocument document = new PdfDocument())
            {
                document.Info.Title = "Created with PDFsharp";
                PdfPage page = document.AddPage();
                using (XGraphics gfx = XGraphics.FromPdfPage(page))
                {
                    XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
                    gfx.DrawString("Hello, World!", font, XBrushes.Black, new XRect(0, 0, page.Width, page.Height), XStringFormats.Center);
                    document.Save(filnavn);
                }
            }
        }
    }
}
