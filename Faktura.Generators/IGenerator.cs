﻿using System;

namespace Faktura.Generators
{
    public interface IGenerator
    {
        void Generer();
        string Filnavn { get; }
    }
}
