﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace Faktura.Generators
{
    public class G_Rapport : IGenerator
    {
        private string rapportFil;

        public G_Rapport(int aar)
        {
            this.rapportFil = Path.GetTempFileName();
            this.rapportFil = Path.ChangeExtension(rapportFil, "pdf");
        }

        public string Filnavn
        {
            get
            {
                return rapportFil;
            }
        }

        public void Generer()
        {
            // Create a new PDF document
            using (PdfDocument document = new PdfDocument())
            {
                document.Info.Title = "Created with PDFsharp";
                PdfPage page = document.AddPage();
                using (XGraphics gfx = XGraphics.FromPdfPage(page))
                {
                    XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
                    // TODO
                }
            }
        }
    }
}
