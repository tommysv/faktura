# Norsk fakturaprogram basert på .NET-framework #

(under utvikling)

![faktura.jpg](https://bitbucket.org/repo/LyyyzR/images/1805592961-faktura.jpg)

## Funksjon(er): ##
* Legge til/redigere og slette fakturagrunnlag
* Håndtere liste med fakturagrunnlag (status 'under behandling', 'sendt', 'godkjent' osv.)
* Håndtere liste med utstedte faktura (status, send på nytt osv.)
* Legge til/redigere og slette kunder (privat/bedrifts-kunder)
* Liste med varer/produkter (beskrivelse, enhetspris, mvasats)
* Logg for visning av siste operasjoner
* Konfigurasjon (opplysninger om foretak, grensesnitt (farger), epost etc.)
* Benytter Firebird database og .NET Firebird data provider (lagrede prosedyrer)
* Bruker externt bibliotek PDFSharp .NET for generering av rapporter

Teknologier benyttet: Visual Studio 2010, C#, Windows Forms, .Net Framework 2.0, MVP (Model-View-Presenter)